<?php
namespace App\Traits\Dashboard;

use App\Models\Order;
use App\Models\User;
use App\Models\Direction;

trait ManageOrders {
    public function orders() {
        $orders = Order::Filtered();

       // dd($orders);
        return view('dashboard.orders.index', compact('orders'));
    }

    public function filterOrder(){

        session()->remove('filter[admin.dashboard.orders]');
        
       

        if (request('filter')) {
            session()->put('filter[admin.dashboard.orders]', request('filter'));
            session()->save();
        }
    
        return redirect(route('admin.dashboard.orders'));
        
    }

    public function showOrder(Order $order) {
        
        if(auth()->user()->role == User::ADMIN){
             $order
            ->load("order_lines.product")
            ->loadCount("order_lines");
    
        }else{
            $order
            ->load(["order_lines.product" => function($query){
                $query->whereUserId(auth()->id());
            }])
            ->loadCount("order_lines");
        }
       
        $direction = Direction::findOrFail($order->direction_id);

        return view('dashboard.orders.show', compact('order', 'direction'));
    }

    public function showInvoice($invoice_id) {
        $order = Order::where('invoice_id', $invoice_id)->first();

        
        $order
            ->load("order_lines.product")
            ->loadCount("order_lines");
        $direction = Direction::findOrFail($order->direction_id);

        return view('dashboard.orders.show', compact('order', 'direction'));
    }

    public function downloadInvoice(Order $order) {

        
        try {
            if ($order->user_id !== auth()->id() && auth()->user()->role !== User::ADMIN) {
                session()->flash("message", ["danger", __("No tienes acceso a este factura")]);
                return back();
            }

            $data_direction = Direction::findOrFail($order->direction_id);
            $admin = User::where('role',User::ADMIN)->with('profile')->with('defaultDirection')->first();
           
            $user = User::findOrFail($order->user_id);

            $order->load('order_lines.product');
            
            if(count($admin->defaultDirection)){
                return $user->downloadInvoice($order->invoice_id, [
                    'vendor' => $admin->profile->company,
                    'product' => __("Compra de productos"),
                    'street' => $admin->defaultDirection[0]->line_1,
                    'location' => $admin->defaultDirection[0]->postal_code . " " . $admin->defaultDirection[0]->country . " (" . $admin->defaultDirection[0]->city . ")",
                    'phone' => $admin->defaultDirection[0]->phone,
                    'vendorVat' => $admin->nif,
                    'url' => "www.mundialsouvenir.es",
                    'title' => __('Factura'),
                    'data_direction' => $data_direction,
                    'order' => $order
                    

                ], env('app.name') . $order->created_at->format("d/m/Y") );
            }else{
                return $user->downloadInvoice($order->invoice_id, [
                    'vendor' => $admin->profile->company,
                    'product' => __("Compra de productos"),
                    'vendorVat' => $admin->nif,
                    'url' => "www.mundialsouvenir.es",
                    'title' => __('Factura'),
                    'data_direction' => $data_direction,
                    'order' => $order
                    
                ], config('app.name') . " - " . $order->created_at->format("d/m/Y") );
    
            }
            

        } catch (\Exception $exception) {
            session()->flash("message", ["danger", __("Ha ocurrido un error descargando la factura")]);
            return back();
        }
    }
}
