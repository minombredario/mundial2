@section('title', 'Roles')

<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles') }}">
        <i class="fas fa-user-shield"></i> {{ __('Roles') }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@push('css')
    <link rel="stylesheet" href="/css/jConfirm.css" />
@endpush

<!-- role section -->
<div class="container-fluid">
    <div class="panel shadow">
        <div class="header">
            <h2 class="title">
                <i class="fas fa-user-shield"></i> {{ __('Roles') }}
			</h2>
			<ul>
				@can('haveaccess','role.create')
				<li>
					<a href="{{ route('admin.dashboard.roles.create') }}">
						<i class="fas fa-plus"></i> {{ __('Crear Role') }}
					</a>
				</li>
				@endif
			<li>
				<a href="#" class="btn-search">
					<i class="fas fa-search"></i> {{ __('Buscar') }}
				</a>
			</li>
		</ul>
		
		<div class="inside">
			<div class="form-search d-none my-4">
				{!! Form::model([], ['route' => ['admin.dashboard.roles'], 'files' => true]) !!}
                        @csrf
				
                <div class="row">
					
					<div class="col-md-10">
						{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Busqueda']) !!}
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-dark w-100"> {{ __('Buscar') }}</button>
					</div>
				</div>

            	{!! Form::close() !!}
				
			</div>
			<table class="table table-striped mtop16">
				<thead>
					<tr>
					  <td class="align-middle">{{ __('Nombre') }}</td>
					  <td class="align-middle">{{ __('Descripción') }}</td>
					  <td class="align-middle text-center">{{ __('Acceso total') }}</td>
					  <td colspan="3"></td>
					</tr>
				  </thead>
				<tbody>
					<!-- role -->
					@forelse ($roles as $role)
					<tr>
						<td class="align-middle">{{ $role->name}}</td>
						<td class="align-middle">{{ $role->description}}</td>
						<td class="align-middle text-center">{!! $role['full-access'] === 'yes' ? '<i class="fas fa-thumbs-up fa-3x" style="color:#82c91e"></i>' : '<i class="fas fa-thumbs-down fa-3x" style="color:#fd7e14"></i>' !!}</td>                            
						<td class="align-middle"> 
							<div class="opts align-middle">
								@can('haveaccess','role.edit')
									<a href="{{ route('admin.dashboard.roles.edit',$role)}}" data-toggle="tooltip" data-placement="top" title="Editar">
										<i class="fas fa-edit" title="Editar"></i>
									</a>
								@endcan

								@can('haveaccess','role.destroy')
									<a href="#" data-route="{{ route('admin.dashboard.roles.destroy',  ["role" => $role])}}" class="delete-record" data-toggle="tooltip" data-placement="top" title="Eliminar">
										<i class="fas fa-trash-alt" title="Eliminar"></i>
									</a>
								@endcan
							</div>
						</td>  
					</tr> 
					@empty
					<tr>
						<div class="empty-results">
							
							{!! __("No tienes ningún role todavía") !!}
						</div>
					</tr>
				@endforelse
				
				</tbody>
			</table>
			<div class="d-flex justify-content-center">
				@if(count($roles))
					{{ $roles->links() }}
				@endif
			</div>
		</div>
	</div>
</div>

<!-- role section end -->
