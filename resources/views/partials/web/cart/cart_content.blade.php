@push('css')
    <style>
        .card{
            background: transparent;
            border: none;
            
        }
        .card a{
            padding-left: 20px;
            text-transform: uppercase;
            text-decoration: none;
            color: #000;
        }

        .card-footer{
            background: transparent;
        }

        .card-wrapper{
            padding-top: 20px;
            min-width: 600px;
        }

        .content-cart{
            padding-top: 200px;
            height: calc(100vh - 150px);
            overflow: auto;

        }
        
        @media screen and (max-width: 1200px) {
            .content-cart{
                padding-bottom: 300px;
            }
        }

        @media screen and (max-width: 995px) , screen and (max-height: 700px) {
            .content-cart{
                padding-top: 250px;
            }
            .btn{
                letter-spacing: 0px;
            }
            

        }

        @media screen and (max-width: 750px) , screen and (max-height: 700px) {
            .card-wrapper{
                min-width: 350px;
            }

            h4, h2{
                font-size: 19px;
            }

            h2{

            }
        }   

        @media screen and (max-width: 500px) {
            
            h4, h2{
                font-size: 15px;
            }

            .btn{
                font-size: 12px;
            }

        }   
    </style>
@endpush

<div class="row content-cart">  
    @forelse($cart->getContent() as $product)
    <div class="col-sm-12 col-md-6 align-items-stretch card-wrapper">
        <div class="card h-100">
            <div class="row g-0">
                <!-- product image -->
                <div class="col-sm-12 g-0 col-md-6">
                    <img src="{{ $product->imagePath() }}"  class="img-fluid" alt="...">
                </div>
                <!-- end product image -->

                <!-- product content-->
                <div class="col-sm-12 col-md-6">
                    <div class="card-body">
                        <div style="height: 75px">
                            <h2 class="card-title text-uppercase">{{ $product->name }}</h2>
                        </div>
                        <p class="card-text">{{ __("Ref: :code", ["code" => $product->code]) }}</p>
                        @if(!$payment)
                            {!! Form::model($product, ['route' => ['change_quantity_product', ["product" => $product]] , 'id' => 'qtyForm-' . $product->id ]) !!}
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="qty">{{ __('Uds.') }}</label>
                                    {!! Form::number('qty', $product->qty  , ['min' => '0', 'max' => $product->stock, 'step' => '1', 'onchange' => 'changeQty(' . $product->id .');', 'id' => 'change_qty-' . $product->id ]) !!}
                                </div>
                            {!! Form::close() !!}
                        @else
                            <div class="input-group mb-3">
                                <label class="input-group-text w-100" for="qty">{{ __('Uds. :qty', ['qty' => $product->qty ]) }} </label>
                            </div>
                        @endif

                        @if($product->in_discount)
                        <h4 class="card-text w-100">
                            <del><small>{{ $product->formatted_price }}</small></del>
                        </h4>
                        <h1 class="card-text"><strong> {{ $product->priceWithDiscount() }} </strong></h1>
                        @else
                            <h1 class="card-text"><strong>{{ $product->formatted_price }}</strong></h1>            
                        @endif

                        

                    </div>
                   
                </div>

                <!-- end product content-->
                 @if(!$payment)
                <div class="card-footer text-center">
                    <a href="{{ route("remove_product_from_cart", ["product" => $product]) }}">
                        {{ __("Eliminar producto") }} <i class="fas fa-times"></i>
                    </a>
                </div>
                    
                @endif

            </div>
        </div>
    </div>
    @empty
            
        <div class="container">
            {!! __("No tienes ningún producto en el carrito") !!}
        </div>
    @endforelse
        
   
</div>

@push("js")
    <script>
        function changeQty(id) {
            $('#qtyForm-' + id).submit()
        }
        
    </script>
@endpush

