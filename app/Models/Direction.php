<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\Hashidable;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * App\Models\Direction
 *
 * @property int $id
 * @property int $user_id
 * @property int $status
 * @property string $default
 * @property string $country
 * @property string $name
 * @property string $line_1
 * @property string $line_2
 * @property string $city
 * @property string $state State / Province / Region
 * @property string $postal_code
 * @property string $phone
 * @property string $nif solo para Canarias, Ceuta y Melilla
 * @property string $comments
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Direction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Direction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Direction query()
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereLine1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereLine2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereNif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Direction whereUserId($value)
 * @mixin \Eloquent
 */
class Direction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'status', 'default', 'country','name', 'line_1','line_2','city','state','postal_code','phone','nif','comments'
    ];
    	
    protected static function boot() {
        parent::boot();
        if ( !app()->runningInConsole()) {
            self::creating(function ($table) {
                $table->user_id = auth()->id();
            });
        }
    }
    
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function defaultDirection(Builder $builder) {
      
        $direction = $builder->where([
            ["user_id", auth()->id()],
            ['default', 'yes'],
        ])->first();
           
        return $direction;

    }

    public function scopeFiltered(Builder $builder) {
      
        return $builder->where("user_id", auth()->id())->paginate();

    }

    public function scopeDefaultDirection(Builder $builder) {
      
        return $builder->where([
            ["user_id", auth()->id()],
            ['default', 'yes'],
        ])->first();

    }

    public static function myDirections() {
        return Direction::where("user_id", auth()->id())->get();
    }

    public static function shippingDirection(){

        if (session()->has('direction[shipping]')) {
            
            return Direction::findOrFail(session('direction[shipping]'));
        }
    }
    
   
}
