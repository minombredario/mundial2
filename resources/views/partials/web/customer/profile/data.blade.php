 {!! Form::model($user, $optionsData) !!}
    @method("PUT")
       
    <div class="row">
        <div class="col-md-4 mt-2">
            {!! Form::label('name', __("Nombre")) !!}
            <div class="input-group">
                <span class="input-group-text">
                    <i class="far fa-keyboard"></i>
                </span>
                {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        
        <div class="col-md-4 mt-2">
            {!! Form::label('lastname', __("NIF/DNI")) !!}
            <div class="input-group">
                <span class="input-group-text">
                    <i class="far fa-keyboard"></i>
                </span>
                    {!! Form::text('nif', null, ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-4 mt-2">
            {!! Form::label('email', __("Correo electrónico")) !!}
            <div class="input-group">
                <span class="input-group-text">
                    <i class="far fa-keyboard"></i>
                </span>
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <div class="d-inline-block">
        {!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2']); !!}
    </div>
{!! Form::close() !!}