<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Content;
use App\Http\Requests\ContentRequest;
use DB;
use Str;

trait ManageContents {

	public function contents(){

        //$this->authorize('haveaccess','content.index');

        $contents = Content::paginate();
        return view('dashboard.contents.index', compact('contents'));  

    }

    public function createContent() {

        //$this->authorize('haveaccess','product.create');

        $content = new Content;
        $title = __("Crear Política");
        $textButton = __("Guardar");
        $options = ['route' => ['admin.dashboard.contents.store'] ];
        $required = false;
        return view('dashboard.contents.create', compact('title', 'content', 'options', 'textButton', 'required'));

    }

    public function storeContent(Request $request) {
        
        //$this->authorize('haveaccess','content.create');
        
        try {
            DB::beginTransaction();
            
            
            $content = Content::create($this->contentInput());
           
            DB::commit();
            session()->flash("message", ["success", __("Documento creado satisfactoriamente")]);
            return redirect(route('admin.dashboard.contents', ['content' => $content]));
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function editContent(Content $content) {
       
        //$this->authorize('haveaccess','content.edit');

        $title = __("Editar el contenido de :content", ["content" => $content->title]);
        
        $textButton = __("Actualizar");
         
        $options = ['route' => ['admin.dashboard.contents.update', ["content" => $content]], 'files' => true];
        $update = true;
        $required = true;
        return view('dashboard.contents.edit', compact('title', 'content', 'options', 'textButton', 'update', 'required'));
    }

    public function updateContent(Request $request, Content $content) {
        
        //$this->authorize('haveaccess','content.edit');

        try {
            DB::beginTransaction();

           
            $content->fill($this->contentInput())->save();
            
            DB::commit();

            session()->flash("message", ["success", __("Documento actualizado satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function destroyContent(Content $content){   

        //$this->authorize('haveaccess','content.destroy');
        
        try {
            if (request()->ajax()) {
               
                $content->delete();
                session()->flash("message", ["success", __("El documento :content ha sido eliminado correctamente", ["content" => $content->title])]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }
    }

    protected function contentInput(string $file = null): array {

        return [
            'title' => e(request('title')),
            'content' => e(request('content')),
        ];
    }
    
}