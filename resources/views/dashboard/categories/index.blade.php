@extends("layouts.dashboard")

@section('title', __('Categorías') )

@section("content")
    @include("dashboard.categories.list")
@endsection