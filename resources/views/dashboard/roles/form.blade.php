

<?php $readonly = true; $disabled = 'disabled' ?>

@can('haveaccess', 'role.edit')
	<?php $readonly = true; $disabled = ''?>
@endcan

@can('haveaccess', 'role.update')
	<?php $readonly = true; $disabled = ''?>
@endcan



@include('partials.form_errors')

<div class="container-fluid">
	{!! Form::model($role, $options) !!}
		@isset($update)
			@method("PUT")
		@endisset
		@csrf
	<div class="row">
		<div class="col-md-6">
			<div class="panel shadow">
				<div class="header d-flex justify-content-between">
					<h2 class="title">
						<i class="fas fa-user-shield"></i> {{ __('Role') ." " . $role->name }} 
					</h2>
					<div class="form-check form-switch">
						{!! Form::checkbox('full-access', null ,($role['full-access'] === "yes" ? true : false ), ['class' => 'form-check-input align-middle', 'id' => "full-access", 'disabled' => $readonly ] ) !!}
						{!! Form::label('full-access', __('Acceso total'), ['class' => 'form-check-label align-middle', 'for' => 'full-access' ]) !!}
						
					</div>
				</div>
				<div class="inside">
					<div class="form-group">
						{!! Form::label('name', __("Nombre")) !!}
						{!! Form::text('name', null, ['class' => 'form-control', 'readonly' => $readonly]) !!}
					</div>

					<div class="form-group">
						{!! Form::label('slug', __("Slug")) !!}
						{!! Form::text('slug', null, ['class' => 'form-control', 'readonly' => $readonly]) !!}
					</div>
					
					<div class="form-group">
						{!! Form::label('description', __("Añade una descripción al role")) !!}
						{!! Form::textarea('description', null, ["class" => "form-control", "rows" => 3]) !!}	
					</div>

					@can('haveaccess', 'role.update')
						{!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2 w-100']); !!}
					@endcan
					
					
					
				</div>
				
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="fas fa-user-lock"></i> {{ __("Lista de permisos") }}
					</h2>
				</div>
				<div class="inside">
					
					@foreach($permissions as $permission)
					<div class="form-check">
						<input class="form-check-input" type="checkbox" id="permission_{{$permission->id}}" value="{{$permission->id}}" name="permission[]"
						@if( in_array( $permission->id, $role->permissions->pluck('id')->toArray() ) )
							checked
						@endif
						{{ $disabled }}
						>

						<label class="form-check-label" for="permission_{{$permission->id}}">
							{!! __(':id - :name <em>( :description )</em>', ["id" => $permission->id, 'name' => $permission->name, 'description' => $permission->description]) !!}
						</label>
					</div>
					@endforeach
				</div>				
			</div>
		</div>
	</div>
	
	{!! Form::close() !!}
</div>
