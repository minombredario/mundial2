<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profit extends Model
{
    protected $fillable = [
        'invoice_id', 'total_profit', 'profit'
    ];

    public function orders() {
        return $this->hasMany(Order::class);
    }


}
