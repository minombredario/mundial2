<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ManageCart;
use App\Traits\Customer\ManageProducts;
use App\Traits\Customer\ManageOrders;
use App\Traits\Customer\ManageProfile;
use App\Traits\Customer\ManageDirections;



class CustomerController extends Controller
{
    use ManageCart, ManageProducts, ManageOrders, ManageProfile, ManageDirections;

    public function __construct(Request $request){
        
    }
 

    
}
