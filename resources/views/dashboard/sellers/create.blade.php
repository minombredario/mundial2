@extends("layouts.dashboard")

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.sellers') }}">
        <i class="fas fa-users"></i> {{ __('Distribuidores') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles.create') }}">
        <i class="fas fa-user-plus"></i> {{ __("Crear distribuidor") }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.sellers.form")
@endsection