@extends("layouts.web")

@push('css')
   <style>
       .wrapper-product{
           width: 430px;
       }
       .product-container {
            background-color: #fff;
            height: 450px;
            overflow: hidden;
            position: relative;
            margin-bottom: 20px;
            border-radius: 50%;
            width: 400px;
        }

        .tag-featured, .tag-discount {
            background-color: red;
            width: 50px;
            height: 70px;
            position: absolute;
            color: #fff;
            right: 40px;
            z-index: 9;
            top: 35px;
            transform: rotate(137deg);
            border-radius: 0 50%;
        }

        .tag-featured::before, .tag-discount::before {
            color: #fff;
            font-weight: bold;
            display: block;
            transform-origin: top center;
            transform: rotate(222.5deg) translateX(-28px) translateY(-37px);
        }

        .tag-discount::before{
            content: attr(data-before);
        }
        

        .product-description {
            padding: 10px 20px;
            color: #797979;
        }

        .product-image {
            /*padding: 20px;*/
            height: 323px;
            position: relative;
            overflow: hidden;
            transition: 1s;
        }

        .product-link {
            position: absolute;
            background: #fff;
            width: 100px;
            height: 100px;
            color: #4E5156;            
            border-radius: 50%;
            font-size: 25px;
            text-align: center;
            padding: 35px 0;
            line-height: 25px;
            left: calc(50% - 50px);
            top: calc(50% - 50px);
            opacity: 0;
            transition: 1s;
            font-style: italic;
        }

        .product-link:hover {
            text-decoration: none;
            color: rgba(0,0,0,0.36);
        }


        .hover-link {
            background-color: rgba(0,0,0,0.36);
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
            width: 100%;
            height: 100%;
            transition: 0.5s;
        }

        .product-image img {
            width: 100%;
        }

        .product-description h1 {
            font-size: 23px;
            margin-bottom: 5px;
            margin-top: 0;
            text-align: center;
            width: 100%;
        }

        .product-description p {
            color: #C4C4C4;
        }

        .product-description .price {
            width: 100%;
            font-size: 23px;
            text-align: center;
            font-weight: bold;
            color: #000;
            margin-bottom: 5px;
        }

        .description{
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2; /* number of lines to show */
            -webkit-box-orient: vertical;
            margin-top: -10px;
        }

        .product-cart {
            display: none;
        }

     
        .product-container:hover {
            box-shadow: 0px 10px 25px -2px rgba(0,0,0,0.36);
        }

        .product-container:hover .product-image {
            transition: 1s;
            /*padding: 10px;*/
        }

        .product-container:hover .product-cart {
            display: block;
            position: absolute;
            
           
            width: 300px;
           
            padding: 5px;
            display: flex;
            justify-content: center;
            align-items: center;
            
            /* position the div in center */
            position: absolute;
            top: 75%;
            left: 50%;
            transform: translate(-50%, -50%);
        
            
        }
        .product-container:hover .hover-link {
            opacity: 0.5;
        }
        .product-container:hover .product-link {
            opacity: 1;
        }

        .product-link:hover {
            -webkit-animation: hovering 1000ms linear both;
            animation: hovering 1000ms linear both;
        }
        
    </style>
@endpush

@section('title', __($category->name) )

@section('page')
    
    <h1 class="title-page text-md-left text-sm-center"> {{ $category->name }} </h1>

@endsection

@include('partials.video', ['category' => $category])

@section('content')
 
        <div class="container wrapper-container">
            <div class="row">
                @forelse($category->productsCustomer as $product)
                    <div class="col-md-4 d-flex flex-wrap wrapper-product">
                        <div class="product-container">
                            @if($product->featured)
                                <div class="tag-featured"></div>
                            @endif
                            
                          
                            <div class="product-image">
                                @if($product->in_discount)
                                    <div class="tag-discount" data-before="{{ $product->discount_type == \App\Models\Product::PERCENT ? '-'. $product->discount . '%' : '-'. $product->discount . '€' }}"></div>
                                @endif
                                <span class="hover-link"></span>
                                <a href="{{ route('web.products.show', [ 'module' => $category->module_name , 'cat' =>  $category->slug, 'product' => $product ] )}}" class="product-link">{{ __('Más') }}</a>
                                <img class="img-responsive" src="{{ $product->imagePath() }}" alt="{{ $product->name }}">
                            </div>
                            <div class="product-description">
                                <div class="product-label">
                                    <div class="product-name">
                                        <h1>{{ $product->name }}</h1>
                                        @if($product->in_discount)
                                            <p class="price"><del><small>{{ $product->formatted_price }}</small></del><br/> {{ $product->priceWithDiscount() }}</p>
                                        @else
                                            <p class="price"> {{ $product->formatted_price }} </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="product-cart">
                                <div class="col d-flex justify-content-center">
                                    {!! Form::model($product, ['route' => ['add_product_to_cart', ["product" => $product]], 'files' => true]) !!}
                                        
                                    <div class="input-group mb-3">
                                        {!! Form::number('qty', "1" , ['min' => '0', 'max' => $product->stock, 'step' => '1' ]) !!}
                                        
                
                                        {!! Form::submit( __('AÑADIR AL CARRITO'), ['class' => 'btn btn-cart mx-1']); !!}
                                        
                                    </div>
                                    {!! Form::close() !!}

                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                
                @empty

                @endforelse
                
            </div>
        </div>
  
@endsection


@push('js')
    
    <script>
        $(document).ready(function(){
            // Target your element
            $('#content').colourBrightness();
            if($('#content').hasClass("light")){
                $('.logo').attr('src', "{{ url('/storage/web/logonegro.png') }}" );
            }

            if($('#content').hasClass("dark")){
                $('.logo').attr('src', "{{ url('/storage/web/logoblanco.png') }}" );
            }
            function hover(element) {
                element.setAttribute('src', '/storage/web/carrito-n.svg');
            }
        });
    </script>
    
@endpush