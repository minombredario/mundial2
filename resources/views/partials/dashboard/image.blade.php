@push('css')
	<style>
		.image_container{
			text-align: center;
		}
		.image_container img{
			border: 5px solid #004a6b;
			width: 250px;
		}
					
	</style>
@endpush
<div class="image_container">
	
	<img src="{{ $category->backgroundImagePath() }}" class="img-fluid"/>
	
</div>