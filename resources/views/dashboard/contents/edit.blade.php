@extends("layouts.dashboard")

@section('title', __("Políticas") )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.contents') }}">
        <i class="fas fa-boxes"></i> {{ __('Políticas') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.contents.edit', $content) }}">
        <i class="fas fa-box"></i> {{ __("Editar") . " " . $content->title }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.contents.form", ['required' => false])
@endsection