<!-- product section -->
@section('title', 'Productos')

<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.products') }}">
        <i class="fas fa-boxes"></i> {{ __('Productos') }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@push('css')
    <link rel="stylesheet" href="{{ asset("css/jConfirm.css") }}" />
@endpush

<!-- product section -->
<div class="container-fluid">
    <div class="panel shadow">
        <div class="header">
            <h2 class="title">
                <i class="fas fa-boxes"></i> {{ __('Productos') }}
			</h2>
			<ul>
				@can('haveaccess','product.create')
				<li>
					<a href="{{ route('admin.dashboard.products.create') }}">
						<i class="fas fa-plus"></i> {{ __('Crear producto') }}
					</a>
				</li>
				@endif
			<li>
				<a href="#"> {{ __('Filtrar') }} <i class="fas fa-chevron-down"></i></a>
				<ul class="shadow">

					<li>
						<form action="{{ route('admin.dashboard.products') }}" method="POST" id="form-all" >
							@csrf
							
							{!! Form::text('search','all', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-all').submit();" >
								<i class="fas fa-globe-americas"></i> {{ __('Todos') }}
							</a> 
						</form>
					</li>

					<li>
						<form action="{{ route('admin.dashboard.products') }}" method="POST" id="form-featured" >
							@csrf
							
							{!! Form::text('search','featured', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-featured').submit();" >
								<i class="fas fa-star"></i> {{ __('Destacados') }}
							</a> 
						</form>
					</li>

					<li>
						<form action="{{ route('admin.dashboard.products') }}" method="POST" id="form-stock" >
							@csrf
							
							{!! Form::text('search','stock', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-stock').submit();" >
								<i class="fas fa-cubes"></i> {{ __('Stock') }}
							</a> 
						</form>

					<li>
						<form action="{{ route('admin.dashboard.products') }}" method="POST" id="form-published" >
							@csrf
							
							{!! Form::text('search','published', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-published').submit();" >
								<i class="fas fa-eye"></i> {{ __('Publicado') }}
							</a> 
						</form>
					</li>
					
					<li>
						<form action="{{ route('admin.dashboard.products') }}" method="POST" id="form-pending" >
							@csrf
							
							{!! Form::text('search','pending', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-pending').submit();" >
								<i class="fas fa-pen-square"></i> {{ __('Pendiente') }}
							</a> 
						</form>
					</li>
					
					<li>
						<form action="{{ route('admin.dashboard.products') }}" method="POST" id="form-rejected" >
							@csrf
							
							{!! Form::text('search','rejected', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-rejected').submit();" >
								<i class="fas fa-eye-slash"></i> {{ __('Rechazado') }}
							</a> 
						</form>
					</li>
					
					<li>
						<form action="{{ route('admin.dashboard.products') }}" method="POST" id="form-trash" >
							@csrf
							
							{!! Form::text('search','trash', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-trash').submit();" >
								<i class="fas fa-trash"></i> {{ __('Eliminado') }}
							</a> 
						</form>
					</li>
					
				</ul>
			</li>
			<li>
				<a href="#" class="btn-search">
					<i class="fas fa-search"></i> {{ __('Buscar') }}
				</a>
			</li>
		</ul>
		
		<div class="inside">
			<div class="form-search d-none my-4">
				{!! Form::model([], ['route' => ['admin.dashboard.products'], 'files' => true]) !!}
                        @csrf
				
                <div class="row">
					
					<div class="col-md-4">
						{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('Busqueda') ]) !!}
					</div>
					<div class="col-md-4">
						{!! Form::select('filter',  ['0' => __('Nombre del producto'), '1' => __('Código'), '2' => __('País') ] , 0, ['class' => 'form-select']) !!}
					</div>
					<div class="col-md-2">
						{!! Form::select('status', \App\Models\Product::statusTypes(), 0, ['class' => 'form-select']) !!}
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-dark w-100"> {{ __('Buscar') }}</button>
					</div>
				</div>

            	{!! Form::close() !!}
				
			</div>
			<table class="table table-striped mtop16">
				<thead>
					<tr>
						<td class="align-middle">{{ __('Nombre') }}</td>
						<td class="align-middle">{{ __('País') }}</td>
						@admin
							<td class="align-middle">{{ __('Vendedor') }}</td>
						@endadmin
                        <td class="align-middle">{{ __('Precio') }}</td>
                        <td class="align-middle">{{ __('Stock') }}</td>
						<td class="align-middle">{{ __('Estado') }}</td>
						@admin
							<td class="align-middle">{{ __('Destacado') }}</td>
						@endadmin
                        <td colspan="3"></td>
					</tr>
				  </thead>
				<tbody>
					<!-- product -->
					@forelse ($products as $product)
					<tr>
						
						<td class="align-middle" >{{ $product->name }}</td>
						<td class="align-middle" >{{ $product->categories[0]->name }}</td>
						@admin
							<td class="align-middle">{{$product->seller->name}}</td>
						@endadmin
						<!--td class="align-middle">{!! htmlspecialchars_decode($product->description) !!}</td-->
						<td class="align-middle">{{ auth()->user()->role == \App\Models\User::PROVIDER ? \App\Helpers\Currency::formatCurrency($product->real_price) : $product->formatted_price  }}</td>
						<td class="align-middle">{{ $product->stock }}</td>
                        <td class="align-middle">
							@switch($product->status)
								@case(\App\Models\Product::PUBLISHED)
									@php
										$color = "success"
									@endphp
									@break
								@case(\App\Models\Product::PENDING)
									@php
										$color = "warning"
									@endphp
									@break
								@case(\App\Models\Product::REJECT)
									@php
										$color = "danger"
									@endphp
									@break
								@default
									
						@endswitch
						
						<span class="badge bg-{{$color}}">{{ \App\Models\Product::statusTypes($product->status) }}</span></td>

						@admin
							<td class="align-middle">{{  $product->featured == 1 ? __("Si") : __("No")}}</td>     
						@endadmin               
						<td> 
							<div class="opts">
								@can('haveaccess','product.edit')
									<a href="{{ route('admin.dashboard.products.edit',$product)}}" data-toggle="tooltip" data-placement="top" title="{{ __("Editar") }}">
										<i class="fas fa-edit"></i>
									</a>
								@endcan

								@can('haveaccess','product.destroy')
									<a href="#" data-route="{{ route('admin.dashboard.products.destroy',  ["product" => $product])}}" class="delete-record" data-toggle="tooltip" data-placement="top" title="{{ __("Eliminar") }}">
										<i class="fas fa-trash-alt"></i>
									</a>
								@endcan
							</div>
						</td>  
					</tr> 
					@empty
					<tr>
						<div class="empty-results">
							{!! __("Ningún resultado") !!}
						</div>
					</tr>
					@endforelse
				
				</tbody>
			</table>
			<div class="d-flex justify-content-center">
				@if(count($products))
					{{ $products->links() }}
				@endif
			</div>
		</div>
	</div>
</div>

<!-- product section end -->

<!-- product section end -->
