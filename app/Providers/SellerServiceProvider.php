<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SellerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::if('seller', function () {
            if (auth()->check()) {
                return auth()->user()->isSeller();
            }
            return false;
        });
    }
}
