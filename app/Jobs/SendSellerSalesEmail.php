<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Mail\SellerNewSale;
use App\Models\Product;
use App\Models\User;
use App\Models\OrderLine;
use Illuminate\Support\Facades\Mail;

class SendSellerSalesEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public int $tries = 5;

    public User $seller;

    public User $customer;

    public OrderLine $order_line;

    public Product $product;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $seller, User $customer, Product $product, OrderLine $order_line)
    {
        $this->seller = $seller;
        $this->customer = $customer;
        $this->product = $product;
        $this->order_line = $order_line;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::to($this->seller->email)->send(
                new SellerNewSale(
                    $this->seller,
                    $this->customer,
                    $this->product,
                    $this->order_line,
                )
            );
        } catch (\Exception $exception) {
            \Log::info($exception->getMessage());
        }
    }
}
