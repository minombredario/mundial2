@push('css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet"/>
@endpush


<div class="container-fluid">
     @include('partials.form_errors')
    <div class="panel shadow">
        <div class="header d-flex justify-content-between">
            <h2 class="title">
                <i class="fas fa-plus"></i> {{ $title }}
            </h2>
            {!! Form::model($content, $options) !!}
            @isset($update)
                @method("PUT")
            @endisset

            @csrf       
        </div>
        <div class="inside">
			<div class="row">   

                <div class="col-md-6">
                    {!! Form::label('title', __("Título")) !!}
                    <div class="input-group" >
                        {!! Form::label('title', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
                        {!! Form::text('title', null, ['class' => 'form-control' .($errors->has('title') ? ' is-invalid' : '') ]) !!}
                    </div>

                    @if ($errors->has('title'))
                        <div class="{{$errors->has('title') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('title') ? 'display:block' : ''}}">
                            <strong>{{ $errors->first('title') }}</strong>
                        </div>
                    @endif
                    
                </div>
                    
                    
            </div>

            <div class="row mtop16">
                <div class="col-md-12">
                    {!! Form::label('content', __("Añade la descripción al producto")) !!}
                    {!! Form::textarea('content', old('content') ?? $content->description, ['id' => 'summernote' , 'class' => ($errors->has('content') ? ' is-invalid' : '')]) !!}
                </div>
                @if ($errors->has('content'))
                    <div class="{{$errors->has('content') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('content') ? 'display:block' : ''}}">
                        <strong>{{ $errors->first('content') }}</strong>
                    </div>
                @endif
            </div>

            
            <div class="row mtop16">
                <div class="col-md-12">
                    {!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2']); !!}
                </div>
            </div>
            

                
            {!! Form::close() !!}
        </div>
    </div>
</div>


@push("js")
    <script src="{{ url('/js/drag-arrange.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            $('#summernote').summernote({
                height: 300,
            });
        });
    </script>
@endpush
