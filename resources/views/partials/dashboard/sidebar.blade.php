<div class="sidebar shadow">
	<div class="section-top">
		<div class="logo">
			
			<img src="{{ '/storage/web/logoblanco.png' }}"  class="img-fluid"/>
			
		</div>
		<div class="logo-small">
			
			<img src="{{ '/storage/web/logo.png' }}"  class="img-fluid"/>
			
		</div>
		
	</div>
	<div class="main  flex-column">	
		<ul>
			@can('haveaccess', 'dashboard.index')
			<li>
				<a href="{{ route("admin.dashboard.index") }}" class="index" data-toggle="tooltip" data-placement="right" title="{{ __('Dashboard') }}">
					<i class="fas fa-home"> </i>
					<span> {{ __('Dashboard') }}</span>
				</a>
			</li>
			@endcan
			
			@admin
			<li>
				<a href="{{ route("admin.dashboard.contents") }}" class="contents" data-toggle="tooltip" data-placement="right" title="{{ __('Políticas') }}">
					<i class="far fa-newspaper"></i>
					<span> {{ __('Políticas') }}</span>
				</a>
			</li>
			@endadmin

			@can('haveaccess', 'product.index')
			<li>
				<a href="{{ route("admin.dashboard.products") }}" class="products" data-toggle="tooltip" data-placement="right" title="{{ __('Productos') }}">
					<i class="fas fa-boxes"></i>
					<span> {{ __('Productos') }}</span>
				</a>
			</li>
			@endcan
			@can('haveaccess', 'category.index')
			<li>
				<a href="{{ route("admin.dashboard.categories") }}" class="categories" data-toggle="tooltip" data-placement="right" title="{{ __('Categorías') }}">
					<i class="fas fa-folder-open"></i>
					<span> {{ __('Categorías') }}</span>
				</a>
			</li>
			@endcan
			@can('haveaccess', 'user.index')
			<li>
				<a href="{{ route("admin.dashboard.users") }}" class="users" data-toggle="tooltip" data-placement="right" title="{{ __('Clientes') }}">
					<i class="fas fa-users"></i>
					<span> {{ __('Clientes') }}</span>
				</a>
			</li>
				@admin
				
					<li>
						<a href="{{ route("admin.dashboard.sellers" ) }}" class="sellers" data-toggle="tooltip" data-placement="right" title="{{ __('Distribuidores') }}">
							<i class="fas fa-user-tie"></i>
							<span> {{ __('Distribuidores') }}</span>
						</a>
					</li>
					<li>
						<a href="{{ route("admin.dashboard.providers") }}" class="providers" data-toggle="tooltip" data-placement="right" title="{{ __('Proveedores') }}">
							<i class="fas fa-user-friends"></i>
							<span> {{ __('Proveedores') }}</span>
						</a>
					</li>
			
				@endadmin
			@endcan
			
			@can('haveaccess', 'order.index')
			<li>
				<a href="{{ route("admin.dashboard.orders") }}" class="orders" data-toggle="tooltip" data-placement="right" title="{{ __('Pedidos') }}">
					<i class="fas fa-shopping-basket"></i>
					<span> {{ __('Pedidos') }}</span>
				</a>
			</li>
			@endcan
			@admin
			<li>
				<a href="{{ route("admin.dashboard.roles") }}" class="roles" data-toggle="tooltip" data-placement="right" title="{{ __('Roles') }}">
					<i class="fas fa-user-shield"></i>
					<span> {{ __('Roles') }}</span>
				</a>
			</li>
			@endadmin
		</ul>
	</div>
</div>


@push("js")
    
    <script>
		
		var route = $('[name=routeName]')[0].getAttribute('content').split('.');
		
		route = route[2];

        var route_active = $(`.${route}`).toggleClass('active');
        
    </script>
@endpush
