@component('mail::message')
# Hola {{ $customer->name }}
<br>
Muchas gracias por haber realizado tu pedido en {{ config('app.name') }}.
<br>
<br>
Aquí tienes los datos de tu pedido:
<br>
<br>
<table style="margin-left: auto; margin-right: auto; margin-top: 20px" width="550">
    <thead>
        <tr>
            <th align="left"> {{ __('Producto') }} </th>
            <th align="right"> {{ __('Precio') }} </th>
            <th align="right"> {{ __('Cantidad') }} </th>
            <th align="right"> {{ __('Descuento') }} </th>
            <th align="right"> {{ __('Total') }} </th>
        </tr>
    </thead>
    <tbody>
        @foreach($order->order_lines as $order_line)
            <tr>
                <td align="left">{{ $order_line->product->name }}</td>
                <td align="right">{{ \App\Helpers\Currency::formatCurrency($order_line->price) }}</td>
                <td align="right">{{ $order_line->quantity }}</td>
                <td align="right">{{ $order_line->product->discount . \App\Models\Product::discountIcon($order_line->product->discount_type) }}</td>
                <td align="right">{{ \App\Helpers\Currency::formatCurrency($order_line->price_total) }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2"></td>
                <td colspan="2" align="right">{{ __('Portes')}}</td>
                <td align="right">{{ \App\Helpers\Currency::formatCurrency($order->shipping_cost) }}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td colspan="2" align="right">{{ __('Total')}}</td>
                <td align="right"><strong>{{ \App\Helpers\Currency::formatCurrency($order->total_amount + $order->shipping_cost) }}</strong></td>
            </tr>
        </tfoot>
    
</table>

@component('mail::button', [
    'url' => env("APP_URL")
])
    Volver a la plataforma
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent
