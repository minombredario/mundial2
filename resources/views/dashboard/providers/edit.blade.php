@extends("layouts.dashboard")

@section('title', __("Proveedor " . $user->name) )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.providers') }}">
        <i class="fas fa-users"></i> {{ __('Proveedores') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.providers.edit', $user) }}">
        <i class="fas fa-user-edit"></i> {{ $user->name }} 
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.providers.form")
@endsection