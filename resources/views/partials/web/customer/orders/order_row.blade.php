<tr class="text-center">
    <td>{{ $order->formatted_total_amount }}</td>
    <td>{{ $order->created_at->format("d/m/Y") }}</td>
    <td>{{ $order->formatted_status }}</td>
    <td>{{ $order->order_lines_count }}</td>
     <td>{{ \App\Helpers\Currency::formatCurrency($order->shipping_cost) }}</td>
    @if(!$detail)
        <td>
            <a href="{{ route("customer.orders.download_invoice", ["order" => $order]) }}" class="btn btn-outline-dark">
                <i class="fas fa-file-download"></i>
            </a>
            <a class="btn btn-outline-dark" href="{{ route('customer.orders.show', ['order' => $order]) }}">
                <i class="fa fa-eye"></i>
            </a>
        </td>
    @endif
</tr>
