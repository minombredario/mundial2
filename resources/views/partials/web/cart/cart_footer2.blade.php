@push('css')
    <style>
        *------------------
            Footer section
        ---------------------*/

    .footer-section {
        border-top: 3px solid #d82a4e;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
    }

    .footer-warp {
        max-width: 1559px;
        padding: 0 15px;
        margin: 0 auto;
    }

    .widget-item {
        width: 30%;
        padding: 0 15px;
    }

    
    .widget-item .direction-list {
        margin-bottom: 27px;
        position: relative;
        padding-left: 10px;
    }

    .footer-bottom {
        overflow: hidden;
        background: #edf4f6;
        padding: 20px 0;
        margin-top: 50px;
    }

    .footer-top {
        padding-top: 25px;
    }

    .footer-menu {
        list-style: none;
        float: right;
    }

    .footer-menu li {
        display: inline;
    }

    .footer-menu li:last-child a {
        padding-right: 0;
        margin-right: 0;
    }

    .footer-menu li:last-child a:after {
        display: none;
    }

    .footer-menu li a {
        display: inline-block;
        font-size: 14px;
        font-weight: 600;
        color: #474747;
        padding-right: 10px;
        margin-right: 10px;
        position: relative;
    }

    .footer-menu li a:after {
        position: absolute;
        content: "|";
        right: 0;
        top: 0;
    }

    .copyright {
        padding-top: 3px;
        float: left;
        font-size: 14px;
        font-weight: 600;
        color: #474747;
    }

    .my-custom-scrollbar {
        position: relative;
        height: 150px;
        overflow: auto;
    }
    .table-wrapper-scroll-y {
        display: block;
    }

    @media screen and (max-width: 500px){
        .widget-item {
           width: 100%;
           margin-bottom: 15px;
        }

    }
    </style>

@endpush

@php 
    $dir =  Auth::user() ? \App\Models\Direction::myDirections() : [];
@endphp
<!-- footer section -->
<footer class="footer-section shadow-lg pb-2">
   
    <div class="footer-top">
        <div class="footer-warp">
            <div class="row">
                
                    
                
                <div class="widget-item">
                    @auth
                    <h6>{{__('Dirección de envío') }}</h6>
                    <div class="direction-list my-custom-scrollbar table-wrapper-scroll-y">
                        @if(!$payment)
                            
                                @php 
                                    $dir =  Auth::user() ? \App\Models\Direction::myDirections() : [];
                                @endphp
                        
                                <div class="table">
                                    <table class="table table-sm">
                                        <thead>
                                            @if(!count($dir))
                                                <h6>{{ __('Selecciona la direccion de envío') }}</h6>
                                            @endif
                                        </thead>
                                        <tbody>
                                            
                                            <tr>
                                                @forelse ( \App\Models\Direction::myDirections() as $d )
                                                    <div class="form-check">
                                                        <input class="form-check-input shipping-direction" type="radio" name="directions" value="{{ $d->id }}" id="{{ 'direction-' . $d->id }}" 
                                                        data-route="{{ route('customer.directions.shipping',  ["id" => $d, "_token" => csrf_token() ])}}" @if(session()->get('direction[shipping]') == $d->id) checked @endif>
                                                        <label class="form-check-label" for="{{ 'direction-' . $d->id }}">
                                                            {{ $d->line_1 }} , {{$d->country}} ({{ $d->postal_code }}), {{ $d->city }} {{ $d->state}}
                                                        </label>
                                                    </div>
                                                @empty
                                                    <div class="pt-3">
                                                        <a class="btn btn-dark" href="{{ route('customer.profile', ['user' => Auth::user() ]) }}"> {{ __('Crear dirección')}} </a>
                                                    </div>
                                                    
                                                @endforelse
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
                          
                        @else
                            <hr>
                            @if( \App\Models\Direction::shippingDirection() )
                                <span>{{ \App\Models\Direction::shippingDirection()->name }}</span><br>
                                <span>
                                    {{ \App\Models\Direction::shippingDirection()->line_1 }} <br>
                                    {{\App\Models\Direction::shippingDirection()->country}}
                                    ({{ \App\Models\Direction::shippingDirection()->postal_code }}), {{ \App\Models\Direction::shippingDirection()->city }}
                                    {{ \App\Models\Direction::shippingDirection()->state}}
                                </span>
                            @else
                                <h5> {{ __('Selecciona una dirección de envío en la página anterior') }} </h5>
                                <a href="{{ route('cart') }}"><h6> {{ __('Volver') }} </h6></a>
					        @endif
                        @endif
				   
                    </div>
                    @endauth
                </div>
               
                <div class="widget-item">
                   
                    
                    <h4 class="text-uppercase font-weight-bold text-right">{{ __("Total: :total", ["total" => $cart->totalAmount()]) }} </h4>
                    <h4 class="text-uppercase font-weight-bold text-right" id="shipping_cost">{{ __("Portes: :portes", ["portes" => \App\Helpers\Currency::formatCurrency($cart->portes() * $cart->moreSellers() ) ]) }} </h4>
                    @if($cart->moreSellers() > 1)
                        <h6 class="text-sm-left text-md-right font-weight-bold py-4">{{ Config('mundial_souvenir.text_more_sellers') }}</h6>
                    @endif
                    
                </div>
                <div class="widget-item">
                    <h2 class="text-uppercase text-right font-weight-bold"> {{ __('Total compra')}} </h2>
                    <h1 class="total text-right font-weight-bolder">{{ $cart->totalAmountWithShippingCost() }}</h1>
                    @auth
                        @if($payment)
                            @if( \App\Models\Direction::shippingDirection())
                                <form method="POST" action="{{ route('process_checkout') }}">
                                    @csrf
                                    <button type="submit"  class="btn btn-dark float-right text-uppercase">
                                        {{ __("Pagar") }}
                                    </button>
                                </form>
                               
                                @if(false)
                                    {!! \App\Http\Controllers\RedsysController::index($cart->orderNumber(),$cart->totalAmountWithShippingCost(false),true,'Compra de artículos pedido ' . $cart->orderNumber()) !!}
                                @endif
                                
                                
                            @endif
                        @else
                            @if(count($dir))	
                                <a href="{{ route('checkout_form') }}" class="btn btn-dark float-right text-uppercase">{{ __("Comprar") }}</a>
                            @endif
                        @endif
                    @endauth
                    @guest
                        <a href="javascript:void(0)" onclick="openNavForm()" class="btn btn-dark float-right text-uppercase">{{ __("Logueate") }}</a>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer section end -->


@push('js')
    <script>
		$.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });

		jQuery(document).ready(function () {
			$('.shipping-direction').click(function (e) {
				
				const btn = $(this);
				const route = btn.data("route");

				jQuery.ajax({
					method: "POST",
					url: route,
					success: function (data) {
						location.reload();
					},
					error: function (error) {
						//console.log(error)
					}
				})
    		});
			
            //$('#btn_redsys').addClass('btn btn-dark float-right text-uppercase');
		});

    </script>
    
@endpush