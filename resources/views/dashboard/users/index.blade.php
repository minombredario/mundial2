@extends("layouts.dashboard")

@section('title', __('Usuarios') )

@section("content")
	@include("dashboard.users.list")
@endsection