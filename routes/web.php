<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');

//Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/verify', 'Auth\RegisterController@verifyUser')->name('verify.user');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');


Route::post(
    'stripe/webhook',
    'StripeWebHookController@handleWebhook'
);

Route::group(['prefix' => 'web', 'as' => 'web.'], function() {
    Route::get('/{module}/{cat}', 'HomeController@products')->name('products');
    Route::get('/{module}/{cat}/{product}', 'HomeController@showProduct')->name('products.show');
});

/**
 * CUSTOMER ROUTES
 */

Route::group(['prefix' => 'customer', 'as' => 'customer.', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/profile', 'CustomerController@profile')->name('profile');
    Route::put('/profile', 'CustomerController@updateProfile')->name('profile.update');
    Route::put('/password', 'CustomerController@updatePassword')->name('password.update');

    
    Route::get('directions/json', 'CustomerController@json')->name('directions.json');
    Route::post('directions', 'CustomerController@store')->name('directions.store');
    Route::delete('directions/destroy', 'CustomerController@destroyDirection')->name('directions.destroy');
    Route::put('directions/update', 'CustomerController@updateDirection')->name('directions.update');
    //Route::post('/directions', 'CustomerController@createDirection')->name('directions.create');
    //Route::put('/directions/{direction}', 'CustomerController@updateDirection')->name('directions.update');
    //Route::delete('/directions/{direction}', 'CustomerController@destroyDirection')->name('directions.destroy');

    Route::post('/shipping', 'CustomerController@shippingDirection')->name('directions.shipping');
    Route::get('/get-shipping', 'CustomerController@shippingDirection')->name('directions.get_shipping');

    Route::post("credit-card", 'BillingController@processCreditCardForm')
        ->name("billing.process_credit_card");

    Route::get('/products', 'CustomerController@products')
        ->name('products');

    Route::get('orders/json', 'CustomerController@jsonOrders')->name('orders.json');
    Route::get('/orders', 'CustomerController@orders')->name('orders');
    Route::get('/orders/{order}', 'CustomerController@showOrder')->name('orders.show');
    Route::get('/orders/{order}/download_invoice', 'CustomerController@downloadInvoice')->name('orders.download_invoice');
   
});

Route::get('/', 'HomeController@index')->name('home');

Route::get('/products', 'HomeController@products')->name('products');

 Route::get('customer/order/download', 'CustomerController@downloadInvoiceId')->name('orders.invoice');
/**
 * CART ROUTES
 */
Route::post('/add-product-to-cart/{product}', 'CustomerController@addProductToCart')
    ->name('add_product_to_cart');
Route::get('/cart', 'CustomerController@showCart')
    ->name('cart');
Route::get('/remove-product-from-cart/{product}', 'CustomerController@removeProductFromCart')
    ->name('remove_product_from_cart');
Route::post('/change-quantity-product/{product}', 'CustomerController@changeQuantityProduct')
    ->name('change_quantity_product');


Route::group(["middleware" => ["auth"]], function () {
    Route::get('/checkout', 'CheckoutController@index')
        ->name('checkout_form');
    Route::post('/checkout', 'CheckoutController@processOrder')
        ->name('process_checkout');
});

/**
* Comprobar redsys
*/
Route::post('/redsys/notification','RedsysController@comprobar');
Route::get('/redsys/notification','RedsysController@comprobar');

Route::post('miJqueryAjax','CustomerController@shippingDirection');

