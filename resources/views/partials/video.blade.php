<div class="video_container" id="bgvideo">
	<video autoplay loop id="bgvideo" preload muted style="">
		<source src="{{ $category->backgroundPath() }}" type="video/mp4" />
	</video>
</div>
