<!DOCTYPE html>
<html lang="es">
    <head>
        <title>@yield('title') - {{ config('app.name') }}</title>
        <meta charset="UTF-8">
        <meta name="description" content="Panel de control de {{ config('app.name') }}">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="routeName" content="{{ Route::currentRouteName() }}">

         {{-- ADD CSRF TOKEN FOR AJAX OPERATIONS --}}
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <!-- Favicon -->
        <link href="/img/favicon.ico" rel="shortcut icon"/>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ url('/css/bootstrap5.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fontawesome/all.css') }}">
        <link rel="stylesheet" href="{{ url('/static/css/admin.css?v='.time()) }}">
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap">
        

        

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        @stack('css')

    </head>
    <body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

	
	<div class="wrapper" id="app">
		<div class="col1">
			@include("partials.dashboard.sidebar")
		</div>
		
		<div class="col2">
            @include("partials.dashboard.navbar")
            @include("partials.dashboard.breadcrumb")
            <div class="page">
                @component('components.alert-component')@endcomponent
                @yield('content')
            </div>
		</div>
		
	</div>


    

    <!--====== Javascripts & Jquery ======-->
    <script src="{{ url('/js/fontawesome/all.js') }}"></script> 
    <script src="{{ url('/js/jquery.min.js') }}"></script> 
    <script src="{{ url('/js/jquery-ui.js') }}"></script> 
    <script src="{{ url('/js/popper.min.js') }}"></script> 
    <script src="{{ url('/js/bootstrap5.min.js') }}"></script> 
    <script src="{{ url('/js/jConfirm.js') }}"></script>
    <script src="{{ url('/js/Chart.min.js') }}"></script>
    <script src="{{ url('/js/chartisan_chartjs.js') }}"></script>

   
    
    
    <script>
        jQuery(document).ready(function() {
            $.jConfirm.defaults.question = '{{ __("¿Estás seguro?") }}';
            $.jConfirm.defaults.confirm_text = '{{ __("Sí") }}';
            $.jConfirm.defaults.deny_text = '{{ __("No") }}';
            $.jConfirm.defaults.position = 'top';
            $.jConfirm.defaults.theme = 'black';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        });
        
    </script>

<script src="{{ url('/js/functions.js') }}"></script> 

    @stack('js')
    </body>
</html>
