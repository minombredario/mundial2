jQuery(document).ready(function () {
    
    $('.delete-record').jConfirm().on('confirm', function (e) {
        
        const btn = $(this);
        const route = btn.data("route");
        jQuery.ajax({
            method: "DELETE",
            url: route,
            success: function (data) {
                window.location.reload();
            },
            error: function (error) {
                window.location.reload();
            }
        })
    });

    $('.locked-record').click(function (e) {

        const btn = $(this);
        const route = btn.data("route");
        
        jQuery.ajax({
            method: "DELETE",
            url: route,
            success: function (data) {
                window.location.reload();
            },
            error: function (error) {
                window.location.reload();
            }
        })
    });


    $('.search-record').click( function (e) {
        
        const btn = $(this);
        var route = btn.data("route");
       
        jQuery.ajax({
            method: "POST",
            url: route,
            success: function (data) {
                window.location.reload();
            },
            error: function (error) {
                window.location.reload();
            }
        })
    });

    $('.shipping-direction').click(function (e) {
        
        const btn = $(this);
        const route = btn.data("route");

        jQuery.ajax({
            method: "POST",
            url: route,
            success: function (data) {
                console.log(data)
            },
            error: function (error) {
                console.log(error)
            }
        })
    });

    $('.btn-search').click(function (e) {
       
        e.preventDefault();
        $('.form-search').toggleClass("d-none");     
    });
         
    

    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    
    if(isMobile.Android()) {
		console.log('Esto es un dispositivo Android');
	}

    
});

