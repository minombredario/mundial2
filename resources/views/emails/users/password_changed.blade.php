@component('mail::message')
# Hola {{ $user->name }},
<br><br>
<h2>{{ __('Se ha modificado la contraseña en :web', ['web' => config('app.name') ]) }}</h2>

Atentamente,<br>
{{ config('app.name') }}
@endcomponent