@extends("layouts.dashboard")
@section('title', __('Proveedores') )
<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.providers') }}">
        <i class="fas fa-users"></i> {{ __('Proveedores') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.providers.show', $user) }}">
        <i class="fas fa-id-card"></i> {{ $user->name }} 
    </a>
</li>
@endsection

@section("content")
	
	@include('dashboard.providers.form')
	
@endsection