@extends('errors::illustrated-layout')

@section('code', '404 😭')

@section('title', __('Page Not Found'))

@section('image')

<div style="background-image: url('/storage/web/fondo.jpg');" class="absolute pin bg-no-repeat md:bg-left lg:bg-center">
</div>

@endsection

@section('message', __('Lo sentimos, no se pudo encontrar la página que estás buscando'))
