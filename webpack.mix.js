const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    
    .webpackConfig({
        module: {
            rules: [{
                    test: /\.jsx?$/,
                    exclude: /node_modules(?!\/foundation-sites)|bower_components/,
                    use: [{
                        loader: 'babel-loader',
                        options: Config.babel()
                    }]
                },

                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.(png|jpg)$/,
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]'
                    }
                },

            ]
        },
        resolve: {
            alias: {
                '@': path.resolve('resources/assets/sass')
            }
        },
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
                '@': path.resolve(__dirname, '..')
            }
        }
    })
    .autoload({ jquery: ['$', 'window.jQuery', 'jQuery'] })
    .sass('resources/sass/app.scss', 'public/css')
    .copy('node_modules/jconfirm/jConfirm.js', 'public/js')
    .copy('node_modules/jconfirm/jConfirm.css', 'public/css')
    .copy('node_modules/owl.carousel2/dist/owl.carousel.js', 'public/js')
    .copy('node_modules/owl.carousel2/dist/assets/owl.carousel.min.css', 'public/css')
    .copy('node_modules/owl.carousel2/dist/assets/owl.theme.default.css', 'public/css')
    .copy('node_modules/jquery-validation/dist/jquery.validate.min.js', 'public/js')
    .copy('node_modules/jquery-validation/dist/additional-methods.min.js', 'public/js')

