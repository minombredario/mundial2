<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title ? $title : 'Invoice' }}</title>

    <style>
        body {
            background: #fff none;
            font-size: 12px;
        }

        h2 {
            font-size: 28px;
            color: #ccc;
        }

        .container {
            padding-top: 30px;
        }

        .invoice-head td {
            padding: 0 8px;
        }

        .table th {
            vertical-align: bottom;
            font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        .table tr.row td {
            border-bottom: 1px solid #ddd;
        }

        .table td {
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
        }
        
    </style>
</head>
<body>
<div class="container">
    
    <table style="margin-left: auto; margin-right: auto;" width="550">
		
        <tr>
            <td width="160">
                &nbsp;
            </td>

            <!-- Organization Name / Image -->
            <td align="right">
                
					<img src="http://127.0.0.1:8000/storage/web/logonegro.png" class="img-fluid" width="150"/>
				
               
            </td>
        </tr>
        <tr valign="top">
            <td style="font-size: 28px; color: #ccc;">
               {{ isset($title) ? $title : 'Invoice' }}
            </td>

            <!-- Organization Name / Date -->
            <td>
                <br><br>
                <strong>{{ __('Para')}}:</strong> {{ $owner->name }}
                <br>
                <strong>Email:</strong> {{ $owner->stripeEmail() ?: $owner->name }}
                <br>
                <strong>{{ __('Fecha')}}:</strong> {{ $invoice->date()->toFormattedDateString() }}
            
                <br><br>
                <strong>{{ __('Dirección')}}:</strong>
                {{ $data_direction->line_1 . ', ' . $data_direction->postal_code . ' ' . $data_direction->country . '-' . $data_direction->city }}
                <br>
                <strong>{{ __('Teléfono')}}:</strong> {{ $data_direction->phone }}
                <br>
                <strong>{{ __('DNI/NIF')}}:</strong> {{ $data_direction->nif }}
            </td>
        </tr>
        <tr valign="top">
            <!-- Organization Details -->
            <td style="font-size:9px;">
                <h4>{{ $vendor }}</h4><br>

                @if (isset($street))
                    {{ $street }}<br>
                @endif

                @if (isset($location))
                    {{ $location }}<br>
                @endif

                @if (isset($phone))
                    <strong>T</strong> {{ $phone }}<br>
                @endif

                @if (isset($vendorVat))
                    {{ $vendorVat }}<br>
                @endif

                @if (isset($url))
                    <a href="{{ $url }}">{{ $url }}</a>
                @endif
            </td>
            <td>
                <!-- Invoice Info -->
                <p>
                    <strong>Product:</strong> {{ $product }}<br>
                    <strong>Invoice Number:</strong> {{ $id ?? $invoice->number }}<br>
                </p>

                <!-- Extra / VAT Information -->
                @if (isset($vat))
                    <p>
                        {{ $vat }}
                    </p>
                @endif

                <br><br>

                <!-- Invoice Table -->
                <table width="100%" class="table">
                    <tr>
                        <th>{{ __('Artículo') }}</th>
                        <th>{{ __('Cantidad') }}</th>
						<th>{{ __('Precio') }}</th>
						<th>{{ __('Descuento') }}</th>
                        <th>{{ __('Total') }}</th>
                    </tr>

                    <!-- Display The Invoice Items -->
                  
                    @foreach ($order->order_lines as $item)
                    
                        <tr class="row">
                            <td>{{ $item->product->name }}</td>
							<td>{{ $item->quantity }}</td>
							<td>{{ $item->formatted_price }}</td>
							<td>{{ $item->product->discount . \App\Models\Product::discountIcon($item->product->discount_type) }}</td>
                            <td>{{ \App\Helpers\Currency::formatCurrency($item->price_total) }}</td>
                        </tr>
                    @endforeach

                    <!-- Display The Subscriptions -->
                    @foreach ($invoice->subscriptions() as $subscription)
                        <tr class="row">
                            <td>Subscription ({{ $subscription->quantity }})</td>
                            <td>
                                {{ $subscription->startDateAsCarbon()->formatLocalized('%B %e, %Y') }} -
                                {{ $subscription->endDateAsCarbon()->formatLocalized('%B %e, %Y') }}
                            </td>

                            @if ($invoice->hasTax())
                                <td>
                                    @if ($inclusiveTaxPercentage = $subscription->inclusiveTaxPercentage())
                                        {{ $inclusiveTaxPercentage }}% incl.
                                    @endif

                                    @if ($subscription->hasBothInclusiveAndExclusiveTax())
                                        +
                                    @endif

                                    @if ($exclusiveTaxPercentage = $subscription->exclusiveTaxPercentage())
                                        {{ $exclusiveTaxPercentage }}%
                                    @endif
                                </td>
                            @endif

                            <td>{{ $subscription->total() }}</td>
                        </tr>
                    @endforeach

                    <!-- Display The Subtotal -->
                    @if ($invoice->hasDiscount() || $invoice->hasTax() || $invoice->hasStartingBalance())
                        <tr>
                            <td colspan="{{ $invoice->hasTax() ? 4 : 2 }}" style="text-align: right;">Subtotal</td>
                            <td>{{ $invoice->subtotal() }}</td>
                        </tr>
                    @endif

                    <!-- Display The Discount -->
                    @if ($invoice->hasDiscount())
                        <tr>
                            <td colspan="{{ $invoice->hasTax() ? 3 : 2 }}" style="text-align: right;">
                                @if ($invoice->discountIsPercentage())
                                    {{ $invoice->coupon() }} ({{ $invoice->percentOff() }}% Off)
                                @else
                                    {{ $invoice->coupon() }} ({{ $invoice->amountOff() }} Off)
                                @endif
                            </td>

                            <td>-{{ $invoice->discount() }}</td>
                        </tr>
                    @endif

                    <!-- Display The Taxes -->
                    @unless ($invoice->isNotTaxExempt())
                        <tr>
                            <td colspan="{{ $invoice->hasTax() ? 3 : 2 }}" style="text-align: right;">
                                @if ($invoice->isTaxExempt())
                                    Tax is exempted
                                @else
                                    Tax to be paid on reverse charge basis
                                @endif
                            </td>
                            <td></td>
                        </tr>
                    @else
                        @foreach ($invoice->taxes() as $tax)
                            <!--tr>
                                <td colspan="3" style="text-align: right;">
                                    {{ $tax->display_name }} {{ $tax->jurisdiction ? ' - '.$tax->jurisdiction : '' }}
                                    ({{ $tax->percentage }}%{{ $tax->isInclusive() ? ' incl.' : '' }})
                                </td>
                                <td>{{ $tax->amount() }}</td>
                            </!--tr-->
                        @endforeach
                    @endunless

                    <!-- Starting Balance -->
                    @if ($invoice->hasStartingBalance())
                        <tr>
                            <td colspan="{{ $invoice->hasTax() ? 3 : 2 }}" style="text-align: right;">
                                Customer Balance
                            </td>
                            <td>{{ $invoice->startingBalance() }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="{{ $invoice->hasTax() ? 4 : 2 }}" style="text-align: right;">
                            Portes
                        </td>
                        <td>
                           {{ \App\Helpers\Currency::formatCurrency( $order->shipping_cost ) }}
                        </td>
                    </tr>
                    <!-- Display The Final Total -->
                    <tr>
                        <td colspan="{{ $invoice->hasTax() ? 4 : 2 }}" style="text-align: right;">
                            <strong>Total</strong>
                        </td>
                        <td>
                            <strong>{{ $order->formatted_total_amount }}</strong>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
