@extends('layouts.web')

@push('css')
    <style>
        .tab-pane{
            background: #f5f6fa;
            padding: 30px;
            border-radius: 0 0 20px 20px;
        }
        .nav-tabs{
           background: #181818;
        }

        .nav-item a{
            color: #fff;
        }

        .table tr {
            cursor: pointer;
        }
        .hiddenRow {
            padding: 0 !important;
        }
        .my-custom-scrollbar {
            position: relative;
            height: calc( 100vh/2 );
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
@endpush

@section('content')
<!-- profile section -->
<section class="profile">
    @include('partials.form_errors')
    <div class="container wrapper-container">
     
        <div class="row d-flex justify-content-center g-0">
            
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true"> <i class="far fa-user"></i> {{ __('Mis datos') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="true"> <i class="fas fa-fingerprint"></i> {{ __('Contraseña') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="directions-tab" data-toggle="tab" href="#directions" role="tab" aria-controls="directions" aria-selected="false"><i class="fas fa-truck-moving"></i> {{ __('Mis direcciones') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="card-tab" data-toggle="tab" href="#card" role="tab" aria-controls="card" aria-selected="false"><i class="fa fa-credit-card"></i> {{ __('Datos de pago') }}</a>
                </li>

                 <li class="nav-item">
                    <a class="nav-link" id="order-tab" data-toggle="tab" href="#orders" role="tab" aria-controls="card" aria-selected="false"><i class="fas fa-tasks"></i> {{ __('Mis pedidos') }}</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    @include('partials.web.customer.profile.data')
                </div>

                <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
                    @include('partials.web.customer.profile.password')
                </div>

                <div class="tab-pane fade" id="directions" role="tabpanel" aria-labelledby="directions-tab">
                    @include('partials.web.customer.profile.directions')
                </div>

                <div class="tab-pane fade" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                    @include('partials.web.customer.profile.orders')
                </div>

                <div class="tab-pane fade" id="card" role="tabpanel" aria-labelledby="card-tab">
                    @include('partials.web.customer.profile.card')
                </div>
            </div>
        </div>
    
    </div>
</section>
<!-- profile end section -->
@endsection


@push('js')
    <script>
        jQuery(document).ready(function () {
           
            $('.btn-direction').on('click', function(){
                $('.form-direction').toggleClass('d-none');
                 $('.my-custom-scrollbar').toggleClass('d-none');
            })
          
        });
    </script>
    
@endpush

