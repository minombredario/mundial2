<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class VerificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public User $user;

    public function __construct(User $user)
    {
       
        $this->user = $user;
      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->from(env('MAIL_USERNAME'))->subject(__('¡Bienvenido a MundialSouvenir!'))->view('emails.users.verification');
        return $this
            ->subject("Verifica tu cuenta! - " . config("app.name"))
            ->markdown('emails.users.verification');
    }
}
