@extends("layouts.dashboard")
@section('title', __('Usuarios') )
<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.users') }}">
        <i class="fas fa-users"></i> {{ __('Usuarios') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.users.show', $user) }}">
        <i class="fas fa-id-card"></i> {{ $user->name }} 
    </a>
</li>
@endsection

@section("content")
	
	@include('dashboard.users.form')
	
@endsection