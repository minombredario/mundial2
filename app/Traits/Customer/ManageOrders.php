<?php
namespace App\Traits\Customer;

use App\Models\Order;
use App\Models\Direction;
use App\Models\User;
use Illuminate\Http\Request;
use Auth, DB;

use App\Http\Resources\Order as OrderResource;

trait ManageOrders {
    public function orders() {
        $orders = auth()->user()->processedOrders();
        return view('web.customer.orders.index', compact('orders'));
    }

    public function jsonOrders() {

        if (request()->ajax()) {
            $user = auth()->user();    
            $orders = Order::where('user_id',$user->id)->with('order_lines.product')
                ->orderBy("created_at", "DESC")
                ->paginate(4);
           
            return OrderResource::collection($orders);
        }
        abort(401);
    }


    public function showOrder(Order $order) {
        $order
            ->load("order_lines.product")
            ->loadCount("order_lines");
        return view('web.customer.orders.show', compact('order'));
    }

    public function downloadInvoice(Order $order) {
        try {
            if ($order->user_id !== auth()->id()) {
                session()->flash("message", ["danger", __("No tienes acceso a esta factura")]);
                return back();
            }
            $data_direction = Direction::findOrFail($order->direction_id);
            $admin = User::where('role',User::ADMIN)->with('profile')->with('defaultDirection')->first();

            $order->load('order_lines.product');
            //dd($admin);
            
            if(count($admin->defaultDirection)){
                
                return auth()->user()->downloadInvoice($order->invoice_id, [
                    'vendor' => $admin->profile->company,
                    'product' => __("Compra de productos"),
                    'street' => $admin->defaultDirection[0]->line_1,
                    'location' => $admin->defaultDirection[0]->postal_code . " " . $admin->defaultDirection[0]->country . " (" . $admin->defaultDirection[0]->city . ")",
                    'phone' => $admin->defaultDirection[0]->phone,
                    'vendorVat' => $admin->nif,
                    'url' => "www.mundialsouvenir.es",
                    'title' => __('Factura'),
                    'data_direction' => $data_direction,
                    'order' => $order
                    
    
                ], env('app.name') . $order->created_at->format("d/m/Y") );
            }else{
                return auth()->user()->downloadInvoice($order->invoice_id, [
                    'vendor' => $admin->profile->company,
                    'product' => __("Compra de productos"),
                    'vendorVat' => $admin->nif,
                    'url' => "www.mundialsouvenir.es",
                    'title' => __('Factura'),
                    'data_direction' => $data_direction,
                    'order' => $order
                    
                ], config('app.name') . " - " . $order->created_at->format("d/m/Y") );
    
            }

            
            

        } catch (\Exception $exception) {
            session()->flash("message", ["danger", __("Ha ocurrido un error descargando la factura")]);
            return back();
        }
    }

    public function downloadInvoiceId(Request $request) {
        
        $order = Order::whereId(request('id'))->first();
        
        $pdf = null;
        try {
            if ($order->user_id !== auth()->id()) {
                session()->flash("message", ["danger", __("No tienes acceso a este producto")]);
                return back();
            }
            $data_direction = Direction::findOrFail($order->direction_id);
            $admin = User::where('role',User::ADMIN)->with('profile')->with('defaultDirection')->first();

            $order->load('order_lines.product');
            //dd($admin);
            
            if(count($admin->defaultDirection)){
                
                return auth()->user()->downloadInvoice($order->invoice_id, [
                    'vendor' => $admin->profile->company,
                    'product' => __("Compra de productos"),
                    'street' => $admin->defaultDirection[0]->line_1,
                    'location' => $admin->defaultDirection[0]->postal_code . " " . $admin->defaultDirection[0]->country . " (" . $admin->defaultDirection[0]->city . ")",
                    'phone' => $admin->defaultDirection[0]->phone,
                    'vendorVat' => $admin->defaultDirection[0]->nif,
                    'mail' => $admin->email,
                    'title' => __('Factura'),
                    'data_direction' => $data_direction,
                    'order' => $order
                    
    
                ], env('app.name') . $order->created_at->format("d/m/Y") );
            }else{
                return auth()->user()->downloadInvoice($order->invoice_id, [
                    'vendor' => $admin->profile->company,
                    'product' => __("Compra de productos"),
                    'mail' => $admin->email,
                    'title' => __('Factura'),
                    'data_direction' => $data_direction,
                    'order' => $order
                    
                ], config('app.name') . " - " . $order->created_at->format("d/m/Y") );
    
            }

            
            

        } catch (\Exception $exception) {
            session()->flash("message", ["danger", __("Ha ocurrido un error descargando la factura")]);
            return back();
        }

        

    }
}
