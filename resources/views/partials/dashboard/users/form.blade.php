{!! Form::label('name', __("Nombre:")) !!}
<div class="input-group mb-3" >
	@can('haveaccess', 'user.edit')
	{!! Form::label('name', '<i class="far fa-address-card"></i>', [ 'class' => 'input-group-text'], false) !!}
	{!! Form::text('name', null, ['class' => 'form-control' .($errors->has('name') ? ' is-invalid' : '') ]) !!}
	@elsecan('haveaccess', 'user.show')
	<span class="title"><i class="far fa-address-card"></i> {{__('Nombre:') }}</span>
	<span class="text">{{ $user->name }} {{ $user->lastname}}</span>
	@endcan
	
</div>
@if ($errors->has('name'))
	<div class="{{$errors->has('name') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('name') ? 'display:block' : ''}}">
		<strong>{{ $errors->first('name') }}</strong>
	</div>
@endif

{!! Form::label('email', __("Correo electrónico:")) !!}
<div class="input-group mb-3" >
	{!! Form::label('email', '<i class="far fa-envelope"></i>', [ 'class' => 'input-group-text'], false) !!}
	{!! Form::email('email', null, ['class' => 'form-control' .($errors->has('email') ? ' is-invalid' : '') ]) !!}
</div>
@if ($errors->has('email'))
	<div class="{{$errors->has('email') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('email') ? 'display:block' : ''}}">
		<strong>{{ $errors->first('email') }}</strong>
	</div>
@endif

{!! Form::label('password', __("Password:")) !!}
<div class="input-group mb-3">
	{!! Form::label('password', '<i class="fas fa-key"></i>', [ 'class' => 'input-group-text'], false) !!}
		<input id="password" type="password" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" name="password" $required autocomplete="new-password">

		@if ($errors->has('password'))
			<div class="{{$errors->has('password') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('password') ? 'display:block' : ''}}">
				<strong>{{ $errors->first('password') }}</strong>
			</div>
		@endif
	
</div>

{!! Form::label('password', __("Confirmar Password:")) !!}
<div class="input-group mb-3">
	{!! Form::label('password_confirmation', '<i class="fas fa-key"></i>', [ 'class' => 'input-group-text'], false) !!}
	<input id="password-confirm" type="password" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}"  name="password_confirmation" $required autocomplete="new-password">
	
	@if ($errors->has('password_confirmation'))
			<div class="{{$errors->has('password_confirmation') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('password_confirmation') ? 'display:block' : ''}}">
				<strong>{{ $errors->first('password_confirmation') }}</strong>
			</div>
		@endif

</div>	


							
							