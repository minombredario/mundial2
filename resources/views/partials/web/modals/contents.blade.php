@push('css')
    <style>
       .modal-content{
            background: rgb(255, 255, 255,0.75) !important;
            color: #000;
            box-shadow: 0px 0px 10px 2px #ddd9d9;
        }

        .modal-title{
            color: #000;
        }
    </style>
@endpush

<div class="modal fade" id="content-modal" tabindex="-1" role="dialog" aria-labelledby="content-title" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center d-block">
                <h5 class="modal-title text-uppercase" id="content-title"></h5>
                <button type="button" class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-justify" id="content-body">
              
                
            </div>
        </div>
    </div>
</div>
