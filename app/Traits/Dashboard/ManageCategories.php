<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use App\Helpers\Uploader;
use DB;
use Str;

trait ManageCategories {

	public function categories(){

        $this->authorize('haveaccess','category.index');

        $categories = Category::filtered();
        $category = new Category;
        $title = __("Crear categoría");
        $textButton = __("Guardar");
        $options = ['route' => ['admin.dashboard.categories.store'], 'files' => true];
        $required = false;
        return view('dashboard.categories.index', compact('title', 'category', 'options', 'textButton', 'required', 'categories'));  

    }

    public function searchCategory(){
               
        session()->remove('search[admin.dashboard]');
        if (request('search')) {
            session()->put('search[admin.dashboard]', request('search'));
            session()->save();
        }
       
        return redirect(route('admin.dashboard.categories'));
        
    }
    
    public function storeCategory(CategoryRequest $request) {
        /* error subir imagenes
            $error_types = array(
                1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
                'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
                'The uploaded file was only partially uploaded.',
                'No file was uploaded.',
                6 => 'Missing a temporary folder.',
                'Failed to write file to disk.',
                'A PHP extension stopped the file upload.'
            );

            // Outside a loop...
            if ($_FILES['background']['error'] == 0) {
                // here background is the name
                // i.e(<input type="file" name="*background*" size="30" id="background">
                dd("no error ");
            } else {
                $error_message = $error_types[$_FILES['background']['error']];
                dd($error_message);
            }

        */
        $this->authorize('haveaccess','category.create');
        
        try {
            DB::beginTransaction();
            $file = null;
            if ($request->hasFile('background')) {
                $file = Uploader::uploadFile('background', 'categories');
            }
            if ($request->hasFile('background_image')) {
                $file2 = Uploader::uploadFile('background_image', 'categories');
            }
            
            $category = Category::create($this->categoryInput($file, $file2));
           
            DB::commit();
            session()->flash("message", ["success", __("Categoría creada satisfactoriamente")]);
            return redirect(route('admin.dashboard.categories', ['category' => $category]));
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function editCategory(Category $category) {

        $this->authorize('haveaccess','category.edit');

        $title = __("Editar la categoría :category", ["category" => $category->name]);
        $textButton = __("Actualizar");
        $options = ['route' => ['admin.dashboard.categories.update', ["category" => $category]], 'files' => true];
        $update = true;
        $required = true;
        return view('dashboard.categories.edit', compact('title', 'category', 'options', 'textButton', 'update', 'required'));
    }

    public function updateCategory(CategoryRequest $request, Category $category) {
        
        $this->authorize('haveaccess','category.edit');

        try {
            DB::beginTransaction();

            $file = $category->background;
            if ($request->hasFile('background')) {
                if ($category->background) {
                    Uploader::removeFile("categories", $category->background);
                }
                $file = Uploader::uploadFile('background', 'categories');
            }
            $file2 = $category->background_image;
            if ($request->hasFile('background_image')) {
                if ($category->background_image) {
                    Uploader::removeFile("categories", $category->background_image);
                }
                $file2 = Uploader::uploadFile('background_image', 'categories');
            }

            
            $category->fill($this->categoryInput($file, $file2))->save();
            
            DB::commit();

            session()->flash("message", ["success", __("Categoría actualizada satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function destroyCategory(Category $category){   

        $this->authorize('haveaccess','category.destroy');
        
        try {
            if (request()->ajax()) {
               
                $category->delete();
                session()->flash("message", ["success", __("Categoría :category ha sido eliminada correctamente", ["category" => $category->name])]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }
    }

    protected function categoryInput(string $file = null, string $file2 = null): array {

        return [
            'name' => e(request('name')),
            'slug' => Str::slug(request('name')),
            'background' => $file,
            'background_image' => $file2,
            'module_id' => request('module_id'),
            
        ];
    }
    
}