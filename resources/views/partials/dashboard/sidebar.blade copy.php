<div class="sidebar shadow">
	<div class="section-top">
		<div class="logo">
			
			<img src="{{ '/storage/web/logoblanco.png' }}"  class="img-fluid"/>
			
		</div>
		<div class="user">
			<div class="name">
				{{ Auth::user()->name }}
				<br>
				<a href="{{ route('logout') }}" data-toggle="tooltip" data-placemente="top" title="Salir"
					onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
					<i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
				</a>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
					@csrf
				</form>
				
			</div>
		</div>
	</div>
	<div class="main  flex-column">	
		<ul>
			@can('haveaccess', 'dashboard.index')
			<li>
				<a href="{{ route("admin.dashboard.index") }}" class="index"><i class="fas fa-home"> </i> {{ __('Dashboard') }} </a>
			</li>
			@endcan
			@can('haveaccess', 'userown.edit')
			<li>
				<a href="{{ route("admin.dashboard.profile") }}" class="profile"><i class="fas fa-id-card"></i> {{ __('Mi perfil') }}</a>
			</li>
			@endcan
			@admin
			<li>
				<a href="{{ route("admin.dashboard.settings") }}" class="settings"> <i class="fas fa-cogs"></i> {{ __('Configuración') }}</a>
			</li>
			@endadmin

			@admin
			<li>
				<a href="{{ route("admin.dashboard.contents") }}" class="contents"> <i class="far fa-newspaper"></i> {{ __('Políticas') }}</a>
			</li>
			@endadmin

			@can('haveaccess', 'product.index')
			<li>
				<a href="{{ route("admin.dashboard.products") }}" class="products"><i class="fas fa-boxes"></i> {{ __('Productos') }}</a>
			</li>
			@endcan
			@can('haveaccess', 'category.index')
			<li>
				<a href="{{ route("admin.dashboard.categories") }}" class="categories"><i class="fas fa-folder-open"></i> {{ __('Categorías') }}</a>
			</li>
			@endcan
			@can('haveaccess', 'user.index')
			<li>
				<a href="{{ route("admin.dashboard.users") }}" class="users"><i class="fas fa-user-friends"></i> {{ __('Clientes') }}</a>
			</li>
				@admin
				
					<li>
						<a href="{{ route("admin.dashboard.sellers" ) }}" class="sellers"><i class="fas fa-user-friends"></i> {{ __('Distribuidores') }}</a>
					</li>
					<li>
						<a href="{{ route("admin.dashboard.providers") }}" class="providers"><i class="fas fa-user-friends"></i> {{ __('Proveedores') }}</a>
					</li>
			
				@endadmin
			@endcan
			
			@can('haveaccess', 'order.index')
			<li>
				<a href="{{ route("admin.dashboard.orders") }}" class="orders"><i class="fas fa-shopping-basket"></i> {{ __('Pedidos') }}</a>
			</li>
			@endcan
			@admin
			<li>
				<a href="{{ route("admin.dashboard.roles") }}" class="roles"><i class="fas fa-user-shield"></i> {{ __('Roles') }}</a>
			</li>
			@endadmin
		</ul>
	</div>
</div>


@push("js")
    
    <script>
		
		var route = $('[name=routeName]')[0].getAttribute('content').split('.');
		
		route = route[2];

        var route_active = $(`.${route}`).toggleClass('active');
        
    </script>
@endpush
