<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case "POST": {
                return [
                    'name' => 'required|min:5|unique:products',
                    'categories' => 'required',
                    'description' => 'required|min:10',
					'price' => 'required|gt:0',
					'in_discount' => request('in_discount') != null ? 'required|boolean' : '',
					'discount_type' => [
                        Rule::in(Product::PERCENT, Product::PRICE)
                    ],
					'discount' => 'sometimes', //Rule::requiredIf(request->indiscount== true), //'bail|required_if:indiscount,true|numeric|gt:0',
					'stock' => 'sometimes|required|numeric',
					'code' => 'sometimes',
					'picture' => 'required|image|mimes:jpg,jpeg,png'
				];
            }
            case "PUT": {
                return [
                    'name' => 'required|min:5|unique:products,name,' . $this->route('product')->id,
                    'categories' => 'required',
                    'description' => 'required|min:10',
					'price' => 'required|gt:0',
					'in_discount' => request('in_discount') != null ? 'required|boolean' : '',
					'discount_type' => [
                        Rule::in(Product::PERCENT, Product::PRICE)
                    ],
					'discount' => 'sometimes', //Rule::requiredIf(request->indiscount== true), //'bail|required_if:indiscount,true|numeric|gt:0',
					'stock' => 'required|numeric',
					'code' => 'sometimes',
					'picture' => 'required|sometimes|image|mimes:jpg,jpeg,png'
                ];
            }
            default: {
                return [];
            }
        }
    }

}

