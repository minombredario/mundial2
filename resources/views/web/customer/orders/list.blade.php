<!-- orders section -->
<section class="profile">
    <div class="container wrapper-container">
        <div class="row row-content">
            <div class="table-responsive">
                <table class="table">
                    @include("partials.web.customer.orders.order_thead", ["detail" => false])
                    <tbody>
                        @forelse($orders as $order)
                            @include("partials.web.customer.orders.order_row", ["detail" => false])
                        @empty
                            <tr class="text-center">
                                <td colspan="6">
                                    <div class="text-center">
                                        {!! __("No tienes ningún pedido todavía") !!}
                                    </div>
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            @if(count($orders))
                {{ $orders->links() }}
            @endif
        </div>
    </div>
</section>
<!-- orders end section -->