<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Passwords\PasswordBroker;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    // Comentamos esto que no nos hace falta
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }
 
    // Añadimos las respuestas JSON, ya que el Frontend solo recibe JSON
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
 
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );
       
        switch ($response) {
            case PasswordBroker::INVALID_USER:
                return redirect("/")->with(session()->flash('alert-danger', $response));
                return response()->json($response, 422);
                break;
 
            case PasswordBroker::INVALID_TOKEN:
                return redirect("/")->with(session()->flash('alert-danger', $response));
                break;
            default: 
                return redirect("/")->with(session()->flash('alert-success', 'Se ha enviado un e-mail para la recuperación de la contraseña.'));
                //return response()->success($response, 200);
        }
    }
}
