@extends("layouts.dashboard")

@section('title', __("Producto") )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.products') }}">
        <i class="fas fa-boxes"></i> {{ __('Productos') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.products.create') }}">
        <i class="fas fa-box"></i> {{ __("Crear Producto") }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.products.form", ['required' => true])
@endsection