<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Direction;
use App\Models\Profile;
use App\Helpers\Uploader;
use Hash;
use DB;
use Str;

trait ManageProfile {

    public function profile(){
		$user = Auth()->user();
		$user->load("profile");
		$user->load("directions");
		
		$title = __(":title", [ "title" => $user->profile ? $user->profile->company : '' ]); 
        $textButton = __("Actualizar");
        $options = ['route' => ['admin.dashboard.profile.user.update', ["user" => $user]], 'files' => true];
		$required = true;
		$update = true;
		return view('dashboard.profile.index', compact('user', 'title', 'textButton', 'options', 'required'));
	}

	public function updateProfile(Request $request, User $user) {
		
		//dd($user->profile);
		try {
			DB::beginTransaction();
				//update data 
				$user->fill($this->profileUserInput())->save();

				//update data profile
				if($user->profile){
					$file_1 = $user->profile->logo_1;
					$file_2 = $user->profile->logo_2;

					if ($request->hasFile('logo_1')) {
						$file_1 = 'logoblanco.' . request()->file('logo_1')->getClientOriginalExtension();
						if ($user->profile->logo_1) {
							Storage::delete(sprintf('%s/%s', 'users/profile', $user->profile->logo_1));
						}
						request()->file('logo_1')->storeAs('users/profile', $file_1);
					}

					if ($request->hasFile('logo_2')) {
						$file_2 = 'logonegro.' . request()->file('logo_2')->getClientOriginalExtension();
						if ($user->profile->logo_2) {
							Storage::delete(sprintf('%s/%s', 'users/profile', $user->profile->logo_2));
						}
						request()->file('logo_2')->storeAs('users/profile', $file_2);
					}
			
					$user->profile->fill($this->profileInput($file_1, $file_2))->save();

				}else{
					$file_1 = null;
					$file_2 = null;

					if ($request->hasFile('logo_1')) {
						$file_1 = 'logoblanco.' . request()->file('logo_1')->getClientOriginalExtension();
						request()->file('logo_1')->storeAs('users/profile', $file_1);
					}

					if ($request->hasFile('logo_2')) {
						$file_2 = 'logonegro.' . request()->file('logo_2')->getClientOriginalExtension();
						request()->file('logo_2')->storeAs('users/profile', $file_2);
					}
            
            		$user = Profile::create($this->profileInput($file_1, $file_2));
				}
				
				//update data direction
				if(count($user->directions)){
					
					Direction::where('user_id', $user->id)->where('id','!=')->update(['default' => null]);
					$user->directions[0]->fill($this->profileDirectionInput())->save();
				}else{
					
					Direction::where('user_id', $user->id)->update(['default' => null]);
					
					$direction = Direction::create($this->profileDirectionInput());
					
					$direction->update(['default' => 1]);
					
				}
				
			DB::commit();

			session()->flash("message", ["success", __("Datos actualizados satisfactoriamente")]);
			
			return back();
			
		} catch (\Throwable $exception) {
			DB::rollBack();
			session()->flash("message", ["danger", $exception->getMessage()]);
			return back();
		}
		
		
		
	}

	protected function profileUserInput(): array {

        return [
             
            "name" => e(request("name")),
			"email" => e(request("email")),
			"nif" => e(request("nif")),
			//"password" => Hash::make(request('password')),
            //"password_code" => e(request("password")),
        ];
	}

	protected function profileInput(string $file_1 = null, string $file_2 = null): array {

        return [
			"company" => e(request("company")),
			"phone_1" => e(request("phone_1")),
			"phone_2" => e(request("phone_2")),
			"logo_1" => $file_1,
			"logo_2" => $file_2,
        ];
	}
	
	protected function profileDirectionInput(): array {

        return [
			"name" => e(request("name")),
			"nif" => e(request("nif")),
			'default' => 1,
            "phone" => e(request("phone_1")),
			"line_1" => e(request("line_1")),
			"line_2" => e(request("line_2")),
			"postal_code" => e(request("postal_code")),
			"country" => e(request("country")),
			"city" => e(request("city")),
			"state" => e(request("state")),
			
        ];
    }
}