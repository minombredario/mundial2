
<div class="table-wrapper-scroll-y my-custom-scrollbar">
    
        <!--directions-component :data='json($directions)'></!--directions-component-->
        <directions :postalcode="{{ json_encode(\App\Helpers\Functions::postalCode()) }}" :municipios="{{ json_encode(\App\Helpers\Functions::municipios()) }}"></directions>
    
</div>


