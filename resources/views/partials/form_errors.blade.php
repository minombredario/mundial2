@if ($errors->any())
    <div class="container-fluid">
        <div class="alert alert-danger mtop16" style="display:block; margin-bottom: 16px">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
        
    </div>
@endif
