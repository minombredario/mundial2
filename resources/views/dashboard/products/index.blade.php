@extends("layouts.dashboard")

@section('title', __('Productos') )

@section("content")
    @include("dashboard.products.list")
@endsection