
<div class="mx-auto pt-4">
    <form role="form" action="{{ route('customer.billing.process_credit_card') }}" method="POST" >
       
        @csrf
        <div class="row">
            <div class="col-md-6 mt-2">
                <div class="form-group">
                    <label for="card_number">{{ __("Número de la tarjeta") }}</label>
                    <div class="input-group">
                        <input type="text" name="card_number" placeholder="{{ __("Número de la tarjeta") }}" class="form-control" required 
                            value="{{
                                old('card_number') ?
                                old('card_number') :
                                (auth()->user()->card_last_four ? '************' . auth()->user()->card_last_four : null)
                            }}"
                        />
                        
                            <span class="input-group-text">
                                <i class="fab fa-cc-visa mx-1"></i>
                                <i class="fab fa-cc-mastercard mx-1"></i>
                            </span>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3 mt-2">
                <div class="form-group">
                    <label>
                        <span class="hidden-xs">{{ __("Fecha expiración") }}</span>
                    </label>
                        <div class="input-group">
                            <input type="number" placeholder="{{ __("MM") }}" min="01" max="12" name="card_exp_month" value="{{ auth()->user()->card_exp_month }}" class="form-control" required />
                            <input type="number" placeholder="{{ __("YY") }}" min="{{ date("y") }}" name="card_exp_year" value="{{ auth()->user()->card_exp_year }}" class="form-control" required />
                        </div>
                </div>
            </div>
            <div class="col-md-3 mt-2">
                <div class="form-group mb-4">
                    <label >{{ __("CVC") }}</label>
                    <input type="text" name="cvc" value="{{ auth()->user()->cvc }}" class="form-control" required >
                </div>
            </div>
        </div>
        

                    

        <button type="submit" class="btn btn-dark shadow-sm">
            {{ __("Guardar tarjeta") }}
        </button>
    </form>
</div>    
            
                    
