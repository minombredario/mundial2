<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\User;
use App\Models\Role;
use App\Http\Requests\UserRequest;
use App\Helpers\Uploader;
use Hash;
use DB;
use Str;

trait ManageSellers {

	public function sellers(){

        
        $this->authorize('haveaccess','user.index');

        $users = User::FilteredSeller(User::SELLER);
        return view('dashboard.sellers.index', compact('users'));
    }

    public function showSeller(User $user) {

        $this->authorize('haveaccess','user.show');

        $title = __('<i class="fas fa-id-card"></i> Distribuidor :user', ["user" => $user->name]);
        $view = 'show';
        $textButton = '';
        $options = [];
        return view('dashboard.sellers.show', compact('title', 'user','view', 'options','textButton'));

    }

    public function createSeller() {

        $this->authorize('haveaccess','user.create');

        $user = new User;
        $user->role = User::SELLER;
        $user->status = "100";
        $title = __('<i class="fas fa-user-plus"></i> Crear distribuidor');
        $textButton = __("Guardar");
        $options = ['route' => ['admin.dashboard.sellers.store'], 'files' => true];
        $required = false;
        $view = 'post';
        $password = true;
        return view('dashboard.sellers.create', compact('title','view', 'user', 'options', 'textButton', 'required', 'password'));

    }

    public function storeSeller(UserRequest $request) {
      
        $this->authorize('haveaccess','user.create');

        try {
            DB::beginTransaction();
            
           
            $file = null;
            if ($request->hasFile('avatar')) {
                $file = Uploader::uploadFile('avatar', 'users');
            }
            
            $user = User::create($this->sellerInput($file));

            $role = Role::whereName(request("role"))->pluck('id')->first();
            
            $user->roles()->sync($role);
           
            DB::commit();

            session()->flash("message", ["success", __("Distribuidor creado satisfactoriamente")]);
            return redirect(route('admin.dashboard.sellers.edit', ['user' => $user]));
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function editSeller(User $user) {
        
        $this->authorize('haveaccess','user.edit');

        $title = __('<i class="fas fa-user-edit"></i> Editar :user', ["user" => $user->name]);
        $textButton = __("Actualizar");
        $options = ['route' => ['admin.dashboard.sellers.update', ["user" => $user]], 'files' => true];
        $view = 'put';
        $required = false;
        $update = true;
        $password = false;
        return view('dashboard.sellers.edit', compact('title', 'user', 'options', 'textButton', 'view', 'required', 'update', 'password'));
    }

    public function updateSeller(UserRequest $request, User $user) {

        $this->authorize('haveaccess','users.edit');
        
        try {
            DB::beginTransaction();
            
                $user->fill([
                    "role" => e(request("role")),
                    "status" => e(request("status")),
                ])->save();

                $role = Role::whereName(request("role"))->pluck('id')->first();
                
                $user->roles()->sync($role);
            DB::commit();

            session()->flash("message", ["success", __("Usuario actualizado satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function destroySeller(User $user) {
       
        $this->authorize('haveaccess','user.destroy');

        try {
            if (request()->ajax()) {
                $user->status = $user->status == 100 ? 1 : 100;
                $user->save();
                session()->flash("message", ["success", __("El distribuidor :user ha sido :locked correctamente", ["user" => $user->name, 'locked' => $user->status == 100 ? 'desbloquedo' : 'bloqueado'] )]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }
    }

    protected function sellerInput(string $file = null): array {

        return [
             
            "name" => e(request("name")),
            "email" => e(request("email")),
            "password" => Hash::make(request('password')),
            "role" => e(request("role")),
            "status" => e(request("status")),
            "avatar" => $file,
            "password_code" => e(request("password")),
        ];
    }
    
}