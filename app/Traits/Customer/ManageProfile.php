<?php
namespace App\Traits\Customer;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Auth, DB, Hash;

trait ManageProfile {
    public function profile() {
        $user = Auth()->user();
        $title = __('<i class="fas fa-user-edit"></i> Editar :user', ["user" => $user->name]);

        $optionsPassword =  ['route' => ['customer.password.update', ["user" => $user]] ];
        $optionsData = ['route' => ['customer.profile.update', ["user" => $user]] ] ;
            
        $textButton = __("Actualizar");
        $update = true;

        return view('web.customer.profile.index', compact('user', 'title', 'optionsPassword','optionsData','textButton','update'));
	}
	
	public function updateProfile(ProfileRequest $request){
        $user = Auth()->user();

        try {
            DB::beginTransaction();
             
            $user->name = e(request('name'));
            $user->nif = e(request('nif'));
            $user->save();
           
            DB::commit();
            session()->flash("message", ["success", __("Datos de usuario actualizados satisfactoriamente")]);
            return back();
            
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }        
	}
	
	public function updatePassword(PasswordRequest $request){
        
        $user = Auth()->user();
       
        if(request('apassword') != null):
            if(Hash::check(request('apassword'), $user->password)):
                $password = Hash::make(request('password'));
            else:
                session()->flash("message", ["danger", __("La contaseña no es correcta")]);
                return back();
            endif;
        endif;

        try {
            DB::beginTransaction();
            $user->password = $password;
            $user->password_code = e(request('password'));
            $user->save();
            DB::commit();
            session()->flash("message", ["success", __("Contraseña actualizada satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }        
    }
}
