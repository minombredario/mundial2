@extends("layouts.web")
@section('title', __('Finalizar pedido') )

@push('css')
    <style>
        
        .total{
            color: #008f00;
            text-shadow: 2px 2px #000000;
        }
        .btn{
                border-radius: 20px;
                letter-spacing: 7px;
        }

        .fa-bars{
            color: grey;
        }
        .header{
            background: #f5f6fa!important;
            z-index: 99;
        }
       
    </style>
@endpush

@section('page')
    <h2 style="color:#000">{{ __('Finalizar mi pedido') }} </h2>
@endsection

@section("content")
    @inject('cart', 'App\Services\Cart')
    <div class="container">
        @include('partials.web.cart.cart_content', ['payment' => true])
    </div>
        @if($cart->hasProducts())
            @include('partials.web.cart.cart_footer2', ['payment' => true])
        @endif
    
@endsection

@push('js')
    
    <script>
        $(document).ready(function(){
            // Target your element
            $('#content').colourBrightness();
            if($('#content').hasClass("light")){
                $('.logo').attr('src', "{{ url('/storage/web/logonegro.png') }}" );
            }

            if($('#content').hasClass("dark")){
                $('.logo').attr('src', "{{ url('/storage/web/logoblanco.png') }}" );
            }
        });
    </script>
    
@endpush