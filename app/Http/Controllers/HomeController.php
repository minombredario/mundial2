<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function products(){
       
        $category = Category::with('productsCustomer')->where('slug',request('cat'))->first();
        
       /* foreach($category->productsCustomer as $item){
           
            if($item->user_id == 2){
                 
                 $item->price += intval($item->price) * Config('mundial_souvenir.percentage_to_provider') / 100;
                    
            }
               
        };*/
       

        return view('web.content.products.list', compact('category'));
    
    }

    public function showProduct(){
       $product = request('product')->load('categories');
       //$category = Category::findOrFail($product->module_id);
       $category = $product->categories[0];
       return view('web.content.products.show', compact('product','category'));
    
    }

}
