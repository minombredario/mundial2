@extends("layouts.dashboard")

@section('title', __('Roles') )

@section("content")
	@include("dashboard.roles.list")
@endsection