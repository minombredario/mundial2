@extends("layouts.web")

@push('css')
   <style>
       .product{
            overflow: hidden;
            background-color: rgba(255,255,255,0.8);
            height: 600px;
            width: 600px;
            border-radius: 50%;
            position: absolute;
            margin: auto;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .product .row{
            margin: auto;
           
        }

        .product .content{
            height: 75%;
        }

        .product .content img{
            padding-top: 3rem; 
        }

        .product .title{
            position: absolute;
            top: 50px;
            left: 0;
            right: 0;
        }

        .product .description-wrapper{
            width: 300px;
            height: 300px;
            position: relative;
            margin: 10vh auto 0; /*OPTIONAL MARGIN*/
        }
        .product .description{
            overflow: hidden;
            width: 100%;
            height: 100%;
            position: relative;
            margin: 0;
        }
        .product .description p {
            height: 100%;
            padding: 0;
        }
        
        .product .description::before {
            content: "";
            width: 50%;
            height: 120%;
            float: right;
            shape-outside: polygon(
                2% 0%,
                100% 0%,
                100% 100%,
                2% 100%,
                50% 94%,
                79.6% 82.7%,
                90% 65.6%,
                90% 50%,
                83% 32.6%,
                68.6% 17.3%,
                50% 6%
            );
            shape-margin: 7%;
        }
        
        @media (min-width: 850px) {
        
            .description-wrapper{
                width: 370px;
                height: 370px;
            }
            .text p {
                font-size: 26px;
            }

            
        }
        
        
        @media (max-width: 650px){
            .product{
                width: 450px;
                height: 450px;
            }
            .product .content{
                height: 65%;
            }

            .product .content img{
                padding-top: inherit; 
            }

            .product .description{
                padding-top: 2rem;; 
            }
        }

        @media (max-width: 500px){
            .product{
                width: 350px;
                height: 350px;
                top: inherit;
                bottom: 75px;
            }
        }

        @media (max-width: 450px){
            .product .content img{
                margin-top: -50px; 
            }

            .product .description{
                padding-top: 0.2rem;; 
            }
        }
        @media (max-width: 320px){
            .product{
                bottom: 0px;
            }
        }

        

        @media (max-width: 1024px) and (orientation: landscape){
            
            .product{
                top: 150px;
            }
                     
        }

        @media (max-width: 950px) and (orientation: landscape){
            h3{
                font-size: 1.4rem;
            }
            .product{
                width: 300px;
                height: 300px;
                top: 50px;
            }
            .product .description-wrapper{
                width: 140px;
                height: 140px;
            }

            .product .content img {
                padding-top: 4rem;
            }

            .product .content {
                height: 70%;
            }

            .product .content .description{
                top: 30px;
            }

            
        }
        
    </style>
@endpush

@section('title', __($product->name) )

@section('page')
    
    <a href="{{ route('web.products', [ 'module' => $category->module_name , 'cat' =>  $category->slug ] )}}" class="title-page text-md-left text-sm-center text-decoration-none text-reset">
        <h1> {{ $category->name }} </h1>
    </a>

@endsection

@include('partials.video', ['category' => $category])

@section('content')
    @include('partials.web.products.show', ['show' => true ])
@endsection


@push('js')
    
    <script>
        $(document).ready(function(){
            // Target your element
            $('#content').colourBrightness();
            if($('#content').hasClass("light")){
                $('.logo').attr('src', "{{ url('/storage/web/logonegro.png') }}" );
            }

            if($('#content').hasClass("dark")){
                $('.logo').attr('src', "{{ url('/storage/web/logoblanco.png') }}" );
            }
            function hover(element) {
                element.setAttribute('src', '/storage/web/carrito-n.svg');
            }
        });
    </script>
    
@endpush