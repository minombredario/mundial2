<?php
namespace App\Http\Controllers;

use App\Jobs\SendSellerSalesEmail;
use App\Mail\CustomerNewOrder;
use App\Mail\AdminNewOrder;
use App\Models\Order;
use App\Models\Profit;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Laravel\Cashier\Http\Controllers\WebhookController;
use Log;

class StripeWebHookController extends WebhookController {
    /**
     *
     * WEBHOOK que se encarga de obtener un evento al hacer un pago correctamente
     * charge.succeeded
     * @param $payload
     * @return Response
     */
    public function handleChargeSucceeded($payload) {
        //return new Response('Webhook Handled: {handleChargeSucceeded}', 200);
         
        try {
            $invoice_id = $payload['data']['object']["invoice"];
            $user = $this->getUserByStripeId($payload['data']['object']['customer']);
            if ($user) {
                $order = $user->orders()
                              ->where("status", Order::PENDING)
                              ->latest()
                              ->first();
                if ($order) {
                    $order->load("order_lines.product.seller");
                    $order->update([
                        'invoice_id' => $invoice_id,
                        'status' => Order::SUCCESS
                    ]);
                    $profit = Profit::where('invoice_id', $order->id)->update(['created_at' => now()]);     
                    Log::info(json_encode($profit));
                    Log::info(json_encode($user));
                    Log::info(json_encode($order));
                    Log::info("Pedido actualizado correctamente");
                    
                    Mail::to($user->email)->send(new CustomerNewOrder($user, $order));
                    
                    $admin = User::where('role', User::ADMIN)->first();

                    Mail::to($admin->email)->send(new AdminNewOrder($user, $order));

                    foreach ($order->order_lines as $order_line) {
                        SendSellerSalesEmail::dispatch(
                            $order_line->product->seller,
                            $user,
                            $order_line->product,
                            $order_line
                        )->onQueue("emails");
                    }
                   
                    
                    return new Response('Webhook Handled: {handleChargeSucceeded}', 200);
                }
            }
        } catch (\Exception $exception) {
            Log::debug("Excepción Webhook {handleChargeSucceeded}: " . $exception->getMessage() . ", Line: " . $exception->getLine() . ', File: ' . $exception->getFile());
            return new Response('Webhook Handled with error: {handleChargeSucceeded}', 400);
        }
    }
}
