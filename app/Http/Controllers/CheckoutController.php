<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Order;
use App\Models\Profit;
use App\Models\OrderLine;
use App\Models\User;
use App\Services\Cart;
use DB, Auth;

use Stripe\Stripe;
use Stripe\InvoiceItem;
use Stripe\Customer;
use Stripe\Charge;

use Laravel\Cashier\Exceptions\IncompletePayment;

class CheckoutController extends Controller
{
    public function index() {
        return view('web.checkout.index');
    }

    public function processOrder()
    {

        
        if ( !auth()->user()->hasPaymentMethod()) {
            return redirect(
                route("customer.profile")
            )->with("message", ["warning", __("Debes añadir un método de pago antes de procesar el pedido")]);
        }
      
        if ( !auth()->user()->hasShippingDirection() && is_null(auth()->user()->hasDefaultDirection()) ) {
            return redirect(
                route("customer.profile")
            )->with("message", ["warning", __("Debes añadir una dirección de envío antes de procesar el pedido")]);
        }
       
        $order = null;
       
        try {
            DB::beginTransaction();
            Stripe::setApiKey(config('services.stripe.secret'));
            $cart = new Cart;
            if (!$cart->hasProducts()) {
                return back()->with("message", ["danger", __("No hay productos para procesar")]);
            }

            $order = new Order;
            $order->user_id = auth()->id();
            $order->total_amount = $cart->totalAmount(false);
            $order->shipping_cost = $cart->shippingCost();
            $order->iva = Config('mundial_souvenir.iva');
            $order->direction_id =  auth()->user()->ShippingDirection();

            
         
            if (session()->has("coupon")) {
                $coupon = Coupon::available(session("coupon"))->first();
                if ($coupon) {
                    $order->coupon_id = $coupon->id;
                }
            }
            
            $order->save();
             
            $profits = [];
            $orderLines = [];
            foreach ($cart->getContent() as $product) {
                
                $discount = "0";
                $discount_type = null;
                $apply_discount = 0;
                if($product->in_discount){
                    $discount = $product->discount;
                    $discount_type = $product->discount_type;
                    $apply_discount = $product->discount_type == Product::PERCENT ?  ($product->price * $product->discount) / 100 : $product->discount;
                };
               
                $orderLines[] = [
                    "product_id" => $product->id,
                    "order_id" => $order->id,
                    "quantity" => $product->qty,
                    "price" => $product->price,
                    "price_total" => ($product->price - $apply_discount) * $product->qty,
                    "discount" => $discount,
                    "discount_type" => $discount_type,
                    "created_at" => now()
                ];
                                
                InvoiceItem::create([
                    "unit_amount" => ($product->price - $apply_discount) * 100,
                    "quantity" => $product->qty,
                    "currency" => "eur",
                    "customer" => auth()->user()->stripe_id,
                    "description" => $product->name
                ]);
               
                $total_profit = 0;
                $profit = 0;
                $product->load('seller');
               
                if($product->seller->role == User::PROVIDER){
                    $total_profit = (($product->price - $apply_discount) * $product->qty) * Config("mundial_souvenir.profits_to_provider") / 100;
                    $profit = Config("mundial_souvenir.profits_to_provider");
                }

                if($product->seller->role == User::SELLER){
                    $total_profit = (($product->price - $apply_discount) * $product->qty) * Config("mundial_souvenir.profits_to_seller") / 100;
                    $profit = Config("mundial_souvenir.profits_to_seller");
                }

                $profits[] = [
                    "total_profit" =>  $total_profit,
                    'profit' => $profit,
                    'invoice_id' => $order->id
                ];
                
                Product::where('id', $product->id)->decrement('stock',$product->qty);
            }
             
            OrderLine::insert($orderLines);
            Profit::insert($profits);
            
            DB::commit();

            
            
            auth()->user()->invoiceFor('Portes', $cart->shippingCost() * 100 ,[],[]);

            $cart->clear();
           
            return redirect(route("customer.profile"))
                ->with("message", ["success", __("Muchas gracias por tu pedido, en breve empezaremos a prepararlo")]);
            
        } catch (IncompletePayment $exception) {
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('customer.profile', ["order" => $order])]
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            return back()->with("message", ["danger", __($exception->getMessage())]);
        }

    }
}