@extends("layouts.dashboard")

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.users') }}">
        <i class="fas fa-users"></i> {{ __('Usuarios') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles.create') }}">
        <i class="fas fa-user-plus"></i> {{ __("Crear usuario") }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.users.form")
@endsection