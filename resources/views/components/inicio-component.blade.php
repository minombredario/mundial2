@push('css')
	<style>

		

		

	</style>
@endpush


<div class="modal fade " id="modalInicio" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalInicioLabel" aria-hidden="true">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close" id="videoButton">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="embed-responsive embed-responsive-21by9">
					
					
					 <div class="video_container" id="bgvideo">
							
						<video autoplay="false" playsinline="true" id="intro" preload="true" controls="true" muted="true">
							<source src="{{ url('/storage/web/inicio.mp4') }}" type="video/mp4">
							<source src="{{ url('/storage/web/inicio.mkv') }}" type="video/mkv">
							<source src="{{ url('/storage/web/inicio.avi') }}" type="video/avi">
							<source src="{{ url('/storage/web/inicio-apple.mp4') }}" type="video/mp4">
						</video>
					
					</div>
				</div>
				
			
			</div>
		</div>
	</div>
</div>


@push('js')
@if($introMundial == true) 

	<script>
		$(document).ready(function(){
			$("#modalInicio").modal("show");

			var myVideoPlayer =$('#intro')[0];
			$('#intro').on('loadedmetadata', function () {
				var duration = myVideoPlayer.duration;
				setTimeout(function(){  $('#modalInicio').modal('hide'); }, duration * 1000);

			});	 

			$('#videoButton').on('click', function(){
				$('#intro').prop('muted', true);
				$('#intro').prop('controls', true);
			})
			
			var isSafari = window.safari !== undefined;

			if (isSafari){ 
				$('#intro').prop('muted', true);
			};
       
			if (/Firefox/.test(navigator.userAgent)) {
				$('#intro').prop('muted', false);
			}

			if (/Edg/.test(navigator.userAgent)) {
				$('#intro').prop('muted', false);
			}

			
		});
	</script>
@else
	<script>
		$('#intro').prop('autoplay', false);
		$('#intro').prop('controls', false);
	</script>
@endif
@endpush

