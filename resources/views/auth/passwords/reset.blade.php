@extends("layouts.web")
@push('css')
    <style>
        .row-content{
            background: #f5f6fa;
        }
    </style>
@endpush
@section('title',  __('Mis Datos') )

@section('logo')
     
        <img src="{{ url('/storage/web/logoblanco.png') }}" class="img-fluid" alt="" >
      
@endsection

@section('page')
    
    <h1 class="title-page text-md-left text-sm-center"> {{ __('Cambiar contraseña') }} </h1>

@endsection

@section("content")
    @include("web.customer.reset_password")
@endsection

