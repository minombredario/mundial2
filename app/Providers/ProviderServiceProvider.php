<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ProviderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::if('provider', function () {
            if (auth()->check()) {
                return auth()->user()->isProvider();
            }
            return false;
        });
    }
}
