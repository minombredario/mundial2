

<?php $readonly = true; $disabled = 'disabled' ?>

@can('haveaccess', 'product.edit')
	<?php $readonly = false; $disabled = ''?>
@endcan

@can('haveaccess', 'product.update')
	<?php $readonly = false; $disabled = ''?>
@endcan

@push('css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <style>
        .rmv {
            cursor: pointer;
            color: #fff;
            border-radius: 30px;
            border: 1px solid #fff;
            display: inline-block!important;
            background: rgba(255, 0, 0, 1);
            margin: -5px -10px;
            height: 25px;
        }
        .btn-rmv {
            display: none;
        }
    </style>
@endpush


<div class="container-fluid">
     @include('partials.form_errors')
    <div class="panel shadow">
        <div class="header d-flex justify-content-between">
            <h2 class="title">
                <i class="fas fa-plus"></i> {{ $title }}
            </h2>
            {!! Form::model($product, $options) !!}
    
            @isset($update)
                @method("PUT")
                
            @endisset

            @csrf
            
            @admin
                <div class="p-2">
                    <div class="input-group mb-3">
                        {!! Form::select('user_id',\App\Models\User::usersSeller(), null, ["class" => "form-select"]) !!}
                        {!! Form::select('status', \App\Models\Product::statusTypes(), null, ["class" => "form-select"]) !!}
                    </div>
                </div> 
            @endadmin         
        </div>
        <div class="inside">
                 <div class="d-flex justify-content-center mb-3 @if($required) invisible @endif" id="imag" >
                    <img src="{{ is_null($product->picture) ? '#' : $product->imagePath() }}" id="ImgPreview" width="250px" class="img-thumbnail"/>   <!--for preview purpose -->
                    <input type="button" id="removeImage" value="x" class="btn-rmv" />
                </div>
            <div class="row">   
                
                <div class="col-md-6">
                    {!! Form::label('name', __("Nombre")) !!}
                    <div class="input-group" >
                        {!! Form::label('name', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
                        {!! Form::text('name', null, ['class' => 'form-control' .($errors->has('name') ? ' is-invalid' : ''), 'required' ]) !!}
                    </div>

                    @if ($errors->has('name'))
                        <div class="{{$errors->has('name') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('name') ? 'display:block' : ''}}">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                    @endif
                    
                </div>
                
                <div class="col-md-3">
                    {!! Form::label('categories', __("Selecciona la categoría")) !!}
                    <div class="input-group mb-3">
                        {!! Form::label('categories', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                        {!! Form::select('categories', \App\Models\Category::pluck("name", "id"), null, ["class" => "form-select"]) !!}
                    </div>    
                </div>
                
                <div class="col-md-3">
                    {!! Form::label('picture', __("Imagen Destacada:")) !!}
                    <div class="input-group mb-3">
                        {!! Form::label('picture', '<i class="fas fa-image"></i>', [ 'class' => 'input-group-text' ], false) !!}
                        <div class="form-file ">
                            {!! Form::file('picture', ['class' => 'form-file-input' . ($errors->has('picture') ? ' is-invalid' : ''), 'id' => 'customFile', 'accept' => "image/*", $required ]) !!}
                            <label class="form-file-label" for="customFile">
                                {!! Form::label('picture', __('Selecciona una imagen'), [ 'class' => 'form-file-text', 'for' => "customFile", 'id' => 'file-label' ], false) !!}
                            </label>
                        </div>

                        @if ($errors->has('picture'))
                            <div class="{{$errors->has('picture') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('picture') ? 'display:block' : ''}}">
                                <strong>{{ $errors->first('picture') }}</strong>
                            </div>
                        @endif  
                    </div>

                    

                </div>
            </div>

            <div class="row mtop16">
                
                <div class="col-md-3">
                    {!! Form::label('price', __("Precio:")) !!}
                    <div class="input-group mb-3">
                        {!! Form::label('price', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                        @if(auth()->user()->role == \App\Models\User::PROVIDER || auth()->user()->role == \App\Models\User::ADMIN)
                            {!! Form::number('price', auth()->user()->role == \App\Models\User::PROVIDER || auth()->user()->role == \App\Models\User::ADMIN ? $product->real_price : $product->formatted_price  , ['class' => 'form-control', 'min' => '0.00', 'step' => '0.01']) !!}
                        @else
                            {!! Form::number('price', null  , ['class' => 'form-control', 'min' => '0.00', 'step' => '0.01']) !!}
                        @endif
                        @admin
                            @if($product->seller)
                                @if($product->seller->role == \App\Models\User::PROVIDER)
                                    <span class="input-group-text">PVP {{ $product->price }}€</span>
                                @endif
                            @endif
                        @endadmin
                    </div>   
                </div>
                @provider
                    <div class="col-md-3">
                        {!! Form::label('in_discount', __("¿Descuento?")) !!}
                        <div class="input-group mb-3">
                            {!! Form::label('in_discount', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                            {!! Form::select('in_discount', ['0' => 'No', '1' => 'Si'], null , ["class" => "form-select"]) !!}
                        </div>   
                    </div>
                    
                    <div class="col-md-3">
                        {!! Form::label('discount_type', __("Tipo de descuento")) !!}
                        <div class="input-group mb-3">
                            {!! Form::label('discount_type', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                            {!! Form::select('discount_type', \App\Models\Product::discountTypes(), null, ["class" => "form-select"]) !!}
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        {!! Form::label('discount', __("Descuento:")) !!}
                        <div class="input-group mb-3">
                            {!! Form::label('discount', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                            {!! Form::number('discount', null , ['class' => 'form-control', 'min' => '0.00', 'step' => '0.50']) !!}
                        </div>   
                    </div>
                @endprovider
            </div>

            <div class="row mtop16">
                <div class="col-md-3">
                    {!! Form::label('stock', __("Stock:")) !!}
                    <div class="input-group mb-3">
                        {!! Form::label('stock', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                        {!! Form::number('stock', null , ['class' => 'form-control', 'min' => '0', 'setp' => 'any', 'required']) !!}
                    </div>   
                </div>

                <div class="col-md-3">
                    {!! Form::label('code', __("Código de sistema")) !!}
                    <div class="input-group">
                        {!! Form::label('code', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                        {!! Form::text('code', null, ['class' => 'form-control' . ($errors->has('code') ? ' is-invalid' : ''), 'required']) !!}
                    </div>

                    @if ($errors->has('code'))
                        <div class="{{$errors->has('code') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('code') ? 'display:block' : ''}}">
                            <strong>{{ $errors->first('code') }}</strong>
                        </div>
                    @endif

                </div>
                @admin
                    <div class="col-md-3">
                        {!! Form::label('order', __("Prioridad:")) !!}
                        <div class="input-group mb-3">
                            {!! Form::label('order', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                            {!! Form::number('order', null , ['class' => 'form-control', 'min' => '0', 'step' => 'any', 'required']) !!}
                        </div>   
                    </div>
                @endadmin
                
            </div>

            <div class="row mtop16">
                <div class="col-md-12">
                    {!! Form::label('description', __("Añade la descripción al producto")) !!}
                    {!! Form::textarea('description', old('description') ?? $product->description, ['id' => 'summernote' , 'class' => ($errors->has('description') ? ' is-invalid' : '')]) !!}
                </div>
                @if ($errors->has('description'))
                        <div class="{{$errors->has('description') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('description') ? 'display:block' : ''}}">
                            <strong>{{ $errors->first('description') }}</strong>
                        </div>
                    @endif
            </div>

            @can('haveaccess', 'product.edit')
            <div class="row mtop16">
                <div class="col-md-12">
                    {!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2 w-100']); !!}
                </div>
            </div>
            @endcan

                
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push("js")
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            $('#summernote').summernote({
                height: 100,
            });
            $()
            $('#customFile').on('change', function() {
                var picture = $('#customFile')[0].files[0].name;
                $('#file-label').text(picture);
                $('.btn-rmv').addClass('rmv');
            });

            $("#removeImage").click(function(e) {
                e.preventDefault();
                $('#file-label').text('');
                $("#imag").val("");
                $("#ImgPreview").attr("src", "");
                $("#imag").addClass('invisible');
                $('.btn-rmv').removeClass('rmv');
            });
        });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ImgPreview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $("#customFile").change(function(){
        $("#imag").removeClass('invisible');
        readURL(this);
    });


    </script>
@endpush
