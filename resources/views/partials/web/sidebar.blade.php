@inject("cart", "App\Services\Cart")

<button type="button" id="sidebarCollapse" class="openbtn position-fixed btn btn-outline" onclick="openNav()" style="z-index: 999; color: #fff">
	<i class="fas fa-bars"></i>
</button>

<div id="mySidebar" class="sidebar">
	
	
	

	<div class="closebtn">
		<span aria-hidden="true" aria-label="Close" onclick="closeNav()">&times;</span>
	</div>
	
	<a href="{{ route('cart') }}">
		<div class="cart" cart="{{ $cart->hasProducts() }}" >
			<img src="{{ url('/storage/web/carrito-b.svg') }}" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid" alt="" > 
		</div>
	</a>



	
	<div id="dismiss" class="d-block text-right mapabtn" style="background:transparent" href="javascript:void(0)"> 
		<a href="{{ route('home') }}">
			<h4 class="text-capitalize">{{ __('volver') }}</h4>
			<h1 class="text-capitalize">{{ __('mapa') }}</h1>
		</a>
	</div>

		@auth
			<div>
				<a class="btnOpenForm" href="javascript:void(0)" onclick="openForm()">
					<i class="fas fa-user"></i> <span> {{ Auth::user()->name . ' ' . Auth::user()->lastname  }}</span>
				</a>
				<div class="formulario_login invisible">
					
						<div class="text-right pt-2 logged">
							<a href="{{ route('customer.profile', ['user' => Auth::user() ]) }}" class="d-block text-uppercase"><h6>{{ __('Mis datos') }}</h6></a>
							@if(auth()->user()->accessDashboard())
								<a href="{{ url('/dashboard') }}" class="d-block text-uppercase"><h6>{{ __('Panel de control') }}</h6></a>
							@endif
							<a href="{{ route('logout') }}" class="d-block text-uppercase" onclick="event.preventDefault();  document.getElementById('logout-form').submit();">
                       			<h6>{{ __('Desconectar') }}</h6>
                   			 </a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
								@csrf
							</form>
					
							
						</div>
						
					
				</div>
			</div>
		@endauth
		@guest
			<a class="btnOpenForm" href="javascript:void(0)" onclick="openForm()">
				
				<i class="fas fa-user"></i> <span class="text-uppercase">{{ __('acceder') }}</span>
			</a>
			<div class="formulario_login invisible">
				{!! Form::open(['url' => '/login', 'id' => 'form_user_login', 'class' => 'p-3']) !!}
					<div class="form-row">
						<div class="col-12 pt-1 form-group">
							{!! Form::email('email', null, ['class' => 'form-control col-12 pt-1', 'placeholder' => 'email', 'id' => 'inputEmail', 'required']) !!}
						</div>
						<div class="col-12 pt-1 form-group">
							{!! Form::password('password', ['class' => 'form-control col-12 pt-1', 'placeholder' => 'password', 'required', 'onkeypress' => "javascript: if(event.keyCode == 13) document.getElementById('form_user_login').submit();" ]) !!}
						</div>
						
					</div>
					<div class="text-right pt-2">
						<a href="#" onclick="sendLoginForm()" class="text-uppercase">{{ __('acceder') }}</a>
						<a href="#" id="recovery-button" class="d-block text-uppercase">{{ __('¿OLVIDO CONTRASEÑA?') }}</a>
						<a href="#" id="signup-button" class="d-block text-uppercase"> {{ __ ('nuevo cliente') }}</a>
					</div>
					
				{!! Form::close() !!}
			</div>	
		@endguest
	@include("partials.web.categories.list")	
	@include("partials.web.sidebar_footer")
</div>
  
@include('partials.web.modals.signup_customer')
@include('partials.web.modals.recovery_password')
	


@push("js")

<script type="text/javascript">
		
	@if(session('error-signup'))
		$("#signup-modal").modal();
	@endif
	
	jQuery("#signup-button").on("click", function (e) {
		e.preventDefault();
		$("#signup-modal").modal();
	})

	@if(session('error-signup'))
		$("#recovery-modal").modal();
	@endif
	
	jQuery("#recovery-button").on("click", function (e) {
		e.preventDefault();
		$("#recovery-modal").modal();
	})
		
	function openNav() {
		if(screen.width < 750){
			document.getElementById("mySidebar").style.width = "150px";
		}else{
			document.getElementById("mySidebar").style.width = "250px";
		}
		
		$('.openbtn').toggleClass('invisible');	
	}

	function closeNav() {
		document.getElementById("mySidebar").style.width = "0";
		$('.openbtn').toggleClass('invisible');
		$('.formulario_login').addClass('invisible');
	}

	function openForm(){
		$('.formulario_login').toggleClass('invisible');
		$('.formulario_login').find('input[name="email"]').focus();
	}

	function hover(element) {
		element.setAttribute('src', '/storage/web/carrito-b.svg');
	}

	function unhover(element) {
		element.setAttribute('src', '/storage/web/carrito-b.svg');
	}

	function sendLoginForm(e){
		
		$("#form_user_login").validate({
	
				rules: {
					password: {
						required: true,
						minlength: 8
					},
	
					email: {
						required: true,
						email: true,
					},
				},
				
				messages: {
	
					password: {
						required: "{{ __('La contraseña es requerida') }}",
						minlength: "{{ __('La contraseña debe tener al menos 8 caracteres')}}",
					},
					email: {
						required: "{{ __('El email es requerido') }}",
						email: "{{ __('Introduce un email valido') }}",
					},
	
				},

				errorElement: 'span',
				errorPlacement: function (error, element) {
					error.addClass('invalid-feedback');
					element.closest('.form-group').append(error);
				},
				highlight: function (element, errorClass, validClass) {
					$(element).addClass('is-invalid');
				},
				unhighlight: function (element, errorClass, validClass) {
					$(element).removeClass('is-invalid');
				},
		
		});
		
		$('#form_user_login').submit()
		
	}

	

		
</script>
@endpush