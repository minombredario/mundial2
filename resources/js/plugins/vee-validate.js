import { extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import { messages } from 'vee-validate/dist/locale/es.json';

Object.keys(rules).forEach(rule => {
    extend(rule, {
        ...rules[rule], // rule
        message: messages[rule] // override messages
    });
});

extend('postal_code', {
    message: 'No enviamos a esta provincia',
    validate: value => (value >= 1000 && value < 35000) || (value >= 36000 && value < 38000 || (value >= 39000 && value < 51000))
});

extend('wysiwyg-required', {
    ...rules['required'],
    message: 'El campo descripción es requerido'
});

extend('wysiwyg-min', {
    ...rules['min'],
    message: 'El campo descripción ha de tener almenos {length} carácteres'
});
