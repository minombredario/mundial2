@extends("layouts.web")

@section('title', __('Inicio') )

@section('logo')
    
        <img src="{{ url('/storage/web/logoblanco.png') }}" class="img-fluid" alt="" >
      
@endsection

@push('css')
   

    <style>
    .mundial_viewed_nav_container {
        text-align: center;
    }

    .mundial_viewed_nav {
        display: inline-block;
        cursor: pointer
    }

    .mundial_viewed_nav i {
        color: #dadada;
        font-size: 18px;
        padding: 5px;
        -webkit-transition: all 200ms ease;
        -moz-transition: all 200ms ease;
        -ms-transition: all 200ms ease;
        -o-transition: all 200ms ease;
        transition: all 200ms ease
    }

    .mundial_viewed_nav:hover i {
        color: #606264
    }

    .mundial_viewed_prev {
        margin-right: 15px
    }

    .owl-carousel .owl-item .item {
        position: relative;
    }
    .owl-carousel .owl-item img {
        border-radius: 25px;
    }

    .owl-carousel .owl-item p{
        font-size: 1.6rem;
        font-weight: 600;
        color: #fff;
        left: 0;
        right: 0;
        background: rgba(0,0,0,.70);
        position: absolute;
        top: 50%;
        transform: translate(0, -50%);
    }

    @media (max-width: 900px) and (orientation: landscape){
        .mapa{
            padding-top: 75px;
        }
       
        .owl-carousel .owl-item p{
            font-size: 1.3rem;
        }
    }

    @media screen and (max-width: 850px){
        .owl-carousel .owl-item p{
            font-size: 1.3rem;
            
        }
    }
    @media screen and (max-width: 750px){
        .owl-carousel .owl-item img {
            border-radius: 10px;
        }

        .owl-carousel .owl-item p{
            font-size: 0.9rem;
            font-weight: 500;
        }
    }

    </style>
@endpush
@php
        # Iniciando la variable de control que permitirá mostrar o no el modal
        $introMundial = false;
        # Verificando si existe o no la cookie
        if(!isset($_COOKIE["introMundial"]))
        {
            # Caso no exista la cookie entra aquí
            # Creamos la cookie con la duración que queramos
            
            //$expirar = 3600; // muestra cada 1 hora
            //$expirar = 10800; // muestra cada 3 horas
            //$expirar = 21600; //muestra cada 6 horas
            $expirar = 43200; //muestra cada 12 horas
            //$expirar = 86400;  // muestra cada 24 horas
            setcookie('introMundial', true, (time() + $expirar)); // mostrará cada 12 horas.
            # Ahora nuestra variable de control pasará a tener el valor TRUE (Verdadero)
            $introMundial = true;
        }
@endphp
	
        

        
@section("content")
        <!-- Vídeo inicio -->
        @component('components.inicio-component', ['introMundial' => $introMundial])@endcomponent
        <!-- End Video inicio -->
    <div class="d-flex justify-content-center mapa">
        
        <div class="m-auto d-grid container">
            @php 
                $modules = \App\Models\Module::getModules2();
            @endphp
            @foreach ( $modules as $key => $value)
               @include('partials.web.carrousel', [ 'module' => $modules[$key] ])
            @endforeach
           

        </div>
    </div>
    @include('partials.cookies_law')
@endsection

@push("js")
    <script type="text/javascript">
   
        $(document).ready(function(){
           if($('.mundial_viewed_slider').length){
            
            var viewedSlider = $('.mundial_viewed_slider');
            
        
        
       
            $('.owl-carousel').owlCarousel({
                
                loop: true,
                margin:10,
                autoplay: true,
                autoplayTimeout:2000,
                autoplayHoverPause:true,
                lazyLoad:false,
                
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
           
            
                responsive:{
                    0:{
                        items: 3
                    },
                    575:{items:3},
                    768:{items:4},
                    991:{items:4},
                    1199:{items:5}
                }
            })
        };
    });
    </script>
@endpush
