<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'name'          => 'required|max:50|unique:roles,name',
                    'slug'          => 'required|max:50|unique:roles,slug',
                    'full-access'   => 'sometimes|in:on,off'
                ];
            }
            case 'PUT': {
                return [
                    'name'          => 'required|max:50|unique:roles,name,' . $this->route('role')->id,
                    'slug'          => 'required|max:50|unique:roles,slug,' . $this->route('role')->id,
                    'full-access'   => 'sometimes|in:on,off'
                ];
            }
            default: {
                return [];
            }
        }
    }
}

