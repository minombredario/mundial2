<?php

namespace App\Http\Middleware;

use Closure;

class DashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = redirect(route("welcome"));
        if (!auth()->check()) {
            return $route;
        }
        if (!auth()->user()->accessDashboard()) {
            return $route;
        }

        return $next($request);
    }
}
