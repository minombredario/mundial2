<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Product;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('name')->unique();
            $table->string('slug');
            $table->string('picture');
            $table->text('description');
            $table->float('price');
            $table->integer('stock')->default('0');
            $table->integer('in_discount');
            $table->enum('discount_type', [Product::PERCENT, Product::PRICE])->default(Product::PRICE);
            $table->integer('discount');
            $table->string('code')->unique()->nullable();
            $table->boolean('featured')->default(false);
            $table->enum('status', [Product::PUBLISHED, Product::PENDING, Product::REJECTED])->default(Product::PENDING);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
