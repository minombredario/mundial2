<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

/**
 * App\Models\Module
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $categories
 * @property-read int|null $categories_count
 * @method static \Illuminate\Database\Eloquent\Builder|Module newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Module newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Module query()
 * @mixin \Eloquent
 */
class Module extends Model
{
    
    const MODULES = [
            ['id' => 0, 'name' => 'África', 'slug' => 'africa', 'categories' => []],
            ['id' => 1, 'name' => 'América', 'slug' => 'america', 'categories' => []],
            ['id' => 2, 'name' => 'Asia', 'slug' => 'asia', 'categories' => []],
            ['id' => 3, 'name' => 'Europa', 'slug' => 'europa', 'categories' => []],
            ['id' => 4, 'name' => 'Oceanía', 'slug' => 'oceania', 'categories' => []]
    ];


    public static function getModules($field1, $field2): array {

        return array_column( self::MODULES , $field1, $field2);
        
    }

    public static function getModules2(){
        return Collect(self::MODULES);
    }

    public static function getModule($module_id): string {
        $array = [
            (object)['id' => 0, 'name' => 'África', 'slug' => 'africa', 'categories' => []],
            (object)['id' => 1, 'name' => 'América', 'slug' => 'america', 'categories' => []],
            (object)['id' => 2, 'name' => 'Asia', 'slug' => 'asia', 'categories' => []],
            (object)['id' => 3, 'name' => 'Europa', 'slug' => 'europa', 'categories' => []],
            (object)['id' => 4, 'name' => 'Oceanía', 'slug' => 'oceania', 'categories' => []]
        ];
        $module = current(array_filter($array, function($e) use($module_id) { return $e->id==$module_id; }));
        return $module->slug;
        
    }

    public function categories() {
        return $this->hasMany(Category::class, 'module_id');
    }

    public static function getModulesWithCatagories() {
        $modulos = self::MODULES;
        foreach($modulos as $module){
            
            $modulos[$module['id']]['categories'] = Category::select('id','name','slug','module_id', 'background')->where('module_id', $module['id'])->get()->toArray();
            
        }
       // dd($modulos[0]['categories'][0]['id']);
        return $modulos;
    }
 

}
