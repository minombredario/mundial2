@extends("layouts.dashboard")

@section('title', __('Políticas') )

@section("content")
    @include("dashboard.contents.list")
@endsection