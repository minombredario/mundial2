<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\Order as OrderResource;

class OrderLine extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order' => New OrderResource( $this->order ),
            'product' => New ProductResource( $this->product ),
            'quantity' => $this->quantity,
            'price' => $this->price,
            'price_total' => $this->price_total,
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d-m-Y H:i'),
       ];
    }
}
