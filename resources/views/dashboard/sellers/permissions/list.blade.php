@extends("layouts.dashboard")

@section('title', 'Permisos de usuario')

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.users') }}">
        <i class="fas fa-user-friends"></i> {{ __('Usuarios') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.users.permissions' , $user) }}">
        <i class="fas fa-cogs"></i> {{ __('Permisos') }}: {{ $user->name }}
    </a>
</li>
@endsection

@include('partials.form_errors')


@section('content')
	@include('dashboard.users.permissions.edit')
@endsection
