<?php

namespace App\Http\Middleware;

use Closure;

class isAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (!auth()->check()) {
            session()->flash("message", ["warning", __('No tienes acceso a esta sección')]);
            return redirect("/");
        }
        if (!auth()->user()->isAdmin()) {
            session()->flash("message", ["warning", __('No tienes acceso a esta sección')]);
            return  redirect("dashboard");
        }

        return $next($request);
    }
}
