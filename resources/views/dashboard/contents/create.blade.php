@extends("layouts.dashboard")

@section('title', __("Políticas") )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.contents') }}">
        <i class="far fa-newspaper"></i> {{ __('Políticas') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.contents.create') }}">
        <i class="fas fa-newspaper"></i> {{ __("Crear documento") }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.contents.form", ['required' => true])
@endsection