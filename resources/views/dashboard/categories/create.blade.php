

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.categories') }}">
        <i class="fas fa-boxes"></i> {{ __('Categorías') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.categories.create') }}">
        <i class="fas fa-box"></i> {{ __("Crear categoría") }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@include('dashboard.categories.form')