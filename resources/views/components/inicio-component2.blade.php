@push('css')
	<style>

		
#c {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
}	

	</style>
@endpush
<div class="modal fade " id="modalInicio" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalInicioLabel" aria-hidden="true">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="videoButton">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="embed-responsive embed-responsive-21by9">
					
					
					<canvas id=c></canvas>
					<video autoplay="false" playsinline="true" id="v" preload="true" controls="true">
						<source src="{{ url('/storage/web/inicio.mp4') }}" type="video/mp4">
					</video>
				</div>
				
			
			</div>
		</div>
	</div>
</div>


@push('js')
<script>
	$(document).ready(function(){
		$("#modalInicio").modal("show");
		var myVideoPlayer =$('#intro')[0];
		
			var v = document.getElementById('v');
			var canvas = document.getElementById('c');
			var context = canvas.getContext('2d');

			var cw = Math.floor(canvas.clientWidth / 100);
			var ch = Math.floor(canvas.clientHeight / 100);
			canvas.width = cw;
			canvas.height = ch;

			v.addEventListener('play', function(){
				draw(this,context,cw,ch);
			},false);

		

		function draw(v,c,w,h) {
			if(v.paused || v.ended) return false;
			c.drawImage(v,0,0,w,h);
			setTimeout(draw,20,v,c,w,h);
		}

		$('#intro').on('loadedmetadata', function () {
			var duration = myVideoPlayer.duration;
			setTimeout(function(){  $('#modalInicio').modal('hide'); }, duration * 1000);

		});	 

		$('#videoButton').on('click', function(){
			$('#intro').prop('muted', true);
			$('#intro').prop('controls', true);
		})
			
		var isSafari = window.safari !== undefined;

		if (isSafari){ 
			$('#intro').prop('muted', true);
			$('#intro').prop('controls', true);
		};
	
		if (/Chrome/.test(navigator.userAgent)) {
			$('#intro').prop('muted', true);
			$('#intro').prop('controls', true);
		}

		if (/Edg/.test(navigator.userAgent)) {
			$('#intro').prop('muted', false);
			$('#intro').prop('controls', true);
		}
	});
	</script>
@endpush




