<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderLine as OrderLineResource;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Direction as DirectionResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'direction' => new DirectionResource($this->direction),
            'invoice_id' => $this->invoice_id,
            'total_amount' => $this->total_amount,
            'shipping_cost' => $this->shipping_cost,
            'status' => $this->formattedStatus,
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d-m-Y H:i'),
            'lines' => $this->order_lines,
       ];
    }
}
