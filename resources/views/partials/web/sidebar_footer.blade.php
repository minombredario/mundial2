@push('css')
    <style>
        .list-group-item{
            padding: 0px;
        }

        .list-group-item a{
            padding: 0px;
        }

        .modal-header{
            border-bottom: none;
        }
    </style>
@endpush

<div class="sidebar_footer bg-transparent">
    <ul class="list-group">
        @foreach( \App\Models\Content::getContents() as $content)
            <li class="list-group-item bg-transparent text-sm-left text-lg-right">
                <h6>
                    <a href="#" onClick="openModal({{$content}})" class="d-block text-uppercase">{{ $content->title }}</a>
                </h6>
            </li>
            
        @endforeach  
    </ul>
   
</div>

 @include('partials.web.modals.contents')

@push("js")

<script type="text/javascript">
		
    function openModal(content){
        $("#content-modal").modal();
        $("#content-body").html( decodeHtml(content['content']) );
        $("#content-title").html( content['title'] );
    }	

    function decodeHtml(html) {	
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }
	


	
</script>
@endpush