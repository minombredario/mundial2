@include('partials.form_errors')

<div class="container-fluid">
    {!! Form::open(['route' => 'admin.dashboard.settings']) !!}
    <div class="row mtop16">
        <div class="col-md-4">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-coins"></i> {{ __('Moneda') }}
                    </h2>
                </div>
        
                <div class="inside px-5">
                    <div class="row">
                        <div class="col">
                            <label for="currency"> {{ __('Moneda')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::text('currency', Config('mundial_souvenir.currency'), ['class' => 'form-control', 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-chart-line"></i> {{ __('Impuestos') }}
                    </h2>
                </div>
        
                <div class="inside px-5">
                    <div class="row">
                        
                        <div class="col">
                            <label for="iva"> {{ __('IVA')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::number('iva', Config('mundial_souvenir.iva'), ['class' => 'form-control', 'required']) !!}
                                <span class="input-group-text">%</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mtop16">
        <div class="col-md-4">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-wallet"></i> {{ __('Beneficios') }}
                    </h2>
                </div>
        
                <div class="inside px-5">
                    <div class="row mtop16">
                        <div class="col">
                            <label for="percentage_to_supplier"> {{ __('Beneficio sobre distribuidor')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::number('profits_to_seller', Config('mundial_souvenir.profits_to_seller'), ['class' => 'form-control', 'required']) !!}
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="row mtop16">
                        <div class="col">
                            <label for="percentage_to_provider"> {{ __('Beneficio sobre proveedor')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::number('profits_to_provider', Config('mundial_souvenir.profits_to_provider'), ['class' => 'form-control', 'required']) !!}
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
        
                    </div>                   
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-truck"></i> {{ __('Portes') }}
                    </h2>
                </div>
        
                <div class="inside px-5">
                    <div class="row">
                        <div class="col">
                            <label for="shipping_cost"> {{ __('Portes Nacionales')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::number('shipping_cost', Config('mundial_souvenir.shipping_cost'), ['class' => 'form-control', 'required']) !!}
                                <span class="input-group-text">€</span>
                            </div>
                        </div>
                    </div>
                    <div class="row mtop16">
                        <div class="col">
                            <label for="shipping_cost_international"> {{ __('Portes Internacionales')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::number('shipping_cost_international', Config('mundial_souvenir.shipping_cost_international'), ['class' => 'form-control', 'required']) !!}
                                <span class="input-group-text">€</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mtop16">
                        <div class="col">
                            <label for="text_more_sellers"> {{ __('Texto cuando se compra a varios vendedores')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::textarea('text_more_sellers', Config('mundial_souvenir.text_more_sellers'), ['class' => 'form-control', 'rows' => 3, 'required']) !!}
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mtop16">
        
        <div class="col-md-4">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-info-circle"></i> {{ __('SEO') }}
                    </h2>
                </div>
        
                <div class="inside px-5">
                    <div class="row">
                        <div class="col">
                            <label for="meta_keywords"> {{ __('Palabras clave')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::textarea('meta_keywords', Config('mundial_souvenir.meta_keywords'), ['class' => 'form-control', 'rows' => 3, 'required']) !!}
                                
                            </div>
                        </div>
                    </div>
                    <div class="row mtop16">
                        <div class="col">
                            <label for="meta_description"> {{ __('Descripción')}}:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="far fa-keyboard"></i>
                                </span>
                                {!! Form::textarea('meta_description', Config('mundial_souvenir.meta_description'), ['class' => 'form-control', 'rows' => 3, 'maxlength' => "150", 'required']) !!}
                                
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 d-none">
        <div class="panel shadow">
            <div class="header">
                <h2 class="title">
                    <i class="fas fa-cogs"></i> {{ __('Ajustes') }}
                </h2>
            </div>
    
            <div class="inside px-5">
                
                <div class="row mtop16">
                    <div class="col-md-3">
                        <label for="maintenance_mode"> {{ __('Modo mantenimiento')}}:</label>
                        <div class="input-group">
                            <span class="input-group-text">
                                <i class="far fa-keyboard"></i>
                            </span>
                            {!! Form::select('maintenance_mode',['0' => __('Desactivado'), '1' => _('Activo') ] , Config('mundial_souvenir.maintenance_mode'),['class' => 'form-select']) !!}
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>

    {!! Form::submit(__('Guardar'), ['class' => 'btn btn-dark mt-2']); !!}
    {!! Form::close() !!}
</div>