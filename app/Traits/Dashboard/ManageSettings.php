<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use DB;
use Str;

trait ManageSettings {

	public function settings(){

         return view('dashboard.settings.index');

    }

    public function storeSettings(Request $request){
        if(!file_exists(config_path() . '/mundial_souvenir.php')){
            fopen(config_path() . '/mundial_souvenir.php', 'w');
        }

        $file = fopen(config_path() . '/mundial_souvenir.php', 'w');
        fwrite($file, '<?php' . PHP_EOL);
        fwrite($file, 'return [' . PHP_EOL);
        foreach($request->except(['_token']) as $key => $value){
            if(is_null($value)){
                $value = 0;//fwrite($file, '\'' . $key . '\' => \'\',' . PHP_EOL );
            }else{
                fwrite($file, '\'' . $key . '\' => \'' . $value . '\',' . PHP_EOL );
            }
        };
        fwrite($file, ']' . PHP_EOL);
        fwrite($file, '?>' . PHP_EOL);
        fclose($file);
        
        session()->flash("message", ["success", __("Configuración guardada satisfactoriamente")]);
        return back();
    }

  
    
   
}