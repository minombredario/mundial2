<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>
		@if (isset($title))
			{{ $title }}
		@endif
	</title>
  <style>
    .clearfix:after {
      content: "";
      display: table;
      clear: both;
    }

    a {
      color: #5D6975;
      text-decoration: underline;
    }

    body {
      position: relative;
      
      color: #001028;
      background: #FFFFFF; 
      font-family: Arial, sans-serif; 
      font-size: 12px; 
      font-family: Arial;
    }

    header {
      padding: 10px 0;
      margin-bottom: 30px;
    }

    #logo {
      text-align: center;
      margin-bottom: 20px;
    }

    #logo img {
      width: 250px;
    }

    h1 {
      border-top: 1px solid  #5D6975;
      border-bottom: 1px solid  #5D6975;
      color: #5D6975;
      font-size: 2.4em;
      line-height: 1.4em;
      font-weight: normal;
      text-align: center;
      margin: 0 0 20px 0;
      background: url("{{'data:image/png;base64,' . base64_encode(file_get_contents(@ asset('dimension.png')))}}");
    }

    #project {
      float: left;
    }

    #project span {
      color: #5D6975;
      text-align: right;
      width: 52px;
      margin-right: 10px;
      display: inline-block;
      font-size: 0.8em;
    }

    #company {
      float: right;
    }

    #project div,
    #company div {
      white-space: nowrap;        
    }

    table {
      width: 90%;
      border-collapse: collapse;
      border-spacing: 0;
      margin-bottom: 20px;
      margin-left: auto;
      margin-right: auto;
    }

    table tbody tr:nth-child(2n-1) td {
      background: #F5F5F5;
    }

    table tbody tr:last-child td {
      border-bottom: 3px ridge #5D6975;
    }

    table th,
    table td {
      text-align: center;
    }

    table th {
      padding: 5px 20px;
      color: #5D6975;
      border-bottom: 1px solid #C1CED9;
      white-space: nowrap;        
      font-weight: normal;
    }

    table .service,
    table .desc {
      text-align: left;
    }

    table td {
      padding: 20px;
      text-align: right;
    }

    table tfoot td {
      padding: 5px;
      text-align: right;
    }


    table td.service,
    table td.desc {
      vertical-align: top;
    }

    table td.unit,
    table td.qty,
    table td.total {
      font-size: 1.2em;
      text-align: right;
    }

    table td.grand {
      border-top: 1px double #5D6975;
    }

    #notices .notice {
      color: #5D6975;
      font-size: 1.2em;
    }

    footer {
      color: #5D6975;
      width: 100%;
      height: 30px;
      position: absolute;
      bottom: 0;
      border-top: 1px solid #C1CED9;
      padding: 8px 0;
      text-align: center;
    }
  </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{{'data:image/png;base64,' . base64_encode(file_get_contents(@ asset('logonegro.png')))}}">
      </div>

      <h1>{{ __('FACTURA') }}</h1>

   		<div id="company" class="clearfix">
        @if(isset($vendor))
          <div>{{ $vendor }}</div>
        @endif
        @if(isset($vendorVat))
          <div>{{ $vendorVat }}</div>
        @endif
        @if(isset($street))
          <div>{{ $street }},<br /> {{ $location }}</div>
        @endif
        @if(isset($phone))
          <div>{{ $phone }}</div>
        @endif
        @if(isset($mail))
          <div>{{$mail}}</div>
        @endif
      </div>
	  	@if(isset($owner))
        <div id="project">
          <div><span>{{ __('FACTURA') }}</span> {{ $id ?? $invoice->number }}</div>
          <div><span>{{ __('CLIENTE') }}</span> {{ $owner->name }}</div>
          <div><span>{{ __('DIRECCIÓN') }}</span> {{ $data_direction->line_1 }}, {{ $data_direction->postal_code }}</div>
          <div><span>{{ __('POBLACIÓN') }}</span>{{ $data_direction->country . '-' . $data_direction->city }}</div>
          <div><span>{{ __('EMAIL') }}</span> <a href="{{ $owner->stripeEmail() ?: $owner->name }}">{{ $owner->stripeEmail() ?: $owner->name }}</a></div>
          <div><span>{{ __('FECHA') }}</span> {{ $invoice->date()->toFormattedDateString() }}</div>
        </div>
      @endif	  
      
    </header>
    <main>
      @php
        $ver = false; 
      @endphp
      <table>
        <thead>
          <tr>
            <th class="service">{{ __('Artículo') }}</th>
            <th class="qty">{{ __('Cantidad') }}</th>
            <th>{{ __('Precio') }}</th>
            @if($ver)
              <th>{{ __('Descuento') }}</th>
            @endif
            <th>{{ __('Total') }}</th>
          </tr>
        </thead>
        <tbody>
           @foreach ($order->order_lines as $item)
                @if($item->in_discount == 1)
                  @php
                    $ver = true;
                  @endphp 
                @endif  
            <tr class="row">
              <td class="service">{{ $item->product->name }}</td>
							<td class="qty">{{ $item->quantity }}</td>
              <td class="unit">{{ $item->formatted_price }}</td>
              @if($item->in_discount == 1)
                <td >{{ $item->product->discount . \App\Models\Product::discountIcon($item->product->discount_type) }}</td>
              @endif
              <td class="total">{{ \App\Helpers\Currency::formatCurrency($item->price_total) }}</td>
            </tr>
             @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="{{ $ver ? 2 : 1 }}"></td>
            <td colspan="2">{{ __('Subtotal') }}</td>
            <td class="total">{{ \App\Helpers\Currency::formatCurrency($order->total_amount) }}</td>
          </tr>
          <tr>
            <td colspan="{{ $ver ? 2 : 1 }}"></td>
            <td colspan="2">{{ __('Portes') }}</td>
            <td class="total">{{ \App\Helpers\Currency::formatCurrency($order->shipping_cost) }}</td>
          </tr>
          <tr>
            <td colspan="{{ $ver ? 2 : 1 }}"></td>
            <td colspan="2">{{ __('IVA') ." " . $order->IVA }}%</td>
            <td class="total">
              {{ \App\Helpers\Currency::formatCurrency( ($order->total_amount + $order->shipping_cost) * $order->IVA / 100 ) }}
            </td>
          </tr>
          <tr>
            <td colspan="{{ $ver ? 2 : 1 }}"></td>
            <td colspan="2" class="grand total">{{ __('Total') }}</td>
            <td class="grand total">{{ \App\Helpers\Currency::formatCurrency($order->total_amount + $order->shipping_cost) }}</td>
          </tr>    
        </tfoot>
      </table>
      @if(isset($notice))
        <div id="notices">
          <div>{{ $notice->title }}</div>
          <div class="notice">{{ $notice->content }}</div>
        </div>
      @endif
    </main>
    @if(isset($footer))
      <footer>
       {!! $footer !!}
      </footer>
    @endif
  </body>
</html>