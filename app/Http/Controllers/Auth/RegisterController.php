<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Role;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Mail\VerificationEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\UserRequest;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        
        $validation =  Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role' => ['required', Rule::in([User::CUSTOMER])]
        ]);

        if($validation->fails()){
            session()->flash('error-signup');
        };

        return $validation;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|min:8|same:password'
        ];

        $messages = [
            'name.required' => 'Su nombre es obligatorio.',
            'email.required' => 'Su correo electrónico es obligatorio.',
            'email.email' => 'El formato de su correo electrónico no es correcto.',
            'email.unique' => 'Ya existe un usuario con este correo electrónico.',
            'password.required' => 'Por favor escriba una contraseña.',
            'password.min' => 'La contraseña debe de tener al menos 8 caracteres.',
            'password_confirmation.required' => 'Es necesario confirmar la contraseña.',
            'password_confirmation.min' => 'La confirmación de la contraseña debe de tener al menors 8 caracteres.',
            'password_confirmation.same' => "Las contraseñas no coinciden."
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
       
        if($validator->fails()){
            return redirect("/")->with( session()->flash('error-signup', __('Ponte en contacto con nosotros')) )->withErrors($validator)->withInput();
        }
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'password_code' => $request['password'],
            'role' => $request['role'],
            'verification_code' => sha1( time() )
        ]);
        
        /*
        $role = Role::whereName('CUSTOMER')->pluck('id')->first();
                
        $user->roles()->sync($role);
        */
        
        
            if($user != null){
                
                Mail::to($user->email)->send(new VerificationEmail($user));
                return redirect("/")->with(session()->flash('alert-success', 'Tu cuenta ha sido creada. Por favor revisa el correo para validar tu cuenta.'));
            }

        return redirect("/")->with(session()->flash('alert-danger', 'Ha ocurrido un error inesperado'));

        //return $user;
    }

    public function verifyUser(Request $request){
        $verification_code = request('code');
        $user = User::where(['verification_code' => $verification_code])->whereNotNull('verification_code')->first();
       
        if($user != null){

            $user->status = 1;
            $user->email_verified_at = now();
            $user->save();
           
            return redirect("/")->with(session()->flash('alert-success', 'Tu cuenta ha sido verificada.'));
        }

        return redirect("/")->with(session()->flash('alert-danger', 'Código de verificación erroneo'));
       
        
    }

    public function registered(Request $request, $user)
    {
        if ($request->ajax()){
            return response()->json(["message" => __("Gracias por crear tu cuenta como profesor en :app", ["app" => env('APP_NAME')])]);
        }
        return redirect("/");
    }

    public function redirectPath()
    {
        return "/";
    }
}
