<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Hashidable;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $background
 * @property int $module_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $module_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static Builder|Category filtered()
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|Category onlyTrashed()
 * @method static Builder|Category query()
 * @method static Builder|Category whereBackground($value)
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereDeletedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereModuleId($value)
 * @method static Builder|Category whereName($value)
 * @method static Builder|Category whereSlug($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Category withoutTrashed()
 * @mixin \Eloquent
 */
class Category extends Model
{
    use Hashidable, SoftDeletes;

    protected $fillable = ["name", "slug", "background","background_image", "module_id"];
    
      protected $appends = [
        "module_name"
    ];

    public function products() {
        return $this->belongsToMany(Product::class);
    }

    public function productsCustomer() {
        return $this->belongsToMany(Product::class)->where('stock','>','0')
                    ->where('status', '=', Product::PUBLISHED)
                    ->orderBy('order', 'ASC');
    }

    public function backgroundPath(){
        return sprintf('%s/%s', '/storage/categories' , $this->background);
    }
    public function backgroundImagePath(){
        return sprintf('%s/%s', '/storage/categories' , $this->background_image);
    }

    public function getModuleNameAttribute() {
        return Module::getModule($this->module_id);
    }

    public function scopeFiltered(Builder $builder) {
        $builder->withCount("products");
      
        if (session()->has('search[admin.dashboard]')) {
            
            $builder->where('module_id', '=', session('search[admin.dashboard]'));
        }else{
            $builder->where('module_id', '=', 0);
        }
        return $builder->paginate();
    }

    public static function categoriesList(){
        return Category::get()->pluck('name','id');
    }

    public static function getCategory($module){
        return Category::where('module_id', $module)->get();
    }

   
}
