<div class="modal fade" id="recovery-modal" tabindex="-1" role="dialog" aria-labelledby="recoveryModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="recoveryModalLabel" style="color: #fff">{{ __('Recuperar contraseña') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #fff">&times;</span>
                </button>
               
            </div>
             
            <div class="modal-body">
              
                <!-- recovery form -->
                

                
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row justify-content-center">
                            
                            <div class="col-12">
                                 <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __("Correo electrónico") }}" autofocus>
                                 @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 mt-3">
                                <button type="submit" class="btn btn-dark">
                                    {{ __('Restablecer contraseña') }}
                                </button>
                            </div>
                        </div>
                    </form>
                
            
                <!-- ./recovery form -->
            </div>
            
            @if(session('error-signup'))
               
                @if ($errors->any())
                    
                    <div class="alert alert-danger mtop16" style="position:relative!important; display:block; margin-bottom: 16px">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    
                    
                @endif
                
            @endif
        </div>
    </div>
</div>
