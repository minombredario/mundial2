<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use App\Charts\AdminProfitChart;
use App\Charts\SellerProfitChart;
use ConsoleTVs\Charts\Registrar as Charts;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Charts $charts)
    {
        $charts->register([
            AdminProfitChart::class,
            SellerProfitChart::class
        ]);
    
        Schema::defaultStringLength(191);
        Carbon::setLocale(config('app.locale'));
    }
}
