@php 
    $modules = \App\Models\Module::getModulesWithCatagories();
@endphp

<div class="scrollbox">
    <div class="scrollbox-inner text-left pl-3">
        @foreach ( $modules as $key => $value)
            @if(count($modules[$key]['categories']))
                <h1>{{ $modules[$key]['name'] }}</h1>
               
                    <ul class="list-group">
                        @foreach( $modules[$key]['categories'] as $subcategory)
                            <li class="list-group-item bg-transparent">
                                <h4>
                                    <a href="{{ route('web.products', [ 'module' => $modules[$key]['slug'], 'cat' =>  $subcategory['slug'] ] )}}"> {{ $subcategory['name'] }} </a>
                                </h4>   
                            </li>
                        @endforeach
                    </ul>
               
            @endif
        @endforeach
    </div>
</div>