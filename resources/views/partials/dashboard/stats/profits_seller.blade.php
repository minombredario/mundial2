<div class="row mtop16">
    <div class="col">
        <div class="panel shadow">
            <div class="header">
                <div class="row p-3 d-flex justify-content-center">
                    <div class="col-md-4 py-1">
                        {{ Form::date("from_seller", null, ["class" => "form-control", "id" => "from_seller"]) }}

                    </div>
                    <div class="col-md-4 py-1">
                        {{ Form::date("to_seller", null, ["class" => "form-control", "id" => "to_seller"]) }}
                    </div>
                    <div class="col-md-2 py-1">
                        <button class="btn btn-sm btn-dark btn-block p-2" onclick="filter_seller.clear()">{{ __("Limpiar") }}</button>
                    </div>
                </div>
            </div>
            <div class="inside">
                
                <div id="chart_seller" style="height: 300px;"></div>
                
            </div>
        </div>
    </div>
</div>
    


@push("js")
    <script>
        let from_seller = null, to_seller = null, fromVal_seller = null, toVal_seller = null;
        const chart_seller = new Chartisan({
            el: '#chart_seller',
            url: `/api/chart/seller/profit?from=${fromVal_seller}&to=${toVal_seller}`,
            
            hooks: new ChartisanHooks()
                .colors(['#004a6b', '#041a2c'])
                .responsive()
                .beginAtZero()
                .legend({
                    position: 'bottom'
                })
                .title('{{ __("Ventas :app", ["app" => env("APP_NAME")]) }}')
                .datasets([{type: 'line', fill: true}])
                .tooltip({
                    callbacks: {
                        label: function (data) {
                            return `Ventas: ${data.value} €`;
                        }
                    }
                })
        });
        const filter_seller = {
            setFrom: (val) => {
                if (!val) return;
                const toInput = $("#to_seller");
                toVal_seller = toInput.val();
                from_seller = new Date(val);
                if (to_seller) {
                    if (to_seller.getTime() < from_seller.getTime()) {
                        toInput.val(val);
                        $("#from_seller").val(toVal_seller);
                    }
                    filter_seller.search();
                }
            },
            setTo: (val) => {
                if (!val) return;
                const fromInput = $("#from_seller");
                fromVal_seller = fromInput.val();
                to_seller = new Date(val);
                if (from_seller) {
                    if (to_seller.getTime() < from_seller.getTime()) {
                        fromInput.val(val);
                        $("#to_seller").val(fromVal_seller);
                    }
                    filter_seller.search();
                }
            },
            clear: () => {
                to_seller = null;
                from_seller = null;
                $("#to_seller, #from_seller").val(null);
                chart_seller.update({
                    url: `/api/chart/seller/profit/?from=null&to=null`
                });
            },
            search: () => {
                chart_seller.update({
                    url: `/api/chart/seller/profit/?from=${$("#from_seller").val()}&to=${$("#to_seller").val()}`
                });
            }
        }

        jQuery(document).ready(function () {
            $("#from_seller").on("input", function () {
                const thisVal = $(this).val();
                filter_seller.setFrom(thisVal);
            });
            $("#to_seller").on("input", function () {
                const thisVal = $(this).val();
                filter_seller.setTo(thisVal);
            })
        })
    </script>

@endpush
