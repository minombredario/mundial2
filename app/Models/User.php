<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword;
use App\Models\Role;

use App\Helpers\Currency;
use Laravel\Cashier\Billable;
use App\Traits\Hashidable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderLine;
use DB;
//use Laravel\Cashier\Billable;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int $status
 * @property string $role
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $password_code
 * @property string|null $avatar
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $stripe_id
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $trial_ends_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content[] $contents
 * @property-read int|null $contents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Direction[] $directions
 * @property-read int|null $directions_count
 * @property-read mixed $role_id
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \App\Models\Profile|null $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @method static Builder|User filtered()
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User processedOrders()
 * @method static Builder|User purchasedCourses()
 * @method static Builder|User query()
 * @method static Builder|User whereAvatar($value)
 * @method static Builder|User whereCardBrand($value)
 * @method static Builder|User whereCardLastFour($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePasswordCode($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereStatus($value)
 * @method static Builder|User whereStripeId($value)
 * @method static Builder|User whereTrialEndsAt($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Hashidable, SoftDeletes, Billable;

    const ADMIN = 'ADMIN';
    const SELLER = 'SELLER';
    const PROVIDER = 'PROVIDER';
    const CUSTOMER = 'CUSTOMER';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','nif', 'role', 'status', 'avatar', 'password_code', 'verification_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'password_code', 'verification_code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $appends = [
        "role_id"
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
    

    public function roles(){
        return $this->belongsToMany(Role::class)->withTimesTamps();
    }
    
    public function permissions(){
        return $this->belongsToMany(Permission::class)->withTimesTamps();
    }
    
    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function directions(){
        return $this->hasMany(Direction::class);
    }

    public function defaultDirection(){
        return $this->directions()->where('default', '1');
    }

    public function hasDefaultDirection(){
        return $this->directions()->first();
    }

    public function hasShippingDirection(){
        return session()->has('direction[shipping]');
    }

    public function ShippingDirection(){
        return session()->get('direction[shipping]');
    }

    public function profile() {
        return $this->hasOne(Profile::class);
    }

    public function contents(){
        return $this->hasMany(Content::class);
    }
 
    public function getRoleIdAttribute(){
        return count($this->roles) == 0? '2' : $this->roles[0]->id;
    }

    public function scopeProcessedOrders() {
        return $this->orders()
            ->where("status", Order::SUCCESS)
            ->with("order_lines")
            ->withCount("order_lines")
            ->paginate(9);
    }

    public function scopeDirectionsUser() {
        return $this->directions()->paginate();
    }

    public function accessDashboard() {
        
        return ($this->role === User::SELLER || $this->role === User::PROVIDER || $this->role === User::ADMIN);
    }

    public function isAdmin() {
        
        return $this->role === User::ADMIN;
    }

    public function isProvider() {
        
        return $this->role === User::PROVIDER ? false : true;
    }

    public function isSeller() {
        
        return $this->role === User::SELLER;
    }

    public function havePermission($permission){
        $role_permission = false;
        $user_permission = false;
        $user = request('user');

        foreach ($this->roles as $role ) {
            
            if ($role['full-access'] =='yes' ) {
                return true;
            }

            foreach ($role->permissions as $perm) {
                
                if ($perm->slug == $permission ) {
                    $role_permission = true;
                    //return true;
                }   
            }
            if($user != null):
                foreach ($user->permissions as $perm) {
                    
                    if ($perm->slug == $permission ) {
                        $user_permission = true;
                        //return true;
                    }   
                }
            endif;
        }
        return ($role_permission ||  $user_permission);
        //return false;
        
    }

    public function scopeFiltered(Builder $builder) {
        
        if(auth()->user()->role !== User::ADMIN){
            $builder->where("role", User::CUSTOMER);
            $builder->with("orders.order_lines.product")
                    ->whereHas("orders.order_lines.product", function ($query) {
                        $query->where("user_id", auth()->id());
                    })->with(['orders' => function($query){
                        $query->where('status','success');
                    }]);

            
            $builder->with('defaultDirection');
       
        }
        
        if(request()->has('name')){
            switch (request('filter')) {
                case '0':
                    $field = 'name';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%');
                    break;
                case '1':
                    $field = 'email';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%');
                    break;
                case '2':
                   $field = 'country';
                   $builder->with('defaultDirection')
                        ->whereHas('defaultDirection', function($query){
                            $query->where('country', 'LIKE', '%' . e(request('name')) . '%');
                        });
                    break;
                case '3':
                    $field = 'city';
                    $builder->with('defaultDirection')
                            ->whereHas('defaultDirection', function($query){
                                $query->where('city', 'LIKE', '%' . e(request('name')) . '%');
                            });
                    break;
                case '4':
                    $field = 'state';
                    $builder->with('defaultDirection')
                            ->whereHas('defaultDirection', function($query){
                                $query->where('state', 'LIKE', '%' . e(request('name')) . '%');
                            });
                    break;
                default:
                    $field = 'name';
                    break;
            }
            
        }
 
        if(request()->has('search')){
            switch (request('search')) {
                case 'registered':
                    $builder->where('status', 1);
                    break;
                case 'verified':    
                    $builder->where('status', 0);
                    break;
                case 'locked':
                    $builder->where("status", 100);
                    break;
                default:
                   $builder;
                    break;
            }
        };

        
        return $builder->where('role', user::CUSTOMER)->paginate(9);
    }

    public function scopeFilteredSeller(Builder $builder) {
        
        $builder->with('defaultDirection')->where('role', user::SELLER);
         if(request()->has('name')){
            switch (request('filter')) {
                case '0':
                    $field = 'name';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%');
                    break;
                case '1':
                    $field = 'email';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%');
                    break;
                case '2':
                   $field = 'country';
                   $builder->with('defaultDirection')
                        ->whereHas('defaultDirection', function($query){
                            $query->where('country', 'LIKE', '%' . e(request('name')) . '%');
                        });
                    break;
                case '3':
                    $field = 'city';
                    $builder->with('defaultDirection')
                            ->whereHas('defaultDirection', function($query){
                                $query->where('city', 'LIKE', '%' . e(request('name')) . '%');
                            });
                    break;
                case '4':
                    $field = 'state';
                    $builder->with('defaultDirection')
                            ->whereHas('defaultDirection', function($query){
                                $query->where('state', 'LIKE', '%' . e(request('name')) . '%');
                            });
                    break;
                default:
                    $field = 'name';
                    break;
            }
            
        }
 
 
        if(request()->has('search')){
            switch (request('search')) {
                case 'registered':
                    $builder->where('status', 0)->where('role', user::SELLER);
                    break;
                case 'verified':    
                    $builder->where('status', 1)->where('role', user::SELLER);
                    break;
                case 'locked':
                    $builder->where("status", 100)->where('role', user::SELLER);
                    break;
                default:
                   $builder;
                    break;
            }
        };

        
        return $builder->paginate(9);
    }

     public function scopeFilteredProvider(Builder $builder) {
        
        $builder->with('defaultDirection')->where('role', user::PROVIDER);
         if(request()->has('name')){
            switch (request('filter')) {
                case '0':
                    $field = 'name';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%');
                    break;
                case '1':
                    $field = 'email';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%');
                    break;
                case '2':
                   $field = 'country';
                   $builder->with('defaultDirection')
                        ->whereHas('defaultDirection', function($query){
                            $query->where('country', 'LIKE', '%' . e(request('name')) . '%');
                        });
                    break;
                case '3':
                    $field = 'city';
                    $builder->with('defaultDirection')
                            ->whereHas('defaultDirection', function($query){
                                $query->where('city', 'LIKE', '%' . e(request('name')) . '%');
                            });
                    break;
                case '4':
                    $field = 'state';
                    $builder->with('defaultDirection')
                            ->whereHas('defaultDirection', function($query){
                                $query->where('state', 'LIKE', '%' . e(request('name')) . '%');
                            });
                    break;
                default:
                    $field = 'name';
                    break;
            }
            
        }
 
 
        if(request()->has('search')){
            switch (request('search')) {
                case 'registered':
                    $builder->where('status', 0)->where('role', user::PROVIDER);
                    break;
                case 'verified':    
                    $builder->where('status', 1)->where('role', user::PROVIDER);
                    break;
                case 'locked':
                    $builder->where("status", 100)->where('role', user::PROVIDER);
                    break;
                default:
                   $builder;
                    break;
            }
        };

        
        return $builder->paginate(9);
    }

    public static function statusTypes($status = null){
    
        switch ($status) {
            
            case '0':
                return  __("Registrado");
                break;

            case '1':
                return  __("Verificado");
                break;

            case '100':
                return  __("Bloqueado");
                break;
            default:
                return [
                    '0'     => __("Registrado"),
                    '1'     => __("Verificado"),
                    '100'   => __("Bloqueado") 
                ];
                break;
        }
    }

    public static function roleTypes($role = null){
        switch ($role) {
            case self::ADMIN:
                return  __("Administrador");
                break;

            case self::SELLER:
                return  __("Distribuidor");
                break;
            
            case self::PROVIDER:
                return  __("Proveedor");
                break;

            case self::CUSTOMER:
                return  __("Cliente final");
                break;
            default:
                return [
                    self::ADMIN     => __("Administrador"),
                    self::SELLER    => __("Distribuidor"),
                    self::PROVIDER  => __("Proveedor"),
                    self::CUSTOMER  => __("Cliente final")
                ];
                break;
        }
    }

    public static function usersSeller(){
       
       return User::whereIn('role', [self::SELLER, self::PROVIDER])->pluck('name','id');
    }

    public static function order_line_seller($customer){

        /*
        SELECT `orders`.`invoice_id`, `orders`.`total_amount`, SUM(`products`.`price`) as "total" FROM `orders`, `order_lines`, `products`
        WHERE `order_lines`.`product_id` = `products`.`id` 
        and `products`.`user_id` = 2
        and `orders`.`user_id` = 4 
        and `orders`.`id` = `order_lines`.`order_id`
        and `orders`.`status` = "SUCCESS"
        group by `orders`.`invoice_id`
        ORDER BY `orders`.`invoice_id` ASC
        */
        $seller = auth()->user();
        
        if($seller->role == self::SELLER){
            $orderLines = DB::select("SELECT `orders`.`invoice_id`, `orders`.`status`,`orders`.`created_at`, SUM(`products`.`price`) as 'total'
                    FROM `orders`, `order_lines`, `products`, `users`
                    WHERE `order_lines`.`product_id` = `products`.`id` 
                    and `products`.`user_id` = $seller->id
                    and `orders`.`user_id` = $customer->id
                    and `orders`.`id` = `order_lines`.`order_id`
                    and `orders`.`status` = 'success'
                    group by `orders`.`invoice_id`
                    ORDER BY `orders`.`created_at` DESC");
        }else{
            $orderLines = DB::select("SELECT `orders`.`invoice_id`, `orders`.`status`,`orders`.`created_at`, SUM(`products`.`price`) as 'total'
                    FROM `orders`, `order_lines`, `products`, `users`
                    WHERE `order_lines`.`product_id` = `products`.`id` 
                    and `orders`.`user_id` = $customer->id
                    and `orders`.`id` = `order_lines`.`order_id`
                    and `orders`.`status` = 'success'
                    group by `orders`.`invoice_id`
                    ORDER BY `orders`.`created_at` DESC");
        }
        

       return Collect($orderLines);
    }



}
