<!DOCTYPE html>
<html lang="es">
    <head>
        <title>@yield('title') - {{ config('app.name') }}</title>
        <meta charset="UTF-8">
        <meta name="description" content="{{ config('mundial_souvenir.meta_description') }}">
        <meta name="keywords" content="{{ config('mundial_souvenir.meta_keywords') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

         {{-- ADD CSRF TOKEN FOR AJAX OPERATIONS --}}
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta property="og:image" content="{{ url('/storage/web/logonegro.png') }}"/>

        <!-- Favicon -->
        <link href="{{ url('/img/favicon.ico') }}" rel="shortcut icon"/>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ url('/css/bootstrap5.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fontawesome/all.css') }}">
        <link rel="stylesheet" href="{{ url('/css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ url('/css/owl.theme.default.css') }}">
        <link rel="stylesheet" href="{{ url('/static/css/template.css?v='.time()) }}">
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap">
        
        
        
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        @stack('css')
    </head>
    <body>
        <main id="content">
             @if(session('error-login'))
                <div class="form-errors">
                    <p>{{ session('error-login') }}</p>
                </div>
            @endif
            <div class="flash-message">
            @foreach(['danger','warning','success','info'] as $msg)
                @if(Session::has('alert-' .  $msg ))
                    <p class="alert alert-{{ $msg }}">
                        {{Session::get('alert-' . $msg ) }}
                    </p>
                @endif
            @endforeach
            </div>
            <header class="header row g-0">
                <div class="col-sm-12 col-md-6">
                    <a href="{{ route('home') }}" class="header__logo">
                        <img src="{{ url('/storage/web/logoblanco.png') }}" class="img-fluid logo" alt="" >
                    </a>
                </div>
                
                
                <div class="pt-md-5 pt-sm-2 col-sm-12 col-md-6 text-sm-center">
                    @yield('page')
                </div>
                
            </header>
            <div class="clearfix"></div>
           
			@include('partials.web.sidebar')
            @component('components.alert-component')@endcomponent
            <div id="app"> 
            @yield('content')
            </div>
            
        </main>
        
        

    <!--====== Javascripts & Jquery ======-->
    <script src="{{ url('/js/fontawesome/all.js') }}"></script> 
    <script src="{{ url('/js/jquery.min.js') }}"></script> 
    <script src="{{ url('/js/jquery-ui.js') }}"></script> 
    <script src="{{ url('/js/popper.min.js') }}"></script> 
    <script src="{{ url('/js/bootstrap5.min.js') }}"></script> 
    <script src="{{ url('/js/app.js') }}"></script>
    <script src="{{ url('/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('/js/jConfirm.js') }}"></script>
    <script src="{{ url('/js/jquery.validate.min.js') }}"></script>
    <script src="{{ url('/js/additional-methods.min.js') }}"></script>
    <script src="{{ url('/js/functions.js') }}"></script>
    
    <script>
        $('.alert').slideDown();
        setTimeout(() => {
            $('.alert').slideUp();
        },10000);

        jQuery(document).ready(function() {
            $.jConfirm.defaults.question = '{{ __("¿Estás seguro?") }}';
            $.jConfirm.defaults.confirm_text = '{{ __("Sí") }}';
            $.jConfirm.defaults.deny_text = '{{ __("No") }}';
            $.jConfirm.defaults.position = 'top';
            $.jConfirm.defaults.theme = 'black';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
        
    </script>

    

    @stack('js')

    </body>
</html>
