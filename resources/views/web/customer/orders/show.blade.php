@extends("layouts.web")
@push('css')
    <style>
        .row-content{
            background: #fff;
        }
    </style>
@endpush
@section('title',  __('Mis Pedidos') )

@section('page')
    
    <h1 class="title-page text-md-left text-sm-center"><a href="{{ url()->previous() }}" class="text-decoration-none text-reset">{{ __("Volver") }} </a></h1>

@endsection
@section("content")
    <!-- order detail section -->
    <section class="profile">
        <div class="container wrapper-container"></div>
        <div class="container row-content">
            <div class="row">
                <div class="table-responsive">
                    <table class="table">
                        @include("partials.web.customer.orders.order_thead", ["detail" => true])
                        <tbody>
                            @include("partials.web.customer.orders.order_row", ["detail" => true])
                        </tbody>
                    </table>
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{ __("Producto") }}</th>
                                <th>{{ __("Unidades") }}</th>
                                <th>{{ __("Precio unidad") }}</th>
                                <th>{{ __("Descuento") }}</th>
                                <th>{{ __("Precio Total") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($order->order_lines as $line)
                            <tr class="text-center">
                                <td>{{ $line->product->name }}</td>
                                <td>{{ $line->quantity }}</td>
                                <td>{{ $line->formatted_price }}</td>
                                <td>{{ $line->discount == null ? 0 : $line->discount}} {{ \App\Models\Product::discountIcon($line->discount_type) }} </td>
                                <td>{{ \App\Helpers\Currency::formatCurrency($line->price_total) }}</td>
                                
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row justify-content-center p-2">
                <a href="{{ route("customer.orders.download_invoice", ["order" => $order]) }}" class="btn btn-dark w-100">
                    <i class="fas fa-file-download"></i>  {{ __("Descargar factura") }}
                </a>
            </div>
        </div>
    </section>
    <!-- order detail end section -->
@endsection
