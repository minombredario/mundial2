<div class="row mtop16">
    <div class="col">
        <div class="panel shadow">
            <div class="header">
                <div class="row p-3 d-flex justify-content-center">
                    <div class="col-md-4 py-1">
                        {{ Form::date("from_admin", null, ["class" => "form-control", "id" => "from_admin"]) }}

                    </div>
                    <div class="col-md-4 py-1">
                        {{ Form::date("to_admin", null, ["class" => "form-control", "id" => "to_admin"]) }}
                    </div>
                    <div class="col-md-2 py-1">
                        <button class="btn btn-sm btn-dark btn-block p-2" onclick="filter_admin.clear()">{{ __("Limpiar") }}</button>
                    </div>
                </div>
            </div>
            <div class="inside">
                
                <div id="chart_admin" style="height: 300px;"></div>
                
            </div>
        </div>
    </div>
</div>
    


@push("js")
    <script>
        let from_admin = null, to_admin = null, fromVal_admin = null, toVal_admin = null;
        const chart_admin = new Chartisan({
            el: '#chart_admin',
            url: `/api/chart/admin/profit?from=${fromVal_admin}&to=${toVal_admin}`,
            
            hooks: new ChartisanHooks()
                .colors(['#004a6b', '#041a2c'])
                .responsive()
                .beginAtZero()
                .legend({
                    position: 'bottom'
                })
                .title('{{ __("Beneficios :app", ["app" => env("APP_NAME")]) }}')
                .datasets([{type: 'line', fill: true}])
                .tooltip({
                    callbacks: {
                        label: function (data) {
                            return `Beneficios: ${data.value} €`;
                        }
                    }
                })
        });
        const filter_admin = {
            setFrom: (val) => {
                if (!val) return;
                const toInput = $("#to_admin");
                toVal_admin = toInput.val();
                from_admin = new Date(val);
                if (to_admin) {
                    if (to_admin.getTime() < from_admin.getTime()) {
                        toInput.val(val);
                        $("#from_admin").val(toVal_admin);
                    }
                    filter_admin.search();
                }
            },
            setTo: (val) => {
                if (!val) return;
                const fromInput = $("#from_admin");
                fromVal_admin = fromInput.val();
                to_admin = new Date(val);
                if (from_admin) {
                    if (to_admin.getTime() < from_admin.getTime()) {
                        fromInput.val(val);
                        $("#to_admin").val(fromVal_admin);
                    }
                    filter_admin.search();
                }
            },
            clear: () => {
                to_admin = null;
                from_admin = null;
                $("#to_admin, #from_admin").val(null);
                chart_admin.update({
                    url: `/api/chart/admin/profit/?from=null&to=null`
                });
            },
            search: () => {
                chart_admin.update({
                    url: `/api/chart/admin/profit/?from=${$("#from_admin").val()}&to=${$("#to_admin").val()}`
                });
            }
        }

        jQuery(document).ready(function () {
            $("#from_admin").on("input", function () {
                const thisVal = $(this).val();
                filter_admin.setFrom(thisVal);
            });
            $("#to_admin").on("input", function () {
                const thisVal = $(this).val();
                filter_admin.setTo(thisVal);
            })
        })
    </script>

@endpush
