<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Permission, App\Models\User, App\Models\Role;
use DB;

trait ManagePermissions {

	public function permissions(){

        $this->authorize('haveaccess','user.permissions.index');

        $permissions = [
            'dashboard' => [
                'icon' => '<i class="fas fa-home"></i>',
                'title' => 'Módulo de Dashboard.',
                'keys' => collect(Permission::where('module','dashboard')->get()),
            ],

            'products' => [
                'icon' => '<i class="fas fa-boxes"></i>',
                'title' => 'Módulo de productos.',
                'keys' => collect(Permission::where('module','product')->get()),
            ],
        

            'categories' => [
                'icon' => '<i class="fas fa-folder-open"></i>',
                'title' => 'Módulo de categorias.',
                'keys' =>  collect($categories = Permission::where('module','category')->get()),
            ],

            'users' => [
                'icon' => '<i class="fas fa-user-friends"></i>',
                'title' => 'Módulo de usuarios.',
                'keys' => collect(Permission::where('module','user')->get()),
            ],
            

            'order' => [
                'icon' => '<i class="fas fa-shopping-basket"></i>',
                'title' => 'Módulo de Pedidos.',
                'keys' => collect(Permission::where('module','order')->get()),
            ],

            
        ];
        if(Auth()->user()->role == User::ADMIN){
            $permissions['roles'] = [
                'icon' => '<i class="fas fa-user-shield"></i>',
                'title' => 'Módulo de Roles.',
                'keys' => collect(Permission::where('module','role')->get()),
            ];
        }
        

        $user = request('user');
        $user->load('permissions')->load('roles');
        $permissions_role = Role::findOrFail($user->roles[0]->id)->load('permissions');
        
        $title = __("Editar permisos de :user", ["user" => $user->name]);
        $textButton = __("Actualizar");
        $options = ['route' => ['admin.dashboard.users.permissions.update', ["user" => $user]], 'files' => true];
        $update = true;
        $required = true;

        return view('dashboard.users.permissions.index', compact('title', 'options', 'textButton', 'update', 'required', 'permissions','user', 'permissions_role'));  
    }
  

    public function updatePermissionsUser(User $user){
        
        $this->authorize('haveaccess','user.permissions.edit');
        
        try {
            DB::beginTransaction();
                $user->permissions()->sync(request("permission"));
            DB::commit();

            session()->flash("message", ["success", __("Permisos actualizados satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }
    
}