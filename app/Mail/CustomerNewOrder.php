<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\User;
use App\Models\Direction;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Laravel\Cashier\Invoice;

class CustomerNewOrder extends Mailable
{
    use Queueable, SerializesModels;

    public User $customer;
    public Order $order;
    public ?Invoice $invoice;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $customer, Order $order)
    {
       $this->customer = $customer;
       $this->order = $order;
       $this->invoice = $this->customer->findInvoice($this->order->invoice_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
            
        $data_direction = Direction::findOrFail($this->order->direction_id);
        $admin = User::where('role',User::ADMIN)->with('profile')->with('defaultDirection')->first();       
                
        $street = $admin->defaultDirection[0]->line_1;
        $location = $admin->defaultDirection[0]->postal_code . " " . $admin->defaultDirection[0]->country . " (" . $admin->defaultDirection[0]->city . ")";
        $phone = $admin->defaultDirection[0]->phone;
        $vendorVat = $admin->defaultDirection[0]->nif;
        $mail = $admin->email;
        $title = __('Factura');
        $data_direction = $data_direction;
               
        $vendor = $admin->profile->company;
        $product = __("Compra de productos");
        $invoice = $this->invoice;
        $owner = $this->customer;
        $order = $this->order;
        $pdf = \PDF::loadview('vendor.cashier.receipt', compact('invoice', 'product', 'vendor', 'owner', 'street','location','phone','vendorVat','mail','title','data_direction', 'order'));
        return $this
            ->attachData($pdf->output(), $this->invoice->id . '-' . date('d-m-Y') . '.pdf', ['mime' => 'application/pdf'])
            ->subject("Gracias por tu pedido - " . config("app.name"))
            ->markdown("emails.customers.new_order");
    }
}
