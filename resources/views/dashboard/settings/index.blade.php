@extends("layouts.dashboard")

@section('title', __('Configuración') )


@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.settings') }}">
        <i class="fas fa-cogs"></i> {{ __('Configuración') }}
    </a>
</li>
@endsection

@section("content")
	@include("dashboard.settings.form")
@endsection