<nav class="navbar shadow">
	
		<ul class="navbar-nav ml-auto ml-3">
			<li class="nav-item">
				<i class="far fa-user"></i> {{ Auth::user()->name }} <i class="fas fa-chevron-down"></i>
				<ul>
					<li>
						<a href="/" data-toggle="tooltip" data-placement="left" title="{{ __('Web') }}">
							<i class="fas fa-globe"></i>
							{{ config('app.name') }}
						</a>
					</li>
					@can('haveaccess', 'userown.edit')
					<li>
						<a href="{{ route("admin.dashboard.profile") }}" class="profile" data-toggle="tooltip" data-placement="left" title="{{ __('Mi perfil') }}">
							<i class="fas fa-id-card"></i>
							<span> {{ __('Mi perfil') }}</span>
						</a>
					</li>
					@endcan
					@admin
						<li>
							<a href="{{ route("admin.dashboard.settings") }}" class="settings" data-toggle="tooltip" data-placement="left" title="{{ __('Configuración') }}"> 
								<i class="fas fa-cogs"></i>
								<span> {{ __('Configuración') }}</span>
							</a>
						</li>
					@endadmin
					<li>
						<a href="{{ route('logout') }}" data-toggle="tooltip" data-placement="left" title="{{ __('Salir') }}"
						onclick="event.preventDefault();
										document.getElementById('logout-form').submit();">
							<i class="fas fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span>
						</a>
			
						<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
							@csrf
						</form>
					</li>
				</ul>
			</li>
		</ul>
	
</nav>
