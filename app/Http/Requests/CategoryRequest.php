<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case "POST": {
                return [
                    'name' => 'required|unique:categories,name',
                    'background' => 'required|mimes:mp4,mov,ogg,qt|max:20000',//'required|file|max:30720',//'required|image|mimes:jpg,jpeg,png'
                    'background_image' =>  'required|image|mimes:jpg,jpeg,png'
				];
            }   
            case "PUT": {
                return [
                    'name' => 'required|unique:categories,name,'. $this->route('category')->id,
                    'background' => request('background') != null ? 'sometimes|required|file|max:30720': '', //'required|sometimes|file|max:30720'
                    'background_image' =>  request('background_image') != null ? 'required|image|mimes:jpg,jpeg,png' : '',
                ];
            }
            default: {
                return [];
            }
        }
    }

}

