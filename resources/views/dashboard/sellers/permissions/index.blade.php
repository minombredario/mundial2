@extends("layouts.dashboard")

@section('title', __('Permisos'))

@section("content")
	@include("dashboard.users.permissions.list")
@endsection