
<table class="table table-hover table-striped table-sm">
    <thead>
        <tr>
            <th scope="col">#{{ __('Factura') }}</th>
            <th scope="col">{{ __('Fecha') }}</th>
            <th scope="col">{{ __('Total') }}</th>
            <th scope="col">{{ __('Estado') }}</th>
            <th scope="col"></th>
        </tr>
    </thead>
    
    <tbody>
        
        @foreach( \App\Models\User::order_line_seller($user) as $order)
           <tr>
                <td class="align-middle">{{ $order->invoice_id }}</td>
                <td class="align-middle">{{  Carbon\Carbon::parse($order->created_at)->format('d-m-Y') }}</td>
                <td class="align-middle">{{ \App\Helpers\Currency::formatCurrency($order->total) }}</td>
                <td class="align-middle">{{ $order->status }}</td>
                <td class="align-middle">
                    <div class="opts">
                        <a href="{{ route('admin.dashboard.users.orders.show', $order->invoice_id )}}" data-toggle="tooltip" data-placemente="top" title="{{ __('Ver') }}">
                            <i class="fas fa-eye"></i>
                        </a>
                    </div>
                </td>
           </tr>
        @endforeach
    </tbody>
        
    
</table>
    
                