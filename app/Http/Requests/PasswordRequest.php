<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){

        //$this->redirect = 'customer/profile';
        
        switch ($this->method()) {
            
            case "PUT": {
                return [
                    'apassword' => 'required',
                    'password' =>  'required|min:8',
                    'cpassword' => 'required|min:8|same:password',
                ];
            }
            default: {
                return [];
            }
        }
    }

    public function messages()
{
    return [
       
        'apassword.required' => 'Escriba su contraseña actual',
        'apassword.min' => 'La contraseña actual debe de tener al menos 8 caracteres',
        'password.required' => 'Escriba su nueva contraseña',
        'password.min' => 'Su nueva contraseña debe de tener al menos 8 caracteres',
        'cpassword.required' => 'Confirme su nueva contraseña',
        'cpassword.min' => 'La confirmación de la contraseña debe de tener al menos 8 caracteres',
        'cpassword.same' => 'Las contraseñas deben de ser iguales'
    ];
}
}
