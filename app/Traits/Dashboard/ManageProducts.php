<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Product, App\Models\Category, App\Models\User;
use App\Http\Requests\ProductRequest;
use App\Helpers\Uploader;
use DB;
use Str;

trait ManageProducts {

	public function products(){
        $this->authorize('haveaccess','product.index');

        $products = Product::Filtered();
        return view('dashboard.products.index', compact('products'));
    }

    public function filterProduct(){
     
        $this->resetSessions();

        if (request('filter')) {
            session()->put('filter[admin.dashboard]', request('filter'));
            session()->save();
        }
    
        return redirect(route('admin.dashboard.products'));
        
    }
    
    public function createProduct() {

        $this->authorize('haveaccess','product.create');

        $product = new Product;
        $title = __("Crear Producto");
        $textButton = __("Guardar");
        $options = ['route' => ['admin.dashboard.products.store'], 'files' => true];
        $required = false;
        return view('dashboard.products.create', compact('title', 'product', 'options', 'textButton', 'required'));

    }

    public function storeProduct(ProductRequest $request, Product $product) {
       
        $this->authorize('haveaccess','product.create');
        
        try {
            DB::beginTransaction();
            $file = null;
            if ($request->hasFile('picture')) {
                $file = Uploader::uploadFile('picture', 'products');
            }
            
            $product = Product::create($this->productInput($file, is_null($product->featured)) ?? false);
            $product->categories()->sync(request("categories"));

            DB::commit();
            session()->flash("message", ["success", __("Producto creado satisfactoriamente")]);
            return redirect(route('admin.dashboard.products.edit', ['product' => $product]));
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function editProduct(Product $product) {

        $this->authorize('haveaccess','product.edit');

        $title = __("Editar el :product", ["product" => $product->name]);
        $textButton = __("Actualizar producto");
        $options = ['route' => ['admin.dashboard.products.update', ["product" => $product]], 'files' => true];
        $update = true;
        $required = true;
        return view('dashboard.products.edit', compact('title', 'product', 'options', 'textButton', 'update', 'required'));
    }

    public function updateProduct(ProductRequest $request, Product $product) {
        
        $this->authorize('haveaccess','product.edit');

        try {
            DB::beginTransaction();
            $file = $product->picture;
            if ($request->hasFile('picture')) {
                if ($product->picture) {
                    Uploader::removeFile("products", $product->picture);
                }
                $file = Uploader::uploadFile('picture', 'products');
            }

                $product->fill($this->productInput($file))->save();
                $product->categories()->sync(request("categories"));
            DB::commit();

            session()->flash("message", ["success", __("Product actualizado satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }
    
    public function destroyProduct(Product $product){   

        $this->authorize('haveaccess','product.destroy');

        try {
            if (request()->ajax()) {
                if ($product->picture) {
                    Uploader::removeFile("products", $product->picture);
                }
                $product->delete();
                session()->flash("message", ["success", __("El product :product ha sido eliminada correctamente", ["product" => $product->name])]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }
    }

    protected function productInput(string $file = null, bool $featured = false): array {
        $adminID = User::where('role', User::ADMIN)->pluck('id')->first();
        $profit = null;
        $order = 99999;
        if(Auth()->user()->role === User::PROVIDER){
            $profit = Config('mundial_souvenir.profit_to_provider');
        }
        if(Auth()->user()->role === User::ADMIN){
            $profit = request('profit');
            $order = request('order');
        }
       
        return [
            'status' => Auth()->user()->role === User::ADMIN ? request('status') : Product::PENDING,
            'user_id' => Auth()->user()->role === User::ADMIN ?  request('user_id') : auth()->id(),
            'code' => e(request('code')),
            'name' => e(request('name')),
            'slug' => Str::slug(request('name')),
            'category_id' => request('category'),
            'picture' => $file,
            'price' => request('price'),
            'profit' => $profit,
            'order' => $order,
            'stock' => is_null(request('stock'))? 0 : request('stock'),
            'in_discount' => is_null(request('in_discount'))? 0 : request('in_discount'),
            'discount_type' => is_null(request('discount_type'))? Product::PRICE : request('discount_type'),
            'discount' =>  is_null(request('discount'))? 0 : request('discount'),
            'description' => e(request('description')),
            "featured" => false, //$featured
        ];
    }

    protected function resetSessions(){
        session()->remove('filter[admin.dashboard]');
        session()->remove('search[admin.dashboard]');
    }
    
}