@extends("layouts.dashboard")

@section('title', __("Role " . $role->name) )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles') }}">
        <i class="fas fa-user-shield"></i> {{ __('Roles') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles.edit', $role) }}">
        <i class="fas fa-user-shield"></i> {{ $role->name }} 
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.roles.form")
@endsection