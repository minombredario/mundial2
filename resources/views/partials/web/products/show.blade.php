

<div class="product">
    <div class="title text-center">
        <h3 class="text-uppercase">{{ $product->name }}</h3>
    </div>
    <div class="row content">
        <div class="col-6 d-flex align-items-center">
            <img  src="{{ $product->imagePath() }}" alt="" class="img-fluid">
        </div>
        <div class="col-6 ">
            <div class="description-wrapper">
                <div class="description">
                    <p>
                        {!! htmlspecialchars_decode($product->description) !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            @if($product->in_discount)
                <div class="price"><del><small>{{ $product->formatted_price }}</small></del><br/><h4> {{ $product->priceWithDiscount() }} </h4></div>
            @else
                <div class="price"> <h4>{{ $product->formatted_price }}</h4></div>
            @endif
        </div>
    
        <div class="col-12 mt-2 d-flex justify-content-center">
            {!! Form::model($product, ['route' => ['add_product_to_cart', ["product" => $product]], 'files' => true]) !!}
                
            <div class="input-group mb-3">
                {!! Form::number('qty', "1" , ['min' => '0', 'max' => $product->stock, 'step' => '1' ]) !!}

                <button type="submit" class="btn btn-cart mx-1"> <i class="fas fa-shopping-cart cart-icon"></i> <span class="cart-text">{{ __('AÑADIR AL CARRITO') }}</span></button>
                
                
            </div>
            {!! Form::close() !!}

                
            
        </div>
    </div>
    
</div>

