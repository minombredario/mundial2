@component('mail::message')
# Hola {{ $user->name }},
<br><br>
<h2>{{ __('Bienvenido a :web', ['web' => config('app.name') ]) }}</h2>

<p>
    {{ __('Haga clic en el botón de abajo para verificar su dirección de correo electrónico.') }}
</p>

@component('mail::button', [
    'url' => 'http://127.0.0.1:8000/verify?code=' . $user->verification_code
])
    {{ __('Confirmar') }}
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent