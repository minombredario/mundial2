@extends("layouts.dashboard")

@section('title', __('Proveedores') )

@section("content")
	@include("dashboard.providers.list")
@endsection