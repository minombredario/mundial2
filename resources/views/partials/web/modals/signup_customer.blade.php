<div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="signupModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="signupModalLabel" style="color: #fff">{{ __("Registra tu cuenta") }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #fff">&times;</span>
                </button>
               
            </div>
             
            <div class="modal-body">
              
                <!-- signup form -->
                <form class="text-center" action="{{ route('register') }}" method="POST">
                    @csrf
                    <input type="hidden" name="role" value="{{ \App\Models\User::CUSTOMER }}" />

                    <div class="row justify-content-center">
                       
                        <div class="row mb-2">
                            <div class="col-12">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="{{ __("Nombre") }}" required/>
                            </div>
                        </div>
                     

                        <div class="row mb-2">
                            <div class="col-12">
                                <input type="email" class="form-control" name="email" class="last-s" value="{{ old('email') }}" placeholder="{{ __("Correo electrónico") }}" required />
                            </div>
                        </div>

                         <div class="row mb-2">
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password" placeholder="{{ __("Contraseña") }}" required/>
                            </div>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation" class="last-s" placeholder="{{ __("Confirma la contraseña") }}" required/>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 mt-3">
                            <button type="submit" class="btn btn-dark">{{ __("Crear cuenta") }}</button>
                        </div>
                    </div>
                </form>
                <!-- ./signup form -->
            </div>
            
            @if(session('error-signup'))
               
                @if ($errors->any())
                    
                    <div class="alert alert-danger mtop16" style="position:relative!important; display:block; margin-bottom: 16px">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    
                    
                @endif
                
            @endif
        </div>
    </div>
</div>
