<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Traits\Dashboard\ManageRoles;

use Illuminate\Support\Facades\Gate;


class RoleController extends Controller
{
    use ManageRoles;

    public function index()
    {
        Gate::authorize('haveaccess','role.index');

        $roles =  Role::orderBy('id','Desc')->paginate(2);

        return view('role.index',compact('roles'));
    }
}
