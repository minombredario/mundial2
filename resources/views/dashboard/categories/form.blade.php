

<?php $readonly = true; $disabled = 'disabled' ?>

@can('haveaccess', 'category.edit')
	<?php $readonly = false; $disabled = ''?>
@endcan

@can('haveaccess', 'category.update')
	<?php $readonly = false; $disabled = ''?>
@endcan


{!! Form::model($category, $options) !!}
     @isset($update)
        @method("PUT")
    @endisset
    {!! Form::label('name', __("País")) !!}
    <div class="input-group">
        {!! Form::label('name', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
        {!! Form::text('name', null, ['class' => 'form-control' .($errors->has('name') ? ' is-invalid' : '') ]) !!}
    </div>
    @if ($errors->has('name'))
        <div class="{{$errors->has('name') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('name') ? 'display:block' : ''}}">
            <strong>{{ $errors->first('name') }}</strong>
        </div>
    @endif

    {!! Form::label('module_id', __("Continente:"), ['class' => "mtop16"]) !!}
    <div class="input-group">
        {!! Form::label('module_id', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
        {!! Form::select('module_id', \App\Models\Module::getModules('name','id') , null,['class' => 'form-select']) !!}
    </div>

    {!! Form::label('background', __("Vídeo:"), ['class' => "mtop16"]) !!}
    <div class="input-group">
      
        {!! Form::label('background', '<i class="fas fa-film"></i>', [ 'class' => 'input-group-text'], false) !!}
        <div class="form-file ">
            {!! Form::file('background', [ $required, 'class' => 'form-file-input', 'id' => 'customFileVideo', 'accept' => "video/*" ]) !!}
            <label class="form-file-label" for="customFile">
                {!! Form::label('background', __("Selecciona un vídeo"), ['class' => "form-file-text", 'id' => 'file-label']) !!}
            </label>
        </div>
    </div>

    {!! Form::label('background_image', __("Imagen:"), ['class' => "mtop16"]) !!}
    <div class="input-group">
      
        {!! Form::label('background_image', '<i class="fas fa-image"></i>', [ 'class' => 'input-group-text'], false) !!}
        <div class="form-file ">
            {!! Form::file('background_image', [ $required, 'class' => 'form-file-input', 'id' => 'customFileImage', 'accept' => "image/*" ]) !!}
            <label class="form-file-label" for="customFile">
                {!! Form::label('background_image', __("Selecciona una imagen"), ['class' => "form-file-text", 'id' => 'file-label-image']) !!}
            </label>
        </div>
    </div>
    
    @can('haveaccess', 'category.create')
        <div class="row mtop16">
            <div class="col-md-12">
                {!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2 w-100']); !!}
            </div>
        </div>
    @endcan
{!! Form::close() !!}

@push("js")
    <script>
        jQuery(document).ready(function () {
            
            $('#customFileVideo').on('change', function() {
                var video = $('#customFileVideo')[0].files[0].name;
                $('#file-label').text(video);
               
            });
            $('#customFileImage').on('change', function() {
                var video = $('#customFileImage')[0].files[0].name;
                $('#file-label-image').text(video);
               
            });
        });
    </script>
@endpush