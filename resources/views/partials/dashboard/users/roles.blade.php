<div class="panel shadow">
	<div class="header d-flex justify-content-between">
		<h2 class="title">
			{!! $title !!} 
		</h2>
		@admin
			@if($view == 'put' || $view == 'show')
				<a class="btn btn-dark" href="{{ route('admin.dashboard.users.permissions', $user)}}" data-toggle="tooltip" data-placemente="top" title="{{ __('Permisos de usuario') }}">
					<i class="fas fa-cogs" title="{{ __('Permisos de usuario') }}"></i>  {{ __('Permisos de usuario') }}
				</a>
			@endif
		@endadmin
	</div>
	<div class="inside">
		<div class="row">
			<div class="col-md-12">
				{!! Form::label('role', __("Role principal de usuario:")) !!}
				<div class="input-group mb-3">
					
					{!! Form::label('role', '<i class="fas fa-user-shield"></i>', [ 'class' => 'input-group-text'], false) !!}
					{!! Form::select('role', \App\Models\User::roleTypes() , null , ['class' => 'form-select']) !!}
				</div>

				{!! Form::label('status', __("Estado del usuario")) !!}
				<div class="input-group mb-3">
					@can('haveaccess', 'user.edit')
					{!! Form::label('status', '<i class="fas fa-user-clock"></i>', [ 'class' => 'input-group-text' ], false) !!}
					{!! Form::select('status',\App\Models\User::statusTypes(), null, ["class" => "form-select"]) !!}
					@endcan
				</div>    
			</div>
		</div>
	</div>
</div>
