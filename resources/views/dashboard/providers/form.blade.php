

@php 
	$readonly = true; 
	$disabled = 'disabled'; 
@endphp

@can('haveaccess', 'user.edit')
	@php 
		$readonly = false; 
		$disabled = '' 
	@endphp
@endcan

@can('haveaccess', 'user.update')
	@php 
		$readonly = false; 	
		$disabled = '';
	@endphp
@endcan




@include('partials.form_errors')

<div class="container-fluid">
	{!! Form::model($user, $options) !!}
		@isset($update)
			@method("PUT")
		@endisset
		@csrf
		<div class="row page_user">
			<div class="col-xl-3">
				<div class="panel shadow">
					<div class="header d-flex justify-content-between">
						<h2 class="title">
							{!! $title !!}
						</h2>
						@if($view == 'put')
							@can('haveaccess','user.destroy')
								@if ($user->status == "100")
									<a href="#" data-route="{{ route('admin.dashboard.users.destroy',  ["user" => $user])}}" class="btn btn-success locked-record">
										<i class="fas fa-user-check"></i> {{ __('Activar') }}
									</a>
								@else
									<a href="#" data-route="{{ route('admin.dashboard.users.destroy',  ["user" => $user])}}" class="btn btn-danger locked-record">
										<i class="fas fa-user-times"></i> {{ __('Bloquear') }}
									</a>
								@endif
							@endcan
						@endif
					</div>
					<div class="inside">
						<div class="mini_profile">
							
							<img src="{{ url('/storage/web/default-avatar.png') }}" class="avatar">  
						
							<div class="info">
								
									@if($view == "post")
										@include('partials.dashboard.users.form')
									@else
										@include('partials.dashboard.users.info')
									@endif
								
									
								
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-5">
				
				@if($view == "post" || $view == "put")
					
						@include('partials.dashboard.users.roles', [ 'default' => 'PROVIDER'])
 
				@else
				<div class="panel shadow">
					<div class="header">
						<h2 class="title">
							<i class="fas fa-shopping-basket"></i>  {{ __('Pedidos')}}
						</h2>
					</div>
					<div class="inside">
						@include('partials.dashboard.users.orders')
					</div>
				</div>
				
			</div>
			<div class="col-xl-4">
				
				<div class="panel shadow">
					<div class="header">
						<h2 class="title">
							<i class="far fa-address-book"></i>  {{ __('Direcciones')}}
						</h2>
					</div>
					<div class="inside">
						@include('partials.dashboard.users.directions')
					</div>
				</div>
				@endif
			</div>
			
		</div>
		@if($view == "post" || $view == "put")
			<div class="row mtop16">
				<div class="col-md-12">
					<div class="panel shadow">
						<div class="inside">
							{!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2']); !!}
						</div>
					</div>
				</div>
			</div>
		@endif
	{!! Form::close() !!}	
</div>
