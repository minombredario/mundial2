<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraDataCardToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('card_exp_month')->nullable()->after('card_last_four');
            $table->string('card_exp_year')->nullable()->after('card_exp_month');
            $table->string('cvc')->nullable()->after('card_exp_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('card_exp_month');
            $table->dropColumn('card_exp_year');
            $table->dropColumn('cvc');
        });
    }
}
