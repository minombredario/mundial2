@extends("layouts.dashboard")

@section('title', __("Role") )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles') }}">
        <i class="fas fa-user-shield"></i> {{ __('Roles') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles.create') }}">
        <i class="fas fa-user-shield"></i> {{ __("Crear Role") }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.roles.form")
@endsection