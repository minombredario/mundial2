<?php
return [
'currency' => 'EUR',
'iva' => '21',
'profits_to_seller' => '5',
'profits_to_provider' => '25',
'shipping_cost' => '6',
'shipping_cost_international' => '30',
'text_more_sellers' => 'Ha seleccionado productos de más de un vendedor',
'meta_keywords' => 'souvenir, decoracion, decoration, gift',
'meta_description' => 'Venta de productos exclusivos de decoración',
'maintenance_mode' => '0',
]
?>
