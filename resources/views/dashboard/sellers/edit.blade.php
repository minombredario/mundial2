@extends("layouts.dashboard")

@section('title', __("Distribuidor " . $user->name) )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.sellers') }}">
        <i class="fas fa-users"></i> {{ __('Distribuidores') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.sellers.edit', $user) }}">
        <i class="fas fa-user-edit"></i> {{ $user->name }} 
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.sellers.form")
@endsection