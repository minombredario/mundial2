@extends("layouts.dashboard")
@section('title', __('Distribuidores') )
<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.sellers') }}">
        <i class="fas fa-users"></i> {{ __('Distribuidores') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.sellers.show', $user) }}">
        <i class="fas fa-id-card"></i> {{ $user->name }} 
    </a>
</li>
@endsection

@section("content")
	
	@include('dashboard.sellers.form')
	
@endsection