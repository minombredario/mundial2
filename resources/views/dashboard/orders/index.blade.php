@extends("layouts.dashboard")

@section('title', __('Pedidos') )

@section("content")
    @include("dashboard.orders.list")
@endsection