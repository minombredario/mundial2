<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\NifNie; 
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case "POST": {
                return [
                    'name' => 'required|min:5',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|min:8',
                    'password_confirmation' => 'required|min:8|same:password',
					'role' => Rule::in(User::ADMIN, User::SELLER, User::CUSTOMER, User::PROVIDER),
					'status' => 'required|max:3',
                    'avatar' => 'required|sometimes|image|mimes:jpg,jpeg,png',
                    'nif' => new NifNie		
				];
            }
            case "PUT": {
                return [
                    'name' => request('name') != null ? 'required|min:5' : '',
                    'email' => request('email') != null ? 'required|email|unique:users,email,' . $this->route('user')->id : '',
                    'password' => request('password') != null ? 'sometimes|required|min:8': '',
                    'password_confirmation' => request('password_confirmation') != null ? 'sometimes|required|min:8|same:password': '',
					'role' => Rule::in(User::ADMIN, User::SELLER, User::CUSTOMER, User::PROVIDER),
					'status' => 'required|max:3',
                    'avatar' => 'required|sometimes|image|mimes:jpg,jpeg,png',
                    'nif' => new NifNie
                ];
            }
            default: {
                return [];
            }
        }
    }
}
