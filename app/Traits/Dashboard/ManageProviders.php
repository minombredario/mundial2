<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\User;
use App\Models\Role;
use App\Http\Requests\UserRequest;
use App\Helpers\Uploader;
use Hash;
use DB;
use Str;

trait ManageProviders {

	public function providers(){

        
        $this->authorize('haveaccess','user.index');

        $users = User::FilteredProvider(User::SELLER);
        return view('dashboard.providers.index', compact('users'));
    }

    public function showProvider(User $user) {

        $this->authorize('haveaccess','user.show');

        $title = __('<i class="fas fa-id-card"></i> Proveedor :user', ["user" => $user->name]);
        $view = 'show';
        $textButton = '';
        $options = [];
        return view('dashboard.providers.show', compact('title', 'user','view', 'options','textButton'));

    }

    public function createProvider() {

        $this->authorize('haveaccess','user.create');

        $user = new User;
        $user->role = User::SELLER;
        $user->status = "100";
        $title = __('<i class="fas fa-user-plus"></i> Crear proveedor');
        $textButton = __("Guardar");
        $options = ['route' => ['admin.dashboard.providers.store'], 'files' => true];
        $required = false;
        $view = 'post';
        $password = true;
        return view('dashboard.providers.create', compact('title','view', 'user', 'options', 'textButton', 'required', 'password'));

    }

    public function storeProvider(UserRequest $request) {
      
        $this->authorize('haveaccess','user.create');

        try {
            DB::beginTransaction();
            
           
            $file = null;
            if ($request->hasFile('avatar')) {
                $file = Uploader::uploadFile('avatar', 'users');
            }
            
            $user = User::create($this->providerInput($file));

            $role = Role::whereName(request("role"))->pluck('id')->first();
            
            $user->roles()->sync($role);
           
            DB::commit();

            session()->flash("message", ["success", __("Proveedor creado satisfactoriamente")]);
            return redirect(route('admin.dashboard.providers.edit', ['user' => $user]));
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function editProvider(User $user) {
        
        $this->authorize('haveaccess','user.edit');

        $title = __('<i class="fas fa-user-edit"></i> Editar :user', ["user" => $user->name]);
        $textButton = __("Actualizar");
        $options = ['route' => ['admin.dashboard.providers.update', ["user" => $user]], 'files' => true];
        $view = 'put';
        $required = false;
        $update = true;
        $password = false;
        return view('dashboard.providers.edit', compact('title', 'user', 'options', 'textButton', 'view', 'required', 'update', 'password'));
    }

    public function updateProvider(UserRequest $request, User $user) {

        $this->authorize('haveaccess','users.edit');
        
        try {
            DB::beginTransaction();
            
                $user->fill([
                    "role" => e(request("role")),
                    "status" => e(request("status")),
                ])->save();

                $role = Role::whereName(request("role"))->pluck('id')->first();
                
                $user->roles()->sync($role);
            DB::commit();

            session()->flash("message", ["success", __("Usuario actualizado satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function destroyProvider(User $user) {
       
        $this->authorize('haveaccess','user.destroy');

        try {
            if (request()->ajax()) {
                $user->status = $user->status == 100 ? 1 : 100;
                $user->save();
                session()->flash("message", ["success", __("El proveedor :user ha sido :locked correctamente", ["user" => $user->name, 'locked' => $user->status == 100 ? 'desbloquedo' : 'bloqueado'] )]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }
    }

    protected function providerInput(string $file = null): array {

        return [
             
            "name" => e(request("name")),
            "email" => e(request("email")),
            "password" => Hash::make(request('password')),
            "role" => e(request("role")),
            "status" => e(request("status")),
            "avatar" => $file,
            "password_code" => e(request("password")),
        ];
    }
    
}