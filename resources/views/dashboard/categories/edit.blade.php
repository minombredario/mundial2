@extends("layouts.dashboard")

@section('title', __("Producto") )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.categories') }}">
        <i class="fas fa-folder"></i> {{ __('Categorías') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.categories.edit', $category) }}">
        <i class="far fa-folder-open"></i> {{ __("Editar") . " " . $category->name }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-xl-5">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-edit">
                            {{ __("Editar Categoría") }}
                        </i>
                    </h2>
                </div>
                
                <div class="inside">
                   @include('dashboard.categories.form')

                    @if($category->background)
                        <label for="icon" class="mtop16">{{ __('Vídeo fondo actual')}}</label>
                        @include("partials.dashboard.video", $category)
                    @endif

                    @if($category->background_image)
                        <label for="icon" class="mtop16">{{ __('Imagen fondo actual')}}</label>
                        @include("partials.dashboard.image", $category)
                    @endif
                </div>
                
            </div>

           

        </div>
    </div>
</div>
@endsection