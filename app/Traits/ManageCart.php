<?php
namespace App\Traits;

use App\Models\Product;
use App\Services\Cart;
use Auth;

trait ManageCart {
    public function showCart() {
       
        if(Auth::user()){
            $user = Auth::user();
            $user->load('defaultDirection');
        }
       
        return view("web.cart");
    }

    public function addProductToCart(Product $product) {
        $cart = new Cart;
        $cart->addProduct($product, request('qty'));
        session()->flash("message", ["success", __("Producto añadido al carrito correctamente")]);
        return back();
        //return redirect(route('cart'));
    }

    public function removeProductFromCart(Product $product) {
        $cart = new Cart;
        $cart->removeProduct($product->id);
        session()->flash("message", ["success", __("Producto eliminado del carrito correctamente")]);
        return back();
    }

    public function changeQuantityProduct(Product $product) {
        $cart = new Cart;
        $cart->changeQuantityProduct($product, request('qty'));
        return redirect(route('cart'));
    }

}
