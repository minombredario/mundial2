
<?php $readonly = true; $disabled = 'disabled' ?>

@can('haveaccess', 'user.edit')
	<?php $readonly = false; $disabled = ''?>
@endcan

@can('haveaccess', 'user.update')
	<?php $readonly = false; $disabled = ''?>
@endcan

@include('partials.form_errors')

<div class="container-fluid">
    <div class="page_user">
		{!! Form::model($user, $options) !!}
		@isset($update)
			@method("PUT")
		@endisset
		@csrf

		<div class="row">
			@foreach( $permissions as $key => $value )
				<div class="col-md-4 d-flex mb-4">
					<div class="panel shadow">
						<div class="header">
							<h2 class="title">
								{!! $value['icon'] !!} {{ $value['title'] }} 
							</h2>
						</div>
                            <div class="inside">
                                @foreach($value['keys'] as $k => $v)
                                    @can('haveaccess',  $v->slug )
                                        <input class="form-check-input" type="checkbox" id="permission_{{$v->id}}" value="{{$v->id}}" name="permission[]"
                                        @if( in_array( $v->id, $user->permissions->pluck('id')->toArray() ) || in_array( $v->id, $permissions_role->permissions->pluck('id')->toArray() ) )
                                            checked
                                        @endif
                                        {{ $disabled }}
                                        >

                                        <label class="form-check-label" for="permission_{{$v->id}}" style="inline: break-word;">
                                            {!! __('<em>:description</em>', ['description' => $v->description]) !!}
                                        </label>	
                                        <br>
                                    @endcan			
							    @endforeach
						    </div>
					</div>
				</div>
			@endforeach
		</div>
		<div class="row mtop16">
			<div class="col-md-12">
				<div class="panel shadow">
					<div class="inside">
						{!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2']); !!}
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>