@component('mail::message')
# Hola {{ $seller->name }},
<br>
El cliente <b>{{ $customer->name }}</b> ha comprado tu producto <b>{{ $product->name }}</b> por el importe <b>{{ \App\Helpers\Currency::formatCurrency($product->price) }}</b>.
<br><br>
¡Felicidades!

@component('mail::button', [
    'url' => env('APP_URL')
])
    Volver a la plataforma
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent
