<thead>
    <tr class="@if(count($user->directions) == 0) invisible @endif">
        <th scope="col">{{ __('Dirección') }}</th>
        <th scope="col">{{ __('C.P.') }}</th>
        <th scope="col">{{ __('Poblacion') }}</th>
        <th scope="col">{{ __('Provincia') }}</th>
        <th scope="col">{{ __('País') }}</th>
        <th scope="col">{{ __('Por defecto') }}</th>
        <th scope="col">{{ __('') }}</th>
    </tr>
</thead>