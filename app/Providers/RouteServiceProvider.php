<?php

namespace App\Providers;


use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\Role;
use App\Models\Content;
use App\Models\Direction;
use App\Models\Permission;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('user', function ($value, $route) {
            return $this->getModel(User::class, $value);
        });

        Route::bind('role', function ($value, $route) {
            return $this->getModel(Role::class, $value);
        });

        Route::bind('product', function ($value, $route) {
            return $this->getModel(Product::class, $value);
        });

        Route::bind('category', function ($value, $route) {
            return $this->getModel(Category::class, $value);
        });

        Route::bind('order', function ($value, $route) {
            return $this->getModel(Order::class, $value);
        });

        Route::bind('content', function ($value, $route) {
            return $this->getModel(Content::class, $value);
        });

        Route::bind('permission', function ($value, $route) {
            return $this->getModel(Content::class, $value);
        });

        Route::bind('direction', function ($value, $route) {
            return $this->getModel(Content::class, $value);
        });
    }

    protected function getModel($model, $routeKey) {
        $id = \Hashids::connection($model)->decode($routeKey)[0] ?? null;
        $modelInstance = resolve($model);
        return $modelInstance->findOrFail($id);
    }
    
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapDashboardRoutes();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    protected function mapDashboardRoutes()
    {
        Route::middleware(['web', 'auth', 'verified','dashboard'])
            ->namespace($this->namespace)
            ->group(base_path('routes/dashboard.php'));
    }


}
