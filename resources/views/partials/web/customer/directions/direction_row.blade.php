<tr>
    <td class="pt-3-half" contenteditable="true">{{ $direction->line_1 }}</td>
    <td class="pt-3-half" contenteditable="true">{{ $direction->postal_code }}</td>
    <td class="pt-3-half" contenteditable="true">{{ $direction->city }}</td>
    <td class="pt-3-half" contenteditable="true">{{ $direction->state }}</td>
    <td class="pt-3-half" contenteditable="true">{{ $direction->country }}</td>
    <td class="pt-3-half" contenteditable="true">{{ $direction->default }}</td>
    <td>
        <button class="btn btn-dark btn-sm" type="button"><i class="fas fa-edit"></i></button>
        <span class="table-remove"><button class="btn btn-dark btn-sm" type="button"><i class="fas fa-trash"></i></button></span>
        <button class="btn btn-dark btn-sm accordion-toggle" data-toggle="collapse" type="button" data-target="{{ '#direction' . $direction->id}}"><i class="fas fa-eye"></i></button>
    </td>
</tr>
<tr>
    <td colspan="12" class="hiddenRow">
        <div class="accordian-body collapse" id="{{ 'direction' . $direction->id }}"> 
            <table class="table">
                <tbody>
                    <tr>
                        <th scope="col">{{ __('Teléfono') }}</th>
                        <td>{{ $direction->phone }}</>
                    </tr>
                    <tr>
                        <th scope="col">{{ __('Comentarios') }}</th>
                        <td>{{ $direction->comments }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </td>
</tr>


    