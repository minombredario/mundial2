<!-- order section -->
@section('title', 'Pedidos')

<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.orders') }}">
        <i class="fas fa-boxes"></i> {{ __('Pedidos') }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@push('css')
    <link rel="stylesheet" href="{{ asset("css/jConfirm.css") }}" />
@endpush

<!-- order section -->
<div class="container-fluid">
    <div class="panel shadow">
        <div class="header">
            <h2 class="title">
                <i class="fas fa-boxes"></i> {{ __('Pedidos') }}
			</h2>
		<ul>
			<li>
				<a href="#" class="btn-search">
					<i class="fas fa-search"></i> {{ __('Buscar') }}
				</a>
			</li>
		</ul>
		
		<div class="inside">
			<div class="form-search d-none my-4">
				{!! Form::model([], ['route' => ['admin.dashboard.orders'], 'files' => true]) !!}
                        @csrf
				
                <div class="row">
					
					<div class="col-md-4">
						{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('Busqueda') ]) !!}
					</div>
					@admin
					<div class="col-md-4">
						{!! Form::select('filter',  App\Models\Order::filterOrder() , 0, ['class' => 'form-select']) !!}
					</div>
					@endadmin
					<div class="col-md-2">
						{!! Form::select('status', \App\Models\Order::statusTypes(), 0, ['class' => 'form-select']) !!}
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-dark w-100"> {{ __('Buscar') }}</button>
					</div>
				</div>

            	{!! Form::close() !!}
				
			</div>
			<table class="table table-striped mtop16">
				<thead>
					<tr>
						<td class="align-middle">{{ __('Factura') }}</td>
						<td class="align-middle">{{ __('Cliente') }}</td>
						@admin
							<td class="align-middle">{{ __('Vendedor') }}</td>
						@endadmin
                        <td class="align-middle">{{ __('Total') }}</td>
                        <td class="align-middle">{{ __('Estado') }}</td>
						<td class="align-middle">{{ __('Artículos') }}</td>
						<td class="align-middle">{{ __('Fecha') }}</td>
                        <td colspan="3"></td>
					</tr>
				  </thead>
				<tbody>
                    <!-- order -->
					@forelse ($orders as $order)
					<tr>
						<td class="align-middle" >{{ $order->invoice_id }}</td>
						<td class="align-middle" >{{ $order->user->name }}</td>
                        @admin
                        
							<td class="align-middle">
                                @foreach($order->order_lines as $line)
                                    {{ $line->product->seller->name }}
                                @endforeach
                            </td>
						@endadmin
                        
						<td class="align-middle"> 
							
								@php
									$total = 0;
								@endphp
								@foreach($order->order_lines as $line)
									@php
									//$total += auth()->user()->role == \App\Models\User::ADMIN ? $line->price * $line->quantity : $line->product->user_id == auth()->id() ?? $line->price * $line->quantity;//sumanos los valores si el artículo es del usuario activo, ahora solo falta mostrar dicho valor
										$total += $line->product->user_id == auth()->id() ? $line->price * $line->quantity : 0;
									@endphp
								@endforeach
								{{ $total }}
							
                           
                        </td>
						<td class="align-middle">{{ $order->formatted_status }}</td>
                        <td class="align-middle">
                            @php
                                $suma = 0;
                            @endphp
							@foreach ($order->order_lines as $line)
							 	@php
                                    $suma += auth()->user()->role == \App\Models\User::ADMIN ? $line->quantity : $line->product->user_id == auth()->id() ?? $line->quantity;//sumanos los valores si el artículo es del usuario activo, ahora solo falta mostrar dicho valor
                                @endphp             
                            @endforeach
                            {{$suma}}
                        </td> 
                        <td class="align-middle">{{  Carbon\Carbon::parse($order->created_at)->format('d-m-Y H:m') }}</td>                 
						<td> 
							<div class="opts">
								@can('haveaccess','order.show')
									<a href="{{ route('admin.dashboard.orders.show',$order)}}" data-toggle="tooltip" data-placemente="top" title="{{ __("Ver") }}">
										<i class="fas fa-eye" title="{{ __("Ver") }}"></i>
									</a>
									@admin
                                    <a href="{{ route("admin.dashboard.orders.download_invoice", ["order" => $order]) }}" data-toggle="tooltip" data-placemente="top" title="{{ __("Descargar factura") }}">
                                        <i class="fas fa-file-download" title="{{ __("Descargar factura") }}"></i>
									</a>
									@endadmin
								@endcan
							</div>
						</td>  
					</tr> 
					@empty
					<tr>
						<div class="empty-results">
							{!! __("Ningún resultado") !!}
						</div>
					</tr>
					@endforelse
				
				</tbody>
			</table>
			<div class="d-flex justify-content-center">
				@if(count($orders))
					{{ $orders->links() }}
				@endif
			</div>
		</div>
	</div>
</div>

<!-- order section end -->
