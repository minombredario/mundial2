<?php
namespace App\Traits\Customer;

trait ManageProducts {
    public function products() {
        $products = auth()->user()->purchasedProducts();
        return view('customer.products.index', compact('products'));
    }
}
