@push('css')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: calc( 100vh/2 );
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
@endpush
<div class="row footer">
@php 
			$dir =  Auth::user() ? \App\Models\Direction::myDirections() : [];
		@endphp
	<div class="col-12 mt-5">
		
		<div class="row">
			<div class="col-md-3 offset-md-3 ofsset-sm-1 pb-5">
				
				@if(!$payment)
					@php 
						$direction =  Auth::user() ? Auth::user()->defaultDirection : [];
					@endphp

					@if(count($direction))
						{{ $direction[0]->line_1 }}
					@else
						
					
					
						<div class="table">
							<table class="table table-sm">
								<thead>
									@if(count($dir))
										<h6>{{ __('Selecciona la direccion de envío') }}</h6>
									@endif
								</thead>
								<tbody>
									<tr>
										@forelse ( \App\Models\Direction::myDirections() as $d )
											<div class="form-check">
												<input class="form-check-input shipping-direction" type="radio" name="directions" value="{{ $d->id }}" id="{{ 'direction-' . $d->id }}" 
												data-route="{{ route('customer.directions.shipping',  ["id" => $d])}}">
												<label class="form-check-label" for="{{ 'direction-' . $d->id }}">
													{{ $d->line_1 }} , {{$d->country}} ({{ $d->postal_code }}), {{ $d->city }} {{ $d->state}}
												</label>
											</div>
										@empty	
											<a class="btn btn-dark" href="{{ route('customer.profile', ['user' => Auth::user() ]) }}"> {{ __('Crear dirección')}} </a>
										@endforelse
									</tr>
								</tbody>
							</table>

						</div>
					@endif
				@else
					<h6>{{ __('Dirección de envío') }} </h6>
					<hr>
					@if( \App\Models\Direction::shippingDirection() )
						<span>{{ \App\Models\Direction::shippingDirection()->name }}</span><br>
						<span>
							{{ \App\Models\Direction::shippingDirection()->line_1 }} <br>
							{{\App\Models\Direction::shippingDirection()->country}}
							({{ \App\Models\Direction::shippingDirection()->postal_code }}), {{ \App\Models\Direction::shippingDirection()->city }}
							{{ \App\Models\Direction::shippingDirection()->state}}
						</span>
					@else
						<h5> {{ __('Selecciona una dirección de envío en la página anterior') }} </h5>
						<a href="{{ route('cart') }}"><h6> {{ __('Volver') }} </h6></a>
					@endif
					
				@endif
			</div>
			
				
			<div class="col-md-3 pb-5">
				
				<div class="float-right">
					<h4 class="text-uppercase font-weight-bold text-left">{{ __("Total: :total", ["total" => $cart->totalAmount()]) }} </h4>
					<h4 class="text-uppercase font-weight-bold text-left">{{ __("Portes: :portes", ["portes" => \App\Helpers\Currency::formatCurrency($cart->portes() * $cart->moreSellers() ) ]) }} </h4>
					@if($cart->moreSellers() > 1)
						<h6>{{ Config('mundial_souvenir.text_more_sellers') }}</h6>
					@endif
				</div>
				
			</div>
			
			<div class="col-md-3 pb-5">
				<h2 class="text-uppercase text-right font-weight-bold"> {{ __('Total compra')}} </h2>
				<h1 class="total text-right font-weight-bolder">{{ $cart->totalAmountWithShippingCost() }}</h1>
				@auth
					@if($payment && count($dir))
						@if( \App\Models\Direction::shippingDirection())
							<form method="POST" action="{{ route('process_checkout') }}">
								@csrf
								<button type="submit"  class="btn btn-dark float-right text-uppercase">
									{{ __("Pagar") }}
								</button>
							</form>
						@endif
					@else
						@if(count($dir))	
							<a href="{{ route('checkout_form') }}" class="btn btn-dark float-right text-uppercase">{{ __("Comprar") }}</a>
						@endif
					@endif
				@endauth
				@guest
					<a href="javascript:void(0)" onclick="openNavForm()" class="btn btn-dark float-right text-uppercase">{{ __("Logueate") }}</a>
				@endguest
			</div>
		</div>
	   
	</div>


</div>

		

@push('js')
    <script>
		$.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });

		jQuery(document).ready(function () {
			$('.shipping-direction').click(function (e) {
				
				const btn = $(this);
				const route = btn.data("route");

				jQuery.ajax({
					method: "POST",
					url: route,
					success: function (data) {
						//console.log(data)
					},
					error: function (error) {
						//console.log(error)
					}
				})
    		});
			
		});

    </script>
    
@endpush