<span class="title"><i class="far fa-address-card"></i> {{ __('Nombre') }}:</span>
<span class="text">{{ $user->name }} {{ $user->lastname}}</span>

<span class="title"><i class="fas fa-user-tie"></i> {{ __('Estado del usuario') }}:</span>
<span class="text">{{ \App\Models\User::statusTypes($user->status) }}</span>

<span class="title"><i class="far fa-envelope"></i> {{ __('Correo electrónico') }}:</span>
<span class="text">{{ $user->email }}</span>

<span class="title"><i class="fas fa-phone"></i> {{ __('Teléfono') }}:</span>
<span class="text">{{ count($user->defaultDirection) ? $user->defaultDirection[0]->phone : '' }}</span>

<span class="title"><i class="far fa-calendar-alt"></i> {{ __('Fecha registro') }}:</span>
<span class="text">{{  Carbon\Carbon::parse($user->created_at)->format('d-m-Y') }}</span>
@admin
    <span class="title"><i class="fas fa-user-shield"></i> {{ __('Role de usuario') }}:</span>
    <span class="text">{{ \App\Models\User::roleTypes($user->role) }}</span>
@endadmin
				