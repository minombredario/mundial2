<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public User $user;

    public function __construct(User $user)
    {
       
        $this->user = $user;
      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->from(env('MAIL_USERNAME'))->subject(__('¡Bienvenido a MundialSouvenir!'))->view('emails.users.verification');
        return $this
            ->subject("Contraseña modificada! - " . config("app.name"))
            ->markdown('emails.users.reset_success');
    }
}
