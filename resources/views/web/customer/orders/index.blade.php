@extends("layouts.web")
@push('css')
    <style>
        .row-content{
            background: #fff;
        }
    </style>
@endpush
@section('title',  __('Mis Pedidos') )

@section('logo')
     
        <img src="{{ url('/storage/web/logoblanco.png') }}" class="img-fluid" alt="" >
      
@endsection

@section('page')
    
    <h1 class="title-page text-md-left text-sm-center"> {{ __('Mis pedidos') }}</h1>

@endsection

@section("content")
    @include("web.customer.orders.list")
@endsection
