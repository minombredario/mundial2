@extends("layouts.dashboard")

@section('title', __('Mi perfil') )

@section("content")
    @include("dashboard.profile.form")
@endsection