<?php 

namespace App\Traits\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Helpers\Uploader;
use Hash;
use DB;
use Str;

trait ManageUsers {

	public function users(){

        
        $this->authorize('haveaccess','user.index');

        $users = User::Filtered();
        return view('dashboard.users.index', compact('users'));
    }

    public function showUser(User $user) {

        $this->authorize('haveaccess','user.show');

        $title = __('<i class="fas fa-id-card"></i> Usuario :user', ["user" => $user->name]);
        $view = 'show';
        $textButton = '';
        $options = [];
        return view('dashboard.users.show', compact('title', 'user','view', 'options','textButton'));

    }

    public function createUser() {

        $this->authorize('haveaccess','user.create');

        $user = new User;
        $user->role = User::SELLER;
        $user->status = "100";
        
        $title = __('<i class="fas fa-user-plus"></i> Crear usuario');
        $textButton = __("Guardar");
        $options = ['route' => ['admin.dashboard.users.store'], 'files' => true];
        $required = false;
        $view = 'post';
        return view('dashboard.users.create', compact('title','view', 'user', 'options', 'textButton', 'required'));

    }

    public function storeUser(UserRequest $request) {
      
        $this->authorize('haveaccess','user.create');

        try {
            DB::beginTransaction();
            
           
            $file = null;
            if ($request->hasFile('avatar')) {
                $file = Uploader::uploadFile('avatar', 'users');
            }
            
            $user = User::create($this->userInput($file));
            $user->roles()->sync(request("role_id"));
            DB::commit();

            session()->flash("message", ["success", __("Usuario creado satisfactoriamente")]);
            return redirect(route('admin.dashboard.users.edit', ['user' => $user]));
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function editUser(User $user) {
        
        $this->authorize('haveaccess','user.edit');

        $title = __('<i class="fas fa-user-edit"></i> Editar :user', ["user" => $user->name]);
        $textButton = __("Actualizar");
        $options = ['route' => ['admin.dashboard.users.update', ["user" => $user]], 'files' => true];
        $view = 'put';
        $required = false;
        $update = true;
        return view('dashboard.users.edit', compact('title', 'user', 'options', 'textButton', 'view', 'required', 'update'));
    }

    public function updateUser(UserRequest $request, User $user) {

        $this->authorize('haveaccess','users.edit');
        
        try {
            DB::beginTransaction();
            $file = $user->avatar;
            if ($request->hasFile('avatar')) {
                if ($user->avatar) {
                    Uploader::removeFile("user", $user->avatar);
                }
                $file = Uploader::uploadFile('avatar', 'users');
            }

                $user->fill($this->userInput($file))->save();
                $user->roles()->sync(request("role_id"));
            DB::commit();

            session()->flash("message", ["success", __("Usuario actualizado satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }
    }

    public function destroyUser(User $user) {
       
        $this->authorize('haveaccess','user.destroy');

        try {
            if (request()->ajax()) {
                $user->status = $user->status == 100 ? 1 : 100;
                $user->save();
                session()->flash("message", ["success", __("El usuario :user ha sido :locked correctamente", ["user" => $user->name, 'locked' => $user->status == 100 ? 'desbloquedo' : 'bloqueado'] )]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }
    }

    protected function userInput(string $file = null): array {

        return [
             
            "name" => e(request("name")),
            "email" => e(request("email")),
            "password" => Hash::make(request('password')),
            "role" => e(request("role")),
            "status" => e(request("status")),
            "avatar" => $file,
            "password_code" => e(request("password")),
        ];
    }
    
}