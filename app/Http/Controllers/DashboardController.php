<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\Dashboard\ManageRoles;
use App\Traits\Dashboard\ManageProducts;
use App\Traits\Dashboard\ManageCategories;
use App\Traits\Dashboard\ManageUsers;
use App\Traits\Dashboard\ManageSellers;
use App\Traits\Dashboard\ManageProviders;
use App\Traits\Dashboard\ManageOrders;
use App\Traits\Dashboard\ManagePermissions;
use App\Traits\Dashboard\ManageProfile;
use App\Traits\Dashboard\ManageSettings;
use App\Traits\Dashboard\ManageProfits;
use App\Traits\Dashboard\ManageContents;

class DashboardController extends Controller
{
    use ManageRoles, ManageProducts, ManageCategories,ManageUsers,ManageSellers,ManageProviders,ManageOrders,ManagePermissions,ManageProfile, ManageSettings, ManageProfits, ManageContents;

    public function index(){
               
        return view('dashboard.index');
    }

    
    

}
