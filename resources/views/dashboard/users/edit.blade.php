@extends("layouts.dashboard")

@section('title', __("Usuario " . $user->name) )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.users') }}">
        <i class="fas fa-users"></i> {{ __('Usuarios') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.users.edit', $user) }}">
        <i class="fas fa-user-edit"></i> {{ $user->name }} 
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.users.form")
@endsection