<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.sellers') }}">
        <i class="fas fa-users"></i> {{ __('Distribuidores') }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@push('css')
    <link rel="stylesheet" href="/css/jConfirm.css" />
@endpush

<!-- role section -->
<div class="container-fluid">
    <div class="panel shadow">
        <div class="header">
            <h2 class="title">
                <i class="fas fa-users"></i> {{ __('Distribuidores') }}
			</h2>
			<ul>
				@can('haveaccess','user.create')
				<li>
					<a href="{{ route('admin.dashboard.sellers.create') }}">
						<i class="fas fa-plus"></i> {{ __('Crear distribuidor') }}
					</a>
				</li>
				@endif
			<li>
				<a href="#"><i class="fas fa-filter"></i> {{ __('Filtrar') }} <i class="fas fa-chevron-down"></i></a>
				<ul class="shadow">
                    <li>
						<form action="{{ route('admin.dashboard.sellers') }}" method="POST" id="form-all" >
							@csrf
							
							{!! Form::text('search','all', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-all').submit();" >
								<i class="fas fa-globe-americas"></i> {{ __('Todos') }}
							</a> 
						</form>
					</li>
					<li>
						<form action="{{ route('admin.dashboard.sellers') }}" method="POST" id="form-registered" >
							@csrf
							
							{!! Form::text('search','registered', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-registered').submit();" >
								<i class="fas fa-unlink"></i> {{ __('No verificados') }}
							</a> 
						</form>
                    </li>
                    <li>
						<form action="{{ route('admin.dashboard.sellers') }}" method="POST" id="form-verified" >
							@csrf
							
							{!! Form::text('search','verified', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-verified').submit();" >
								<i class="fas fa-user-check"></i> {{ __('Verificados') }}
							</a> 
						</form>
                    </li>
                    <li>
						<form action="{{ route('admin.dashboard.sellers') }}" method="POST" id="form-locked" >
							@csrf
							
							{!! Form::text('search','locked', ['class' => 'd-none']) !!}
							<a href = "javascript:{}" class = "nav-item nav-link" onclick = "document.getElementById('form-locked').submit();" >
								<i class="fas fa-user-lock"></i> {{ __('Bloqueados') }}
							</a> 
						</form>
					</li>
				</ul>
			</li>
			<li>
				<a href="#" class="btn-search">
					<i class="fas fa-search"></i> {{ __('Buscar') }}
				</a>
			</li>
		</ul>
		
		<div class="inside">
            <div class="form-search d-none my-4">
				{!! Form::model([], ['route' => ['admin.dashboard.sellers'], 'files' => true]) !!}
                        @csrf
				
                <div class="row">
					
					<div class="col-md-4">
						{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('Busqueda') ]) !!}
					</div>
					<div class="col-md-4">
						{!! Form::select('filter',  ['0' => __('Nombre'), '1' => __('Email'), '2' => __('Población'), '3' => __('Provincia'), '4' => __('País') ] , 0, ['class' => 'form-select']) !!}
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-dark w-100"> {{ __('Buscar') }}</button>
					</div>
				</div>

            	{!! Form::close() !!}
				
            </div>
            
            <table class="table mtop16">
                <thead>
                    <tr>
                        <td>{{ __('Nombre') }}</td>
						<td class="d-none d-xl-table-cell d-xxl-table-cell">{{ __('Dirección') }}</td>
						<td class="d-none d-xl-table-cell d-xxl-table-cell">{{ __('Población') }}</td>
						<td class="d-none d-xl-table-cell d-xxl-table-cell">{{ __('Provincia') }}</td>
						<td class="d-none d-xl-table-cell d-xxl-table-cell">{{ __('País') }}</td>
						<td>{{ __('Teléfono') }}</td>
						<td>{{ __('Email') }}</td>
							
						@admin
							<td class="d-none d-xl-table-cell d-xxl-table-cell">{{ __('Role') }}</td>
						@endadmin
                        <td class="d-none d-xl-table-cell d-xxl-table-cell">{{ __('Estado') }}</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
						<td class="align-middle">{{ $user->name }}</td>
						<td class="align-middle d-none d-xl-table-cell d-xxl-table-cell">
												@if(count($user->defaultDirection))
													{{ $user->defaultDirection[0]->line_1 }}
													<br>
													{{ $user->defaultDirection[0]->postal_code }}
												 @endif
						</td>
						<td class="align-middle d-none d-xl-table-cell d-xxl-table-cell">@if(count($user->defaultDirection)){{ $user->defaultDirection[0]->country }}@endif</td>
						<td class="align-middle d-none d-xl-table-cell d-xxl-table-cell">@if(count($user->defaultDirection)){{ $user->defaultDirection[0]->city }}@endif</td>
						<td class="align-middle d-none d-xl-table-cell d-xxl-table-cell">@if(count($user->defaultDirection)){{ $user->defaultDirection[0]->state }}@endif</td>
						<td class="align-middle">@if(count($user->defaultDirection)){{ $user->defaultDirection[0]->phone }}@endif</td>
						<td class="align-middle">{{ $user->email }}</td>
						
						@admin
							<td class="align-middle d-none d-xl-table-cell d-xxl-table-cell">{{ \App\Models\User::RoleTypes($user->role) }}</td>
						@endadmin
                        <td class="align-middle d-none d-xl-table-cell d-xxl-table-cell">{{ \App\Models\User::statusTypes($user->status) }}</td>
                        <td class="align-middle">
                            <div class="opts">
                                
                               
                                <a href="{{ route('admin.dashboard.sellers.edit', $user)}}" data-toggle="tooltip" data-placement="top" title="{{ __('Editar') }}">
                                    <i class="fas fa-edit" title="{{ __('Editar') }}"></i>
                                </a>
								
                                @admin
                                    <a href="{{ route('admin.dashboard.sellers.permissions', $user)}}" data-toggle="tooltip" data-placement="top" title="{{ __('Permisos de usuario') }}">
                                        <i class="fas fa-cogs" title="{{ __('Permisos de usuario') }}"></i>
                                    </a>
                                @endadmin

                                <a href="{{ route('admin.dashboard.sellers.show', $user)}}" data-toggle="tooltip" data-placement="top" title="{{ __('Ver') }}">
                                    <i class="fas fa-eye" title="{{ __('Ver') }}"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
			</table>
			<div class="d-flex justify-content-center">
				@if(count($users))
					{{ $users->links() }}
				@endif
			</div>
        </div>
	</div>
</div>

<!-- role section end -->
