<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::group(['prefix' => 'dashboard', 'as' => 'admin.dashboard.','middleware' => ['dashboard']], function() {
    Route::get('/', 'DashboardController@index')->name('index');

    /**
     *  PRODUCTS
     */
    Route::get('/products', 'DashboardController@products')->name('products');
    Route::post('/products', 'DashboardController@products')->name('products');

    Route::get('/products/create', 'DashboardController@createProduct')->name('products.create');
    Route::post('/products/store', 'DashboardController@storeProduct')->name('products.store');

    Route::get('/products/{product}', 'DashboardController@editProduct')->name('products.edit');
    Route::put('/products/{product}', 'DashboardController@updateProduct')->name('products.update');

    Route::delete('/products/{product}', 'DashboardController@destroyProduct')->name('products.destroy');

    /**
     *  CATEGORIES
     */
    Route::get('/categories', 'DashboardController@categories')->name('categories')->middleware("isAdmin");
    Route::post('/categories/search', 'DashboardController@searchCategory')->name('categories.search')->middleware("isAdmin");

    Route::get('/categories/create', 'DashboardController@createCategory')->name('categories.create')->middleware("isAdmin");
    Route::post('/categories/store', 'DashboardController@storeCategory')->name('categories.store')->middleware("isAdmin");

    Route::get('/categories/{category}', 'DashboardController@editCategory')->name('categories.edit')->middleware("isAdmin");
    Route::put('/categories/{category}', 'DashboardController@updateCategory')->name('categories.update')->middleware("isAdmin");

    Route::delete('/categories/{category}', 'DashboardController@destroyCategory')->name('categories.destroy')->middleware("isAdmin");

    /**
     *  ROLES
     */
    Route::get('/roles', 'DashboardController@roles')->name('roles')->middleware("isAdmin");
    Route::post('/roles', 'DashboardController@roles')->name('roles')->middleware("isAdmin");

    Route::get('/roles/create', 'DashboardController@createRole')->name('roles.create')->middleware("isAdmin");
    Route::post('/roles/store', 'DashboardController@storeRole')->name('roles.store')->middleware("isAdmin");

    Route::get('/roles/{role}', 'DashboardController@editRole')->name('roles.edit')->middleware("isAdmin");
    Route::put('/roles/{role}', 'DashboardController@updateRole')->name('roles.update')->middleware("isAdmin");

    Route::delete('/roles/{role}', 'DashboardController@destroyRole')->name('roles.destroy')->middleware("isAdmin");


    /**
     *  USERS
     */
    
    Route::get('/users', 'DashboardController@users')->name('users');
    Route::post('/users', 'DashboardController@users')->name('users');

    Route::get('/users/create', 'DashboardController@createUser')->name('users.create')->middleware("isAdmin");
    Route::post('/users/store', 'DashboardController@storeUser')->name('users.store')->middleware("isAdmin");

    Route::get('/users/{user}', 'DashboardController@editUser')->name('users.edit');
    Route::put('/users/{user}', 'DashboardController@updateUser')->name('users.update');

    Route::get('/users/view/{user}', 'DashboardController@showUser')->name('users.show');

    Route::delete('/users/{user}', 'DashboardController@destroyUser')->name('users.destroy');
	Route::get('/users/permissions/{user}', 'DashboardController@permissions')->name('users.permissions')->middleware("isAdmin");
    
    Route::put('/users/permissions/{user}', 'DashboardController@updatePermissionsUser')->name('users.permissions.update')->middleware("isAdmin");

    Route::get('/users/orders/{invoice}', 'DashboardController@showInvoice')->name('users.orders.show');

    /**
     *  SELLERS
     */

    Route::get('/sellers', 'DashboardController@sellers')->name('sellers')->middleware("isAdmin");
    Route::post('/sellers', 'DashboardController@sellers')->name('sellers')->middleware("isAdmin");

    Route::get('/sellers/create', 'DashboardController@createSeller')->name('sellers.create')->middleware("isAdmin");
    Route::post('/sellers/store', 'DashboardController@storeSeller')->name('sellers.store')->middleware("isAdmin");

    Route::get('/sellers/{user}', 'DashboardController@editSeller')->name('sellers.edit')->middleware("isAdmin");
    Route::put('/sellers/{user}', 'DashboardController@updateSeller')->name('sellers.update')->middleware("isAdmin");

    Route::get('/sellers/view/{user}', 'DashboardController@showSeller')->name('sellers.show')->middleware("isAdmin");

    Route::delete('/sellers/{user}', 'DashboardController@destroySeller')->name('sellers.destroy')->middleware("isAdmin");
	Route::get('/sellers/permissions/{user}', 'DashboardController@permissions')->name('sellers.permissions')->middleware("isAdmin");
    
    Route::put('/sellers/permissions/{user}', 'DashboardController@updatePermissionsUser')->name('sellers.permissions.update')->middleware("isAdmin");

    /**
     *  PROVIDERS
     */

    Route::get('/providers', 'DashboardController@providers')->name('providers')->middleware("isAdmin");
    Route::post('/providers', 'DashboardController@providers')->name('providers')->middleware("isAdmin");

    Route::get('/providers/create', 'DashboardController@createProvider')->name('providers.create')->middleware("isAdmin");
    Route::post('/providers/store', 'DashboardController@storeProvider')->name('providers.store')->middleware("isAdmin");

    Route::get('/providers/{user}', 'DashboardController@editProvider')->name('providers.edit')->middleware("isAdmin");
    Route::put('/providers/{user}', 'DashboardController@updateProvider')->name('providers.update')->middleware("isAdmin");

    Route::get('/providers/view/{user}', 'DashboardController@showProvider')->name('providers.show')->middleware("isAdmin");

    Route::delete('/providers/{user}', 'DashboardController@destroyProvider')->name('providers.destroy')->middleware("isAdmin");
	Route::get('/providers/permissions/{user}', 'DashboardController@permissions')->name('providers.permissions')->middleware("isAdmin");
    
    Route::put('/providers/permissions/{user}', 'DashboardController@updatePermissionsUser')->name('providers.permissions.update')->middleware("isAdmin");


    /**
     *  ADMIN PROFILE
     */

    Route::get('/profile', 'DashboardController@profile')->name('profile');
    Route::post('/profile/user/{user}', 'DashboardController@updateProfile')->name('profile.user.update');

    /**
     * CONFIG
     */

    Route::get('/settings', 'DashboardController@settings')->name('settings');
    Route::post('/settings', 'DashboardController@storeSettings')->name('settings.store');

    /**
     * ORDERS
     */
    
    Route::get('/orders', 'DashboardController@orders')->name('orders');
    Route::post('/orders', 'DashboardController@orders')->name('orders');
    Route::get('/orders/{order}', 'DashboardController@showOrder')->name('orders.show');
    Route::get('/orders/{order}/download_invoice', 'DashboardController@downloadInvoice')->name('orders.download_invoice');

    /**
     * CONTENTS
     */

    Route::get('/contents', 'DashboardController@contents')->name('contents')->middleware("isAdmin");
    
    Route::get('/contents/create', 'DashboardController@createContent')->name('contents.create')->middleware("isAdmin");
    Route::post('/contents/store', 'DashboardController@storeContent')->name('contents.store')->middleware("isAdmin");
    
    Route::get('/contents/{content}', 'DashboardController@editContent')->name('contents.edit')->middleware("isAdmin");
    Route::put('/contents/{content}', 'DashboardController@updateContent')->name('contents.update')->middleware("isAdmin");

    Route::delete('/contents/{content}', 'DashboardController@destroyContent')->name('contents.destroy')->middleware("isAdmin");

});



