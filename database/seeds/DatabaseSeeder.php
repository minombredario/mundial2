<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
       
        Storage::deleteDirectory('categories');
        Storage::makeDirectory('categories');
        //$this->call(CategorySeeder::class);

        //$this->call(UserSeeder::class);
        $this->call(PermissionInfoSeeder::class);

        Storage::deleteDirectory('products');
        Storage::makeDirectory('products');
        //$this->call(ProductSeeder::class);

        Storage::deleteDirectory('users');
        Storage::makeDirectory('users');
        //$this->call(UserSeeder::class);
    }
}
