
@push('css')
   <style>
	   #content_policy{
		   overflow: auto;
    		max-height: 600px;
	   }
    </style>
@endpush

<!-- order section -->
@section('title', 'Políticas')

<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.contents') }}">
        <i class="far fa-newspaper"></i> {{ __('Políticas') }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@push('css')
    <link rel="stylesheet" href="{{ asset("css/jConfirm.css") }}" />
@endpush

<!-- content section -->
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="far fa-newspaper"></i> {{ __('Políticas') }}
					</h2>
					<ul>
				
						<li>
							<a href="{{ route('admin.dashboard.contents.create') }}">
								<i class="fas fa-plus"></i> {{ __('Nuevo documento') }}
							</a>
						</li>
					</ul>		
				</div>
				<div class="inside">
			
					<table class="table table-striped mtop16">
						<thead>
							<tr>
								<td class="align-middle">{{ __('Título') }}</td>
								<td colspan="3"></td>
							</tr>
						</thead>
						<tbody>
							<!-- order -->
							@forelse ($contents as $content)
							<tr>
								
								<td class="align-middle">{{ $content->title }}</td>
												
								<td> 
									<div class="opts">
										
										<a href="#" data-toggle="tooltip" data-placement="top" title="{{ __("Ver") }}" onClick="showContent({{$content}})">
											<i class="fas fa-eye" title="{{ __("Ver") }}"></i>
										</a>
										
										<a href="{{ route('admin.dashboard.contents.edit', $content)}}" data-toggle="tooltip" data-placement="top" title="{{ __("Editar") }}">
											<i class="fas fa-edit" title="{{ __("Editar") }}"></i>
										</a>
								
										<a href="#" data-route="{{ route('admin.dashboard.contents.destroy',  ["content" => $content])}}" class="delete-record" data-toggle="tooltip" data-placement="top" title="{{ __("Eliminar") }}">
											<i class="fas fa-trash-alt" title="{{ __("Eliminar") }}"></i>
										</a>
						
										
									</div>
								</td>  
							</tr> 
							@empty
							<tr>
								<div class="empty-results">
									{!! __("Ningún resultado") !!}
								</div>
							</tr>
							@endforelse
						
						</tbody>
					</table>
					<div class="d-flex justify-content-center">
						@if(count($contents))
							{{ $contents->links() }}
						@endif
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="far fa-newspaper"></i> <strong id="title_policy"></strong>
					</h2>
				</div>
				<div class="inside" id="content_policy">
					
				</div>
			</div>
		</div>
		
	</div>
</div>

<!-- content section end -->

@push("js")
    <script>
		function showContent(content){
			$('#title_policy').html("  " + content['title']);
			$('#content_policy').html( decodeHtml(content['content']) );
		}

		function decodeHtml(html) {	
			var txt = document.createElement("textarea");
			txt.innerHTML = html;
			return txt.value;
		}
        
    </script>
@endpush