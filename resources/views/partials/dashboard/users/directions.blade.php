
<table class="table table-hover table-striped table-sm">
    <thead>
        <tr>
            <th scope="col">{{ __('Dirección') }}</th>
            <th scope="col">{{ __('Código postal') }}</th>
            <th scope="col">{{ __('Población') }}</th>
            <th scope="col">{{ __('Provincia') }}</th>
            <th scope="col">{{ __('País') }}</th>
            <th scope="col">{{ __('teléfono') }}</th>
            <th scope="col"></th>
        </tr>
    </thead>
    
    <tbody>
        
        @foreach( $user->directions as $direction)
           <tr>
                <td class="align-middle">{{ $direction->line_1 }}</td>
                <td class="align-middle">{{ $direction->postal_code }}</td>
                <td class="align-middle">{{ $direction->country }}</td>
                <td class="align-middle">{{ $direction->city }}</td>
                <td class="align-middle">{{ $direction->state}}</td>
                <td class="align-middle">{{ $direction->phone}}</td>
           </tr>
        @endforeach
    </tbody>
        
    
</table>
    