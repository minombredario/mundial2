<?php
return [
    'key'                   => env('REDSYS_KEY','sq7HjrUOBfKmC576ILgskD5srU870gJ7'),
    'url_notification'      => env('REDSYS_URL_NOTIFICATION',''),
    'url_ok'                => env('REDSYS_URL_OK',''),
    'url_ko'                => env('REDSYS_URL_KO',''),
    'merchantcode'          => env('REDSYS_MERCHANT_CODE','335406310'),
    'terminal'              => env('REDSYS_TERMINAL','2'),
    'enviroment'            => env('REDSYS_ENVIROMENT','test'),
    'tradename'             => env('REDSYS_TRADENAME','MI TIENDA'),
    'titular'               => env('REDSYS_TITULAR','Darío'),
    'description'           => env('REDSYS_DESCRIPTION','Compra de productos')
];
