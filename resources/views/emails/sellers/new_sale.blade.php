@component('mail::message')
# Hola {{ $seller->name }},
<br>
El usuario <b>{{ $customer->name }}</b> ha comprado <b>{{ $order_line->quantity }}</b> @if($order_line->quantity > 1) unidades @else unidad @endif de tu producto <b>{{ $product->name }}</b>.
<br><br>
¡Felicidades!

@component('mail::button', [
    'url' => env('APP_URL')
])
    Volver a la plataforma
@endcomponent

Atentamente,<br>
{{ config('app.name') }}
@endcomponent
