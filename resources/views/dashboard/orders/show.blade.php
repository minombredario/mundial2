@extends("layouts.dashboard")

@section('title', __("Pedidos") )

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.orders') }}">
        <i class="fas fa-folder"></i> {{ __('Pedidos') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.orders.show', $order) }}">
        <i class="far fa-folder-open"></i> {{ __("Factura") . ": " . $order->invoice_id }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->


@section("content")
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-eye">
                            {{ __("Factura")  . ": " . $order->invoice_id }}
                        </i>
                    </h2>
                </div>
                
                <div class="inside">
                    <div class="row">
                        <div class="col">
                             {{$order->user->name}}
                        </div>
                    </div>
                    <div class="row mtop16">
                        <div class="col">
                            {{ $direction->line_1 }}
                        </div>
                    </div>
                     <div class="row mtop16">
                        <div class="col">
                            {{$direction->country}} ({{ $direction->postal_code }})
                        </div>
                    </div>
                    <div class="row mtop16">
                        <div class="col">
                           {{ $direction->city }}, {{ $direction->state}}
                        </div>
                    </div>

                    <hr>
                    <div class="mtop16 container">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td class="align-middle">{{ __('Producto') }}</td>
                                    <td class="align-middle">{{ __('Precio') }}</td>
                                    <td class="align-middle">{{ __('Unidades') }}</td>
                                    <td class="align-middle">{{ __('Descuento') }}</td>
                                    <td class="align-middle text-right">{{ __('Total') }}</td>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- order line -->
                                @php
                                    $total = 0;
                                @endphp


                                @foreach($order->order_lines as $line)
                                    @if($line->product)
                                        <tr>
                                            <td class="align-middle" >{{ $line->product->name }}</td>
                                            <td class="align-middle">{{ \App\Helpers\Currency::formatCurrency($line->price) }}</td>
                                            <td class="align-middle">{{ $line->quantity }}</td>
                                            <td class="align-middle">{{ $line->discount }} {{ \App\Models\Product::discountIcon($line->discount_type) }}</td>
                                            <td class="align-middle text-right"> {{ \App\Helpers\Currency::formatCurrency( $line->price * $line->quantity) }}</td>                    
                                            @php
                                                $total += $line->price * $line->quantity;
                                            @endphp
                                        </tr>
                                    @endif
                                @endforeach
                            
                            </tbody>
                            @admin
                            <tfoot>
                                <tr>
                                    <td colspan="3" style="border-bottom: 0"></td>
                                    <td class="align-middle text-right" style="border-bottom: 0">{{ __('Gastos de envío') }}</td>
                                    <td class="align-middle text-right">{{ \App\Helpers\Currency::formatCurrency( $order->shipping_cost ) }}</td>                    
                                </tr> 
                                
                                <tr>
                                    <td colspan="3" style="border-bottom: 0"></td>
                                    <td class="align-middle text-right" style="border-bottom: 0">{{ __('Total') }}</td>
                                    <td class="align-middle text-right font-weight-bold ">{{ \App\Helpers\Currency::formatCurrency( (auth()->user()->role == \App\Models\User::ADMIN ? $order->total_amount : $total) + $order->shipping_cost ) }}</td>                    
                                </tr> 
                            </tfoot>
                            @endadmin
                        </table>

                    </div>
                    
                    
                </div>
                
            </div>

           

        </div>
    </div>
</div>
@endsection