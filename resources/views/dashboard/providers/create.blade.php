@extends("layouts.dashboard")

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.providers') }}">
        <i class="fas fa-users"></i> {{ __('Proveedores') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.roles.create') }}">
        <i class="fas fa-user-plus"></i> {{ __("Crear proveedor") }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.providers.form")
@endsection