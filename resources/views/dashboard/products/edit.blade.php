@extends("layouts.dashboard")

@section('title', __("Producto") )

<!-- breadcrumb section -->

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.products') }}">
        <i class="fas fa-boxes"></i> {{ __('Productos') }}
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.products.edit', $product) }}">
        <i class="fas fa-box"></i> {{ __("Editar") . " " . $product->name }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@section("content")
	@include("dashboard.products.form", ['required' => false])
@endsection