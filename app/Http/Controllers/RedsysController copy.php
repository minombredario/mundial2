<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use IlluminateHttpRequest;
use AppHttpRequests; // para recibir la notificación via request
use SsheduardoRedsysFacadesRedsys; //el controlador de redsys

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Ssheduardo\Redsys\Facades\Redsys;

class RedsysController extends Controller
{
    
    //https://carlos-herrera.com/usar-redsys-con-laravel/
    /**
     * llamar a la función desde cualquier controlador
     * $redsys= new AppHttpControllersRedsysController;
     * $redsys->index(512,50,false,'Compra de entradas');
    */

    /**
     * o en blade
     * {{AppHttpControllersRedsysController::index(512,50,false,'Compra de entradas')}}
     */
    
    /**
    * Crear formulario redsys
    *
    */

    
    public static function index($order,$amount,$display=true,$des=false){
        try{
            $order = str_pad($order,12,0,STR_PAD_LEFT); //el número de orden del pedido
            $key = config('redsys.key');
            $merchantcode = config('redsys.merchantcode');
            $terminal = config('redsys.terminal');
            $enviroment = config('redsys.enviroment');
            $urlOk = url(config('redsys.url_ok'));
            $urlKo = url(config('redsys.url_ko'));
            $urlNotification= url(config('redsys.url_notification'));
            $tradeName = config('redsys.tradename');
            $titular = config('redsys.titular');
            $description = $des?$des:config('redsys.description'); //una descripción sobre el pedido ( Si se deja en blanco se coloca por defecto la del archivo .env)
            
            Redsys::setAmount($amount); // la cantidad a cobrar
            Redsys::setOrder($order);
            Redsys::setMerchantcode($merchantcode); 
            Redsys::setCurrency('978');
            Redsys::setTransactiontype('0');
            Redsys::setTerminal($terminal);
            Redsys::setMethod('T');  //Solo pago con tarjeta, no mostramos iupay
            Redsys::setNotification(config('redsys.url_notification')); //Url de notificacion
            Redsys::setUrlOk(config('redsys.url_ok')); //Url OK
            Redsys::setUrlKo(config('redsys.url_ko')); //Url KO
            Redsys::setVersion('HMAC_SHA256_V1');
            Redsys::setTradeName($tradeName);
            Redsys::setTitular($titular);
            Redsys::setProductDescription($description);
            Redsys::setEnviroment($enviroment); //Entorno test

            $signature = Redsys::generateMerchantSignature($key);
            Redsys::setMerchantSignature($signature);
            
            if($display==false){ //si quieres que se muestre el boton o que directamente se redireccione a redsys
                Redsys::setAttributesSubmit('btn_submit', 'btn_id', 'Enviar', 'display:none');
                return Redsys::executeRedirection();
            }else{
                return Redsys::createForm();
            }

        }
        catch(Exception $e){
            echo $e->getMessage();
        }

    }
    public function formulario(){
        return view('form_redsys');
    }

    /**
    * Comprobar respuesta de Redsys
    */
    public function comprobar(Request $request)
{
try{
$key = config('redsys.key');
$parameters = Redsys::getMerchantParameters($request->input('Ds_MerchantParameters'));
$DsResponse = $parameters["Ds_Response"];
$DsResponse += 0;
if (Redsys::check($key, $request->input()) && $DsResponse <=99) {
// lo que quieras que haya si es positiva la confirmación de redsys
} else {
//lo que quieras que haga si no es positivo
}
} catch (SermepaTpvTpvException $e) {
echo $e->getMessage();
}
}

}
