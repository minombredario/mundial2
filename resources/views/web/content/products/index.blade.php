@extends("layouts.web")


@section("content")
    @include("web.content.products.list")
@endsection


@push('js')
    
    <script>
        $(document).ready(function(){
            // Target your element
            $('#content').colourBrightness();
            if($('#content').hasClass("light")){
                $('.logo').attr('src', "{{ url('/storage/web/logonegro.png') }}" );
            }

            if($('#content').hasClass("dark")){
                $('.logo').attr('src', "{{ url('/storage/web/logoblanco.png') }}" );
            }
            function hover(element) {
                element.setAttribute('src', '/storage/web/carrito-n.svg');
            }
        });
    </script>
    
@endpush