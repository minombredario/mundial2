 {!! Form::model($user, $optionsPassword) !!}
    @method("PUT")
   
    <div class="row">
        <div class="col-md-4">
            <label for="apassword">{{ __('Contraseña actual') }}:</label>
            <div class="input-group">
                <span class="input-group-text">
                    <i class="far fa-keyboard"></i>
                </span>
                {!! Form::password('apassword', ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
    
        <div class="col-md-4">
            <label for="password">{{ __('Nueva contraseña') }}:</label>
            <div class="input-group">
                <span class="input-group-text">
                    <i class="far fa-keyboard"></i>
                </span>
                {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        
        <div class="col-md-4">
            <label for="cpassword">{{ __('Confirmar contraseña') }}:</label>
            <div class="input-group">
                <span class="input-group-text">
                    <i class="far fa-keyboard"></i>
                </span>
                {!! Form::password('cpassword', ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
    </div>

    <div class="d-inline-block">
        {!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2']); !!}
    </div>
                
            
{!! Form::close() !!}