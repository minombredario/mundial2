<?php

namespace App\Models;

use App\Helpers\Currency;
use App\Traits\Hashidable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $invoice_id Factura generada por Stripe
 * @property float $total_amount Coste total del pedido
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $formatted_status
 * @property-read mixed $formatted_total_amount
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderLine[] $order_lines
 * @property-read int|null $order_lines_count
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Query\Builder|Order onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Order withoutTrashed()
 * @mixin \Eloquent
 */
class Order extends Model
{
    
    use Hashidable, SoftDeletes;

    protected $guarded = ["id"];

    const SUCCESS = "SUCCESS";
    const PENDING = "PENDING";

    protected $appends = [
        "formatted_total_amount",
        "formatted_status"        
    ];

    public function order_lines() {
        return $this->hasMany(OrderLine::class);
    }

    public function direction(){
        return $this->belongsTo(Direction::Class);
    }
    
    public function user(){
        return $this->belongsTo(User::Class);
    }

    public function shippingCost(){
        return Config('mundial_souvenir.shipping_cost');
    }

    public function getFormattedTotalAmountAttribute() {
        if ($this->total_amount) {
            return Currency::formatCurrency($this->total_amount + $this->shipping_cost, false);
        }
        return Currency::formatCurrency(0);
    }

    public function getFormattedStatusAttribute() {
        return $this->status === self::SUCCESS ? __("Procesado") : __("Pendiente");
    }

    public function scopeFiltered(Builder $builder) {
        
        if(auth()->user()->role !== User::ADMIN){
            $customerID = auth()->id();
            $builder->with('order_lines.product.seller','user')
            ->whereHas('order_lines.product.seller', function($query) use ($customerID) {
                $query->whereUserId($customerID);
            });
        }else{
            $builder->with('order_lines.product.seller','user');
        };


        if(request()->has('name')){
            $search = e(request('name'));
            $table = 'user';
            $field = 'name';
            $status = request('status') == 'ALL' ? null : request('status');

            switch (request('filter')) {
                case '1':
                    $field = 'status';
                    $table = 'user';
                    break;
                case '2':
                    $field = 'name';
                    $table = 'order_lines.product.seller';
                    break;
                default:
                    $field = 'name';
                    $table = 'user';
                    break;
            }

            $builder->whereHas($table, function($query) use ($search, $field) {
                $query->where($field, 'LIKE', '%' .  $search . '%');
            });

            if(request('status') != 'ALL'){
                $builder->where('status',$status);
            }

            //$builder->where($field, 'LIKE', '%' . e(request('name')) . '%');
        }

        return $builder
                ->orderBy('created_at', 'DESC')
                ->paginate(9);
    }

    public static function filterOrder(){
        $list = [
            '0' => __('Cliente'),
            '1' => __('Estado')
        ];
        if(auth()->user()->isAdmin()){
            $list[2] = __('Vendedor');
        }
        return $list;
    }

    public static function statusTypes($status = null) {
    
        switch ($status) {
            case self::SUCCESS:
                return  __("Procesado");
                break;
            case self::PENDING:
                return  __("Pendiente");
                break;
            default:
                return [
                    'ALL' => __("Todos"),
                    self::SUCCESS => __("Procesado"),
                    self::PENDING => __("Pendiente"),
                    
                ];
                break;
            
        }
    
    }

  

}
