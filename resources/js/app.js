
require('./bootstrap');
require('./colourBrightness');


window.he = require('he'); //convertir a string
window.Vue = require('vue');

import "./plugins/vee-validate";
import "./plugins/vue-snotify";
import "./plugins/vue-wysiwyg";

import 'owl.carousel2/dist/assets/owl.carousel.min.css';
import 'owl.carousel2/dist/assets/owl.theme.default.css';
import 'owl.carousel2/dist/owl.carousel.js';

import 'jconfirm/jConfirm.min.js';
import 'jconfirm/jConfirm.min.css';
import '@fortawesome/fontawesome-free/js/all.js';

import 'jquery-validation/dist/jquery.validate';
import 'jquery-validation/dist/additional-methods';



import owlCarousel from 'owl.carousel2';

Vue.prototype.$webBus = new Vue;

/* FILTROS */
Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('es-ES', {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2
    });
    return formatter.format(value);
});


Vue.filter('capitalize', value => {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
});

Vue.filter('lowercase', value => {
    if (!value) return ''
    value = value.toString()
    return value.toLowerCase()
});

Vue.filter('nif', value => {
    const letras = "abcdefghyjklmnñopqrstuvwxyz";
    let nifMay = ''
    if (!value) return ''
    for (let i = 0; i < value.length; i++) {
        if (letras.indexOf(value.charAt(i), 0) != -1) {
            nifMay += value.charAt(i).toUpperCase();
        } else {
            nifMay += value.charAt(i);
        }
    }
    return nifMay;
})

/* DIRECTIVAS */
Vue.directive('focus', {
    inserted(el) {
        el.focus()
    },
})

if (process.env.MIX_ENV_MODE === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug = false;
    Vue.config.silent = true; 
}


Vue.component('directions', require('./components/directions/Directions.vue').default);
Vue.component('orders', require('./components/orders/Orders.vue').default);

const app = new Vue({
    el: '#app',
});
