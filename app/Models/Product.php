<?php

namespace App\Models;


use App\Helpers\Currency;
use App\Traits\Hashidable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $slug
 * @property string $picture
 * @property string $description
 * @property float $price
 * @property int $stock
 * @property int $in_discount
 * @property string $discount_type
 * @property int $discount
 * @property string $code
 * @property int $featured
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $customers
 * @property-read int|null $customers_count
 * @property-read mixed $formatted_price
 * @property-read User $seller
 * @method static Builder|Product filtered()
 * @method static Builder|Product filteredConSesiones()
 * @method static Builder|Product forSeller()
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|Product onlyTrashed()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCode($value)
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereDeletedAt($value)
 * @method static Builder|Product whereDescription($value)
 * @method static Builder|Product whereDiscount($value)
 * @method static Builder|Product whereDiscountType($value)
 * @method static Builder|Product whereFeatured($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereInDiscount($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product wherePicture($value)
 * @method static Builder|Product wherePrice($value)
 * @method static Builder|Product whereSlug($value)
 * @method static Builder|Product whereStatus($value)
 * @method static Builder|Product whereStock($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @method static Builder|Product whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Product withoutTrashed()
 * @mixin \Eloquent
 */
class Product extends Model
{
    
    use Hashidable, SoftDeletes;
    
    protected $fillable = [
        "user_id", "name", "slug", "picture", "description", "code",
        "price", 'profit', "stock", "order", "in_discount", "discount","discount_type", "featured", "status"
    ];

    const PUBLISHED = 1;
    const PENDING = 2;
    const REJECTED = 3;

    const PERCENT = 'PERCENT';
    const PRICE = 'PRICE';

    protected $appends = [
        "formatted_price"
    ];

    //le asignamos automaticamente el id del usuario autenticado al crear el producto
    /*protected static function boot() {
        parent::boot();
        if ( !app()->runningInConsole()) {
            self::saving(function ($table) {
                $table->user_id = auth()->id();
            });
        }
    }
    */

    protected static function boot(){
        parent::boot();

        static::retrieved(function($model){
            if($model->seller->role == User::PROVIDER){
                $model['real_price'] = $model->price;
                $model->price = $model->price + ($model->price * Config('mundial_souvenir.profits_to_provider') / 100);
            }
            
        });
    }  

    public function imagePath() {
        return sprintf('%s/%s', '/storage/products', $this->picture);
    }

    public function categories () {
        return $this->belongsToMany(Category::class);
    }

    public function customers() {
        return $this->belongsToMany(User::class, "product_customer");
    }

    public function seller() {
        return $this->belongsTo(User::class, "user_id");
    }

    public function getFormattedPriceAttribute() {
        
        return Currency::formatCurrency($this->price);
    }

    public function priceWithDiscount() {
        $discount = $this->discount_type == self::PERCENT ? $this->price * $this->discount / 100 : $this->discount ;
        return Currency::formatCurrency($this->price - $discount);
    }

    public function scopeFiltered(Builder $builder) {
      
        if(auth()->user()->role === User::ADMIN){
            $builder->with('seller');
        }else{
            $builder->where("user_id", auth()->id());
        }
       
        if(request()->has('name')){
            switch (request('filter')) {
                case '0':
                    $field = 'name';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%')->where('status',request('status'));
                    break;
                case '1':
                    $field = 'code';
                    $builder->where($field, 'LIKE', '%' . e(request('name')) . '%')->where('status',request('status'));
                    break;
                case '2':
                   $field = 'name';
                   $builder->with('categories')
                        ->whereHas('categories', function($query){
                            $query->where('name', 'LIKE', '%' . e(request('name')) . '%');
                        });
                    break;
                default:
                    $field = 'name';
                    break;
            }
           
            
        }
       
        if(request()->has('search')){
            switch (request('search')) {
                case 'featured':
                    $builder->where('featured', 1);
                    break;
                case 'published':    
                    $builder->where("status", Product::PUBLISHED);
                    break;
                case 'pending':
                    $builder->where("status", Product::PENDING);
                    break;
                case 'rejected':
                    $builder->where("status", Product::REJECTED);
                    break;
                case 'stock':
                    $builder->where("stock", ">", "0");
                    break;
                case 'trash':
                    $builder->onlyTrashed();
                    break;
                default:
                   $builder;
                    break;
            }
        };

        return $builder->orderBy('updated_at','DESC')->paginate();
    }

    public function scopeFiltered_conSesiones(Builder $builder) {

        dd( ( session('search[admin.dashboard]') )->isNotEmpty() );
        
        if(auth()->user()->role === User::ADMIN){
            $builder->with('seller');
        }else{
            $builder->where("user_id", auth()->id());
        }

        if (session()->has('filter[admin.dashboard]')) {
            switch (session('filter[admin.dashboard]')) {
                case 'featured':
                    $builder->where('featured', 1);
                    break;
                case 'published':    
                    $builder->where("status", Product::PUBLISHED);
                    break;
                case 'pending':
                    $builder->where("status", Product::PENDING);
                    break;
                case 'rejected':
                    $builder->where("status", Product::REJECTED);
                    break;
                case 'stock':
                    $builder->where("stock", ">", "0");
                    break;
                case 'trash':
                    $builder->onlyTrashed();
                    break;
                default:
                   $builder;
                    break;
            }
    
        }elseif( ( session('search[admin.dashboard]') )->isNotEmpty() ){
            
            $title = session( 'search[admin.dashboard]' )->get('name');
            $filter = session( 'search[admin.dashboard]' )->get('filter') == 1 ? 'name' : 'code';
            $status = session( 'search[admin.dashboard]' )->get('status');
            $builder->where($filter, 'LIKE', '%' . $title . '%')->where('status', $status);
            //dd(session( 'search[admin.dashboard]' ) );
        };
        //dd($builder);
        return $builder->paginate();
    }

    public function scopeForSeller(Builder $builder) {
        if(auth()->user()->role === User::ADMIN){
            $builder->with('seller');
        }else{
            $builder->where("user_id", auth()->id());
        }
        
        return $builder->paginate();
    }


    public static function discountTypes() {
        return [
            self::PERCENT => __("Porcentaje"),
            self::PRICE => __("Fijo"),
        ];
    }

    public static function discountIcon($discount_type) {
        switch ($discount_type) {
            case self::PERCENT:
                return "%";
                break;
            case self::PRICE:
                return "€";
                break;
            default:
                return '';
                break;
        }
    }
    
    public static function statusTypes($status = null) {
    
        switch ($status) {
            case self::PUBLISHED:
                return  __("Publicado");
                break;

            case self::PENDING:
                return  __("Pendiente");
                break;

            case self::REJECTED:
                return  __("Rechazado");
                break;
            default:
                return [
                    self::PUBLISHED => __("Publicado"),
                    self::PENDING => __("Pendiente"),
                    self::REJECTED => __("Rechazado")
                ];
                break;
            
        }
    
    }
}
