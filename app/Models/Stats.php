<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use Carbon\Carbon;
use DB;
class Stats extends Model
{
    public static function usersCustomer(){
        $seller = auth()->id();
        
        $users = DB::select("SELECT `orders`.`invoice_id`, `orders`.`status`,`orders`.`created_at`, SUM(`products`.`price`) as 'total'
                    FROM `orders`, `order_lines`, `products`
                    WHERE `order_lines`.`product_id` = `products`.`id` 
                    and `products`.`user_id` = $seller
                    and `orders`.`id` = `order_lines`.`order_id`
                    group by `orders`.`user_id`");

        if(auth()->user()->role == User::ADMIN){
            return count(User::where('role', User::CUSTOMER)->get());
        }else{
            return count($users);
        }
      

      
    }

    public static function usersProviders(){
       
       return count(User::where('role', User::PROVIDER)->get());
    }

    public static function usersSellers(){
       
       return count(User::where('role', User::SELLER)->get());
    }

    public static function totalProducts(){
       
        if(auth()->user()->role == User::ADMIN){
         return count(Product::get());
    }else{
         return count(Product::where('user_id', auth()->id())->get());
    }
       
    }

    public static function orders($data = null){
        $orders =  Order::where('status', Order::SUCCESS)->whereDate('created_at', '=', date('Y-m-d'))->get();

        switch ($data) {
            case 'count':
                    return count($orders);
                break;
            case 'sum':
                    return $orders->sum('total_amount');
                break;
            default:
                # code...
                break;
        }
        
    }
}
