@extends("layouts.dashboard")

@section('title', 'Dashboard')


@section("content")

<div class="container-fluid">
	@can('haveaccess', 'dashboard.statistics.show')
	<div class="panel shadow">
		<div class="header">
			<h2 class="title">
				<i class="fas fa-chart-bar"></i> {{ __('Estadísticas rápidas') }}.
			</h2>
		</div>


		@include('partials.dashboard.stats.totales')

			@can('haveaccess', 'dashboard.sales.show') 
			<div class="row">

				@admin
				<div class="col-lg-12 col-xl-6">
					@include('partials.dashboard.stats.profits_admin')
				</div>
				@endadmin

				<div class="@if(auth()->user()->role == 'ADMIN') col-xl-6 @else col-lg-12 @endif">
					@include('partials.dashboard.stats.profits_seller')
				</div>
			</div>
			@endcan
	</div>
	@endcan
</div>
@endsection