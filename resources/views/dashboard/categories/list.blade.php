<!-- category section -->
@section('title', __("Categorías") )

<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.categories') }}">
        <i class="fas fa-folder"></i> {{ __('Categorías') }}
    </a>
</li>
@endsection

<!-- breadcrumb section end-->

@push('css')
    <link rel="stylesheet" href="{{ asset("css/jConfirm.css") }}" />
@endpush

<!-- category section -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-folder-plus"></i>
                            {{ __("Agregar Categoría") }}
                    </h2>
                </div>
                
                <div class="inside">
                   
                   @include('dashboard.categories.create')
                    
                    
                </div>
            </div>

        </div>
        <div class="col-md-9">
            <div class="panel shadow">
                <div class="header">
                    <h2 class="title">
                        <i class="fas fa-folder"></i> {{ __('Categorías') }}
                    </h2>
                </div>

                <div class="inside">
                    <nav class="nav nav-pills nav-fill">
                        @foreach(\App\Models\Module::getModules('name','id') as $key => $value)
                            <a href="#" data-route="{{ route('admin.dashboard.categories.search',  ["search" => $key])}}" class="nav-item nav-link search-record {{ session('search[admin.dashboard]') == $key ? 'active' : ''  }}">
                                {{ __($value) }} 
                            </a>
                        @endforeach
                    </nav>

                    
                    <table class="table">
                        @if(count($categories))
                            <thead>
                                <tr>
                                    <td width="150px">{{ __('Nombre') }}</td>
                                    <td>{{ __('Artículos') }}</td>
                                    <td width="140px"> {{ __('Acciones') }}</td>
                                </tr>
                            </thead>
                        @endif
                        <tbody>
                           
                            @forelse($categories as $category)
                            <tr>
                               <td>{{ $category->name }}</td>
                                <td>{{ __(":count", ['count' => $category->products_count]) }}</td>
                                 <td>
                                    <div class="opts">
                                       
                                        <div class="opts">
                                            @can('haveaccess','category.edit')
                                                <a href="{{ route('admin.dashboard.categories.edit',$category)}}" data-toggle="tooltip" data-placement="top" title="Editar">
                                                    <i class="fas fa-edit" title="Editar"></i>
                                                </a>
                                            @endcan
                                            @can('haveaccess','category.destroy')
                                                <a href="#" data-route="{{ route('admin.dashboard.categories.destroy',  ["category" => $category])}}" class="delete-record" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                                    <i class="fas fa-trash-alt" title="Eliminar"></i>
                                                </a>
                                            @endcan
                                        </div>
                                       
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <div class="empty-results my-4">
                                    {!! __("No tienes ninguna categoría todavía") !!}   
                                </div>
                            </tr>
                            @endforelse  
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                        @if(count($categories))
                            {{ $categories->links() }}
                        @endif
			        </div>
                   

                </div>
            </div>
        </div>

<!-- category section end -->
