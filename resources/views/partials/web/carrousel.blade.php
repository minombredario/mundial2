

@php
    
    $category = \App\Models\Category::getCategory($module['id']);

@endphp
@if(count($category))
    <h2 style="color:#fff">{{ $module['name'] }}</h2>
    <div class="owl-carousel owl-theme owl-loaded mundial_viewed_slider mt-1 mb-2">
       
            
        @foreach ($category as $cat )
            @if($cat->background_image)
                <a href="{{ route('web.products', [ 'module' => $module['slug'], 'cat' =>  $cat['slug'] ] )}}">
                    <div class="item">
                        
                        <img src="{{ $cat->backgroundImagePath() }}" class="img-fluid"/>
                            
                        <p class="text-uppercase text-center">{{ $cat->name == "República dominicana" ? "Rep. Dominicana" : $cat->name }}</p>
                
                    </div>
                </a>
            @endif
        @endforeach

       
       
    </div>
    
     
@endif
