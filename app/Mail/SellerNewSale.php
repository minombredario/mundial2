<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\Product;
use App\Models\OrderLine;
use App\Models\User;

class SellerNewSale extends Mailable
{
    use Queueable, SerializesModels;

    public User $seller;
    public User $customer;
    public Product $product;
    public OrderLine $order_line;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $seller, User $customer, Product $product, OrderLine $order_line)
    {
        
        $this->seller = $seller;
        $this->customer = $customer;
        $this->product = $product;
        $this->order_line = $order_line;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Producto vendido! - " . config("app.name"))
            ->markdown('emails.sellers.new_sale');
    }
}
