<!-- breadcrumb section -->
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.profile') }}">
        <i class="fas fa-id-card"></i> {{ __('Mi perfil') }}
    </a>
</li>
@endsection

<?php $readonly = true; $disabled = 'disabled' ?>

@can('haveaccess', 'userown.edit')
	<?php $readonly = false; $disabled = ''?>
@endcan

@can('haveaccess', 'userown.update')
	<?php $readonly = false; $disabled = ''?>
@endcan

@push('css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet"/>
@endpush

<div class="container-fluid">

	@include('partials.form_errors')
	
    <div class="panel shadow">
        <div class="header d-flex justify-content-between">
            <h2 class="title">
                <i class="fas fa-id-card"></i> {{ $title }}
            </h2>
            {!! Form::model($user, $options) !!}
            @isset($update)
                @method("PUT")
            @endisset

            @csrf
	   </div>
	   
        <div class="inside  px-5">
			@admin		
			<div class="row mtop16">
				<div class="col-md-6">
					<div class="video_container">
						@if($user->profile)
							<img src="{{ $user->profile->imagePath($user->profile->logo_1) }}"  class="img-thumbnail bg-secondary bg-gradient p-4"/>
						@endif
						
					</div>
					{!! Form::label('logo_1', __("Logo blanco:")) !!}
					<div class="input-group mb-3">
						{!! Form::label('logo_1', '<i class="fas fa-image"></i>', [ 'class' => 'input-group-text' ], false) !!}
						<div class="form-file ">
							{!! Form::file('logo_1', ['class' => 'form-file-input lo' . ($errors->has('logo_1') ? ' is-invalid' : ''), 'id' => 'logo_1', 'accept' => "image/*", $required ]) !!}
							<label class="form-file-label" for="logo_1">
								{!! Form::label('logo_1', __('Selecciona una imagen'), [ 'class' => 'form-file-text logo_1', 'for' => "logo_1" ], false) !!}
							</label>
						</div>

						@if ($errors->has('logo_1'))
							<div class="{{$errors->has('logo_1') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('logo_1') ? 'display:block' : ''}}">
								<strong>{{ $errors->first('logo_1') }}</strong>
							</div>
						@endif  
					</div>
				
				</div>
				<div class="col-md-6">
					<div class="video_container" >
						@if($user->profile)
							<img src="{{ $user->profile->imagePath($user->profile->logo_2) }}"  class="img-thumbnail bg-secondary bg-gradient p-4" />
						@endif
					</div>
					{!! Form::label('logo_2', __("Logo negro:")) !!}
					<div class="input-group mb-3">
						{!! Form::label('logo_2', '<i class="fas fa-image"></i>', [ 'class' => 'input-group-text' ], false) !!}
						<div class="form-file ">
							{!! Form::file('logo_2', ['class' => 'form-file-input' . ($errors->has('logo_2') ? ' is-invalid' : ''), 'id' => 'logo_2', 'accept' => "image/*", $required ]) !!}
							<label class="form-file-label" for="logo_2">
								{!! Form::label('logo_2', __('Selecciona una imagen'), [ 'class' => 'form-file-text logo_2', 'for' => "logo_2" ], false) !!}
							</label>
						</div>

						@if ($errors->has('logo_2'))
							<div class="{{$errors->has('logo_2') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('logo_2') ? 'display:block' : ''}}">
								<strong>{{ $errors->first('logo_2') }}</strong>
							</div>
						@endif  
					</div>
				
				</div>
			</div>
			
			<hr>
			@endadmin
			<div class="row mtop16">   

				<div class="col-md-4">
					{!! Form::label('name', __("Gerente")) !!}
					<div class="input-group" >
						{!! Form::label('name', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('name', null, ['class' => 'form-control' .($errors->has('name') ? ' is-invalid' : ''), 'required' ]) !!}
					</div>

					@if ($errors->has('name'))
						<div class="{{$errors->has('name') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('name') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('name') }}</strong>
						</div>
					@endif
					
				</div>
				
				<div class="col-md-4">
					{!! Form::label('company', __("Nombre de la tienda")) !!}
					<div class="input-group" >
						{!! Form::label('company', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('company', ($user->profile ?  $user->profile->company : null ), ['class' => 'form-control' .($errors->has('company') ? ' is-invalid' : ''), 'required' ]) !!}
					</div>

					@if ($errors->has('company'))
						<div class="{{$errors->has('company') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('company') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('company') }}</strong>
						</div>
					@endif
					
				</div>

				<div class="col-md-4">
					{!! Form::label('nif', __("DNI/NIE/NIF")) !!}
					<div class="input-group" >
						{!! Form::label('nif', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('nif',  ($user->profile ?  $user->profile->name : null ), ['class' => 'form-control' .($errors->has('nif') ? ' is-invalid' : ''), 'required' ]) !!}
					</div>

					@if ($errors->has('nif'))
						<div class="{{$errors->has('nif') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('nif') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('nif') }}</strong>
						</div>
					@endif
					
				</div>
			</div>
			
			<div class="row mtop16">
				
				<div class="col-md-3">
					{!! Form::label('phone_1', __("Teléfono")) !!}
					<div class="input-group" >
						{!! Form::label('phone_1', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::number('phone_1', ($user->profile ?  $user->profile->phone_1 : null ), ['class' => 'form-control' .($errors->has('phone_1') ? ' is-invalid' : ''), 'required' ]) !!}
					</div>

					@if ($errors->has('phone_1'))
						<div class="{{$errors->has('phone_1') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('phone_1') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('phone_1') }}</strong>
						</div>
					@endif
				</div>

				<div class="col-md-3">
					{!! Form::label('phone_2', __("Teléfono")) !!}
					<div class="input-group" >
						{!! Form::label('phone_2', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::number('phone_2', ($user->profile ?  $user->profile->phone_2 : null ), ['class' => 'form-control' .($errors->has('phone_2') ? ' is-invalid' : ''), 'required']) !!}
					</div>

					@if ($errors->has('phone_2'))
						<div class="{{$errors->has('phone_2') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('phone_2') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('phone_2') }}</strong>
						</div>
					@endif
				</div>

				<div class="col-md-6">
					{!! Form::label('email', __("Email")) !!}
					<div class="input-group" >
						{!! Form::label('email', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('email', $user->email, ['class' => 'form-control' .($errors->has('email') ? ' is-invalid' : ''), 'required']) !!}
					</div>

					@if ($errors->has('email'))
						<div class="{{$errors->has('email') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('email') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('email') }}</strong>
						</div>
					@endif
				</div>
			</div>

			<hr>
			
			<div class="row mtop16">
				<div class="col-md-12">
					{!! Form::label('line_1', __("Dirección")) !!}
					<div class="input-group" >
						{!! Form::label('line_1', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('line_1', ( count($user->directions) ?  $user->directions[0]->line_1 : null ) , ['class' => 'form-control' .($errors->has('line_1') ? ' is-invalid' : ''), 'required']) !!}
					</div>

					@if ($errors->has('line_1'))
						<div class="{{$errors->has('line_1') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('line_1') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('line_1') }}</strong>
						</div>
					@endif
				</div>
			</div>

			<div class="row mtop16">
				<div class="col-md-3">
					{!! Form::label('postal_code', __("Código Postal")) !!}
					<div class="input-group" >
						{!! Form::label('postal_code', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::number('postal_code', ( count($user->directions) ? $user->directions[0]->postal_code : null ), ['id' => 'postal_code', 'class' => 'form-control' .($errors->has('postal_code') ? ' is-invalid' : ''), 'required']) !!}
					</div>

					@if ($errors->has('postal_code'))
						<div class="{{$errors->has('postal_code') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('postal_code') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('postal_code') }}</strong>
						</div>
					@endif
				</div>

				<div class="col-md-3">
					{!! Form::label('country', __("Población")) !!}
					<div class="input-group" >
						{!! Form::label('country', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('country',( count($user->directions) ? $user->directions[0]->country : null ), ['id' => 'country', 'class' => 'form-control' .($errors->has('country') ? ' is-invalid' : ''), 'required']) !!}
					</div>

					@if ($errors->has('country'))
						<div class="{{$errors->has('country') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('country') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('country') }}</strong>
						</div>
					@endif
				</div>

				<div class="col-md-3">
					{!! Form::label('city', __("Provincia")) !!}
					<div class="input-group" >
						{!! Form::label('city', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('city', ( count($user->directions) ? $user->directions[0]->city : null ), ['id' => 'city', 'class' => 'form-control' .($errors->has('city') ? ' is-invalid' : ''), 'required']) !!}
					</div>

					@if ($errors->has('city'))
						<div class="{{$errors->has('city') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('city') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('city') }}</strong>
						</div>
					@endif
				</div>

				<div class="col-md-3">
					{!! Form::label('state', __("País")) !!}
					<div class="input-group" >
						{!! Form::label('state', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
						{!! Form::text('state', ( count($user->directions) ? $user->directions[0]->state : null ), ['class' => 'form-control' .($errors->has('state') ? ' is-invalid' : ''), 'required']) !!}
					</div>

					@if ($errors->has('state'))
						<div class="{{$errors->has('state') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('state') ? 'display:block' : ''}}">
							<strong>{{ $errors->first('state') }}</strong>
						</div>
					@endif
				</div>

			</div>

			<div class="row">
				@can('haveaccess', 'userown.edit')
				<div class="row mtop16">
                    <div class="col-md-12">
                        {!! Form::submit($textButton, ['class' => 'btn btn-dark mt-2']); !!}
                    </div>
                </div>
				@endcan

                
           		{!! Form::close() !!}
       
			</div>
		</div>
	</div>
	
	
</div>

@push("js")
    <script>
        jQuery(document).ready(function () {
            
            $('#logo_1').on('change', function() {
				
				var image = $('#logo_1')[0].files[0].name;
                $('.logo_1').text(image);
               
			});
			
			$('#logo_2').on('change', function() {
                var image = $('#logo_2')[0].files[0].name;
                $('.logo_2').text(image);
               
			});

			$('#postal_code').change( () => {
				$('#postal_code').val();
			});

			const municipios = {!! json_encode(\App\Helpers\Functions::municipios(), JSON_HEX_TAG) !!};
			const provincias = {!! json_encode(\App\Helpers\Functions::postalCode(), JSON_HEX_TAG) !!};

			$('#postal_code').on("change paste keyup", function() {

				if( $('#postal_code').val() != null &&  $('#postal_code').val() >= 4){
					var search = $('#postal_code').val() == 4 ? `0${$('#postal_code').val().substring(0,1)}` : $('#postal_code').val().substring(0,2);
					var municipio = municipios.find(x => x.id === $('#postal_code').val() );
					var provincia = provincias.find(x => x.id === search );
					
					if(typeof(municipio) == 'object'){
						$('#country').val( municipio['nombre']);
					}else{
						$('#country').val( null );
						
					}

					if(typeof(provincia) == 'object' && typeof(municipio) == 'object'){
						$('#city').val( provincia['nombre']);
					}else{
						$('#city').val( null );
					}


				}else{
					$('#country').val( null );
					$('#city').val( null );
				}
				
				
			});
			

        });
    </script>
@endpush