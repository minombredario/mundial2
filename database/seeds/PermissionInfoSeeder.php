<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Content;
use App\Models\Profile;
use App\Models\Direction;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PermissionInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate tables
        DB::statement("SET foreign_key_checks=0");
            DB::table('role_user')->truncate();
            DB::table('permission_role')->truncate();
            Permission::truncate();
            Role::truncate();
        DB::statement("SET foreign_key_checks=1");



        //user admin
        $useradmin= User::where('email','admin@admin.com')->first();
        if ($useradmin) {
            $useradmin->delete();
        }
        $useradmin= User::create([
            'name' => 'administrador',
            'status' => '1',
            'role' => User::ADMIN,
            'nif' => "ESB96344791",
            'email' =>'admin@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'password_code' => 'password',
            'remember_token' => Str::random(10),
        ]);

        //user seller
        $userseller= User::where('email','seller@admin.com')->first();
        if ($userseller) {
            $userseller->delete();
        }
        $userseller= User::create([
            'name' => 'Distribuidor',
            'status' => '1',
            'role' => User::SELLER,
            'nif' => "ESB96344792",
            'email' =>'seller@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'password_code' => 'password',
            'remember_token' => Str::random(10),
        ]);

        //user provider
        $userprovider= User::where('email','provider@admin.com')->first();
        if ($userprovider) {
            $userprovider->delete();
        }
        $userprovider= User::create([
            'name' => 'Proveedor',
            'status' => '1',
            'role' => User::PROVIDER,
            'nif' => "ESB96344793",
            'email' =>'provider@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'password_code' => 'password',
            'remember_token' => Str::random(10),
        ]);

        

        //user customer
        $usercustomer= User::where('email','customer@admin.com')->first();
        if ($usercustomer) {
            $usercustomer->delete();
        }
        $usercustomer= User::create([
            'name' => 'Usuario final',
            'status' => '1',
            'role' => User::CUSTOMER,
            'nif' => "ESB96344794",
            'email' =>'customer@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'password_code' => 'password',
            'remember_token' => Str::random(10),
        ]);

        

        //rol admin
        $roladmin=Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Administrador plataforma',
            'full-access' => 'yes'
    
        ]);

        //rol seller
        $rolseller=Role::create([
            'name' => 'Seller',
            'slug' => 'seller',
            'description' => 'Administrador tienda',
            'full-access' => 'no'
    
        ]);

        //rol provider
        $rolprovider=Role::create([
            'name' => 'Provider',
            'slug' => 'provider',
            'description' => 'Exportador artículos tienda',
            'full-access' => 'no'
    
        ]);


        //rol Registered User
        $rolcustomer=Role::create([
            'name' => 'Customer',
            'slug' => 'customer',
            'description' => 'Clientes de la tienda',
            'full-access' => 'no'
    
        ]);
        
        //table role_user
        $useradmin->roles()->sync([ $roladmin->id ]);
        $userseller->roles()->sync([ $rolseller->id ]);
        $userprovider->roles()->sync([ $rolprovider->id ]);
        $usercustomer->roles()->sync([ $rolcustomer->id ]);

        //admin profile
        $profileAdmin = Profile::create([
            'user_id' => $useradmin->id,
            'company' => 'Mundial Souvenir',
            'phone_1' => '646136523',
            'phone_2' => '963605056',
            'logo_1' => 'logoblanco.png',
            'logo_2' => 'logonegro.png',
        ]);

            
        //admin content
        $contentAdmin = Content::create([
            'user_id' => $useradmin->id,
            'title' => 'Condiciones de venta',
            'content' => '<strong>Lorem ipsum dolor</strong> sit amet consectetur adipisicing elit. Quod recusandae repellendus, eius necessitatibus tempora, ratione quo, mollitia odit corrupti praesentium ex aut blanditiis quia vero magni dolorum enim deserunt consectetur suscipit accusantium dignissimos molestias. Nemo dolorum quod et porro! Ipsum veniam blanditiis neque at quasi veritatis reprehenderit qui esse sit quas ex odio ullam aliquam, nobis laborum maiores perspiciatis quaerat officia error unde, vel, minima illo velit. Inventore fugit recusandae dolorem. Amet obcaecati molestias quisquam hic incidunt distinctio mollitia voluptates nobis culpa aperiam, fugiat dolore eligendi qui eum impedit harum inventore a optio molestiae eos sit! Dolor explicabo quis excepturi nisi iste hic dolore, alias quam voluptatum temporibus sapiente obcaecati voluptatem sequi velit dolorem earum incidunt maxime debitis, nostrum molestias a harum repellendus aliquam. Voluptatem porro voluptates maiores fugiat saepe adipisci voluptate voluptatibus aperiam incidunt. Veritatis voluptates saepe, error, delectus quidem esse sapiente, eius culpa nihil incidunt temporibus autem nesciunt nobis ipsum minima tempore. Dolorem nam tempora nihil aperiam officiis saepe itaque facilis asperiores id, doloribus tenetur consequuntur odio earum architecto corporis voluptas rerum. Quisquam accusamus, doloribus, aliquid vitae deserunt voluptatum enim animi corrupti maiores inventore temporibus ab amet laboriosam. Soluta optio, corrupti impedit eos totam nostrum, cupiditate excepturi tenetur enim placeat in voluptatibus explicabo porro repudiandae veniam illum autem dignissimos, alias quos sed dolores? Consequuntur quas nemo, ad sed tempora perferendis saepe excepturi omnis molestiae officiis fuga reprehenderit temporibus obcaecati in veritatis repudiandae minus, eaque beatae odit ab laudantium laborum! Cumque assumenda harum, nesciunt libero iure voluptatem doloremque laboriosam reprehenderit, modi placeat minus a provident. Quas numquam praesentium consectetur vitae dignissimos sint. Obcaecati sint quibusdam exercitationem natus ipsam. Reiciendis rem, voluptates illo fuga distinctio iure nemo sit, officia consectetur quidem et tenetur ducimus perferendis laudantium sint minima corporis molestiae obcaecati a cupiditate error alias itaque tempore ex. Quos qui nobis, optio, quisquam ex totam odit fuga consectetur unde nulla officia quod, nam modi accusantium consequatur? Hic distinctio minus consequatur perferendis alias, quidem eos ducimus magnam adipisci non! Aliquam, repudiandae! Magnam assumenda hic esse laboriosam adipisci dolores, doloribus quis, veritatis totam, dolore quas mollitia cupiditate qui enim pariatur illo autem repellendus magni. Unde ex voluptatem expedita, ratione quae dignissimos iure animi rem facere odit impedit ullam quos dolores neque nobis sequi obcaecati aut? Dicta ratione, sequi quibusdam qui dolores deleniti cumque temporibus unde mollitia ipsam quisquam eaque beatae placeat perspiciatis sint at quos totam quia explicabo laborum pariatur. Ut facere incidunt nemo blanditiis, corporis architecto cum suscipit odit ipsum dolore exercitationem. Unde molestiae nemo et quibusdam vel necessitatibus nostrum, consequuntur dolore accusamus explicabo! Deserunt, sint fugiat tenetur assumenda quasi ad ratione ab facere consequatur eaque vel accusamus rem vero reprehenderit quaerat ex consectetur earum magnam eos. Similique ratione a, obcaecati nulla nisi quibusdam dicta in repellendus tempora officiis incidunt debitis necessitatibus cum ipsam corrupti porro, veniam vitae totam doloremque maiores laboriosam. Harum, illo sed quaerat animi nihil consectetur odit sunt tenetur hic suscipit, fugiat labore quas eius! Nisi pariatur tenetur est illo totam? Similique veniam ab iusto ad, pariatur quo nisi facere officiis temporibus dicta quibusdam maiores eaque nemo sint sapiente totam obcaecati adipisci doloribus reiciendis error eveniet laudantium consequatur? Repellat, beatae eaque. Eligendi, facilis maxime eum, ipsam dolor libero tenetur earum soluta distinctio sequi cum consequatur hic quaerat quas similique molestias deserunt commodi pariatur? Assumenda magnam, labore, sapiente fugit ab cupiditate numquam officiis itaque quibusdam impedit corrupti molestias ad sunt veniam, doloribus voluptate est. Laboriosam labore explicabo ullam optio laudantium quibusdam repellendus eligendi. Architecto saepe at dignissimos harum optio totam soluta delectus neque repudiandae illum. Nobis suscipit voluptate dicta ea magni sint omnis cumque, enim animi ab voluptates impedit temporibus nesciunt recusandae minus. Omnis in non libero minima dignissimos eaque, nobis expedita at aspernatur tempore voluptatibus? Voluptatibus magni quo earum similique dolore dolor repudiandae eaque laboriosam voluptas cumque facere ipsam nostrum officiis aperiam quis, officia nulla totam ut odit fuga consectetur molestias eveniet! Eaque dolor suscipit voluptatem enim. Laboriosam, magni dolorem corporis amet quos praesentium rerum. Culpa cum iste odio impedit perspiciatis quibusdam deleniti temporibus voluptatum dolorem, nesciunt eum sint eveniet eaque necessitatibus dolore aperiam incidunt veritatis exercitationem fugiat. Facere perferendis autem nisi! Iure esse commodi provident labore tempore pariatur, libero recusandae minus sit consequatur reprehenderit? Itaque aperiam, iusto eveniet veniam ullam error beatae similique porro, suscipit autem cumque quaerat neque consequuntur non culpa explicabo. Dolor tempore harum blanditiis ullam hic suscipit similique voluptates distinctio dicta rem eligendi repudiandae exercitationem rerum, aliquam, sunt perferendis voluptatum quos obcaecati! Perferendis, rerum totam qui quisquam iusto, illum possimus et ipsa natus aut nesciunt sed? Blanditiis accusamus inventore laboriosam unde, neque repudiandae, beatae deleniti quibusdam quam consequatur provident architecto, facilis illum aperiam quae sint necessitatibus sapiente repellat minima consequuntur ab! Excepturi quae, incidunt numquam illo nemo non quisquam cum aliquid beatae voluptates sequi tenetur totam animi dolor, perspiciatis aspernatur maxime necessitatibus fuga nobis!',
        ]);


        //admin directions
        $directionAdmin = Direction::create([
            'user_id'       => $useradmin->id,
            'status'        => '1',
            'country'       => 'Picanya',
            'name'          => 'Mundial dirección',
            'line_1'        => 'C/Sequia de Tormos nº3',
            'line_2'        => '',
            'city'          => 'Valencia',
            'state'         => 'España',
            'postal_code'   => '46210',
            'phone'         => '646136523',
            'nif'           => 'B96344791',
            'comments'      => 'ninguno',
        ]);


        //permission
        $permission_all = [];
        $permission_seller = [];
        $permission_customer = [];

        //permission role
        $permission = Permission::create([
            'name' => 'Listado roles',
            'slug' => 'role.index',
            'module' => 'role',
            'description' => 'El usuario puede listar los roles',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Ver role',
            'slug' => 'role.show',
            'module' => 'role',
            'description' => 'El usuario puede ver los roles',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Crear role',
            'slug' => 'role.create',
            'module' => 'role',
            'description' => 'El usuario puede crear roles',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Editar role',
            'slug' => 'role.edit',
            'module' => 'role',
            'description' => 'El usuario puede editar roles',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Eliminar role',
            'slug' => 'role.destroy',
            'module' => 'role',
            'description' => 'El usuario puede eliminar roles',
        ]);

        $permission_all[] = $permission->id;
    
        //permission products
        $permission = Permission::create([
            'name' => 'Listado de productos',
            'slug' => 'product.index',
            'module' => 'product',
            'description' => 'El usuario puede listar los productos',
        ]);

        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;
        $permission_provider[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver la ficha del producto',
            'slug' => 'product.show',
            'module' => 'product',
            'description' => 'El usuario puede ver las fichas de los productos',
        ]);

        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;
        $permission_provider[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Crear productos',
            'slug' => 'product.create',
            'module' => 'product',
            'description' => 'El usuario puede crear productos',
        ]);

        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;
        $permission_provider[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar producto',
            'slug' => 'product.edit',
            'module' => 'product',
            'description' => 'El usuario puede editar productos',
        ]);

        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;
        $permission_provider[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar productos',
            'slug' => 'product.destroy',
            'module' => 'product',
            'description' => 'El usuario puede bloquear productos',
        ]);

        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;


        //permission category
        $permission = Permission::create([
            'name' => 'Listado de categorías',
            'slug' => 'category.index',
            'module' => 'category',
            'description' => 'El usuario puede listar categorías',
        ]);
        
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Ver categoría',
            'slug' => 'category.show',
            'module' => 'category',
            'description' => 'El usuario puede ver la ficha de la categoría',
        ]);        
        
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Editar categorías',
            'slug' => 'category.edit',
            'module' => 'category',
            'description' => 'El usuario puede editar categorías',
        ]);
        
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Eliminar categorías',
            'slug' => 'category.destroy',
            'module' => 'category',
            'description' => 'El usuario puede eliminar categorías',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;


        //permission dashboard
        $permission = Permission::create([
            'name' => 'Ver el dashboard',
            'slug' => 'dashboard.index',
            'module' => 'category',
            'description' => 'El usuario puede ver el dashboard',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Ver estadísticas comerciales',
            'slug' => 'dashboard.statistics.show',
            'module' => 'category',
            'description' => 'El usuario puede las estadísticas comerciales',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver ventas',
            'slug' => 'dashboard.sales.show',
            'module' => 'dashboard',
            'description' => 'El usuario puede ver las ventas usuarios',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver usuarios registrados',
            'slug' => 'dashboard.users.show',
            'module' => 'dashboard',
            'description' => 'El usuario puede ver los usuarios registrados',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver usuarios en línea',
            'slug' => 'dashboard.online.show',
            'module' => 'dashboard',
            'description' => 'El usuario puede ver los usuarios en línea',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        //permission orders
        $permission = Permission::create([
            'name' => 'Listado de pedidos',
            'slug' => 'order.index',
            'module' => 'order',
            'description' => 'El usuario puede listar pedidos',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver pedidos',
            'slug' => 'order.show',
            'module' => 'order',
            'description' => 'El usuario puede ver el pedido',
        ]);        
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar pedidos',
            'slug' => 'order.edit',
            'module' => 'order',
            'description' => 'El usuario puede editar pedidos',
        ]);
        
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Eliminar pedidos',
            'slug' => 'order.destroy',
            'module' => 'order',
            'description' => 'El usuario puede eliminar pedidos',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        //permission user
        $permission = Permission::create([
            'name' => 'Listado de usuarios',
            'slug' => 'user.index',
            'module' => 'user',
            'description' => 'El usuario puede listar usuarios',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver usuarios',
            'slug' => 'user.show',
            'module' => 'user',
            'description' => 'El usuario puede ver la ficha de usuario',
        ]);        
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar usuarios',
            'slug' => 'user.edit',
            'module' => 'user',
            'description' => 'El usuario puede editar usuarios',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Bloquear usuarios',
            'slug' => 'user.destroy',
            'module' => 'user',
            'description' => 'El usuario puede bloquear usuarios',
        ]);
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;


        //own user auth
        $permission = Permission::create([
            'name' => 'Ver perfil usuario activo',
            'slug' => 'userown.show',
            'module' => 'user',
            'description' => 'El usuario puede ver su propio perfil',
        ]);        
        
        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;
        $permission_customer[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar perfil usuario activo',
            'slug' => 'userown.edit',
            'module' => 'user',
            'description' => 'El usuario puede editar su propio perfil',
        ]);

        $permission_all[] = $permission->id;
        $permission_seller[] = $permission->id;
        $permission_customer[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Create user',
            'slug' => 'user.create',
            'module' => 'user',
            'description' => 'El usuario puede crear usuarios',
        ]);
        
        $permission_all[] = $permission->id;

        
        //permission permissions
        $permission = Permission::create([
            'name' => 'Crear permisos de usuario',
            'slug' => 'user.permissions.create',
            'module' => 'user',
            'description' => 'El usuario puede crear permisos de usuario',
        ]);
        
        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver permisos de usuario',
            'slug' => 'user.permissions.index',
            'module' => 'user',
            'description' => 'El usuario puede ver permisos de usuario',
        ]);
        
        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar permisos de usuario',
            'slug' => 'user.permissions.edit',
            'module' => 'user',
            'description' => 'El usuario puede editar permisos de usuario',
        ]);
        
        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar permisos de usuario',
            'slug' => 'user.permissions.destroy',
            'module' => 'user',
            'description' => 'El usuario puede eliminar permisos de usuario',
        ]);
        
        $permission_all[] = $permission->id;
        
        
        //table permission_role
        $roladmin->permissions()->sync( $permission_all);
        $rolseller->permissions()->sync( $permission_seller);
        $rolprovider->permissions()->sync( $permission_provider);
        $rolcustomer->permissions()->sync( $permission_customer);
    }
}
