@extends("layouts.dashboard")

@section('title', __('Distribuidores') )

@section("content")
	@include("dashboard.sellers.list")
@endsection