@push('css')
	<style>
		#avisoCookies {
			position: absolute;
			top: 0;
			z-index: 999;
			left:0;
			right:0;
			padding: 15px;
			box-sizing: border-box;
			background: rgba( 220, 220, 220, 0.8);
			border-radius: 0 0 15px 15px;
			display: none;
		}
	</style>
@endpush
<aside id="avisoCookies" class="container">
	Esta página utiliza cookies propias con el fin de mejorar el servicio. Si continuas navegando, aceptas su uso.
	<button class="btn btn-dark float-right" id="aceptarCookies">Aceptar</button>
</aside>

@push('js')
	<script>
		function checkCookieLaw(){
			if ( !window.localStorage.getItem('cookieLawKeyMiWeb') ){
				$('#avisoCookies').show();
			}
		}
		checkCookieLaw();

		$('#aceptarCookies').on( "click", function(){
			window.localStorage.setItem('cookieLawKeyMiWeb', true);
			$('#avisoCookies').hide();
		});
	</script>
@endpush