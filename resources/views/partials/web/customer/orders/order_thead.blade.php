<thead>
    <tr class="text-center">
        <th>{{ __("Coste total") }}</th>
        <th>{{ __("Fecha del pedido") }}</th>
        <th>{{ __("Estado") }}</th>
        <th>{{ __("Número de artículos") }}</th>
        <th>{{ __("Portes") }}</th>
        @if(!$detail)
            <th>{{ __("Acciones") }}</th>
        @endif
    </tr>
</thead>
