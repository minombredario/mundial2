<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DirectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case "POST": {
                return [
                    'default' => Rule::in('1',null),
                    'country' => 'required' ,
                    'name' => 'required', 
                    'line_1' => 'required',
                    'line_2' => '',
                    'city' => 'required',
                    'state' => 'required',
                    'postal_code' => 'required',
                    'phone' => 'required',
				];
            }
            case "PUT": {
                return [
                    'default' => Rule::in('1',null),
                    'country' => 'required' ,
                    'name' => 'required', 
                    'line_1' => 'required',
                    'line_2' => '',
                    'city' => 'required',
                    'state' => 'required',
                    'postal_code' => 'required',
                    'phone' => 'required',
                ];
            }
            default: {
                return [];
            }
        }
    }
}
