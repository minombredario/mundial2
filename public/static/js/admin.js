
var base = location.potocol + '//' + location.host;
var route = document.getElementsByName('routeName')[0].getAttribute('content');
$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

base = "127.0.0.1:8000";

document.addEventListener('DOMContentLoaded', function () {
   
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })

    var btn_search = document.getElementById('btn_search');
    var form_search = document.getElementById('form_search');
    if (btn_search) {
        btn_search.addEventListener('click', function (e) {
            e.preventDefault();
            if (form_search.style.display === 'block') {
                form_search.style.display = 'none';
            } else {
                form_search.style.display = 'block';
            }
        });
    }
    if (route == "product_edit") {
         var btn_product_file_image = document.getElementById('btn_product_file_image');
         var product_file_image = document.getElementById('product_file_image');
         btn_product_file_image.addEventListener('click', function () {
             product_file_image.click();
         }, false);

        product_file_image.addEventListener('change', function () {
             document.getElementById('form_product_gallery').submit();
        })
    }
    route_active = document.getElementsByClassName('lk-' + route)[0].classList.add('active');
    alert(route_active)
    btn_deleted = document.getElementsByClassName('btn-deleted');
    for (var i = 0; i < btn_deleted.length; i++){
       btn_deleted[i].addEventListener('click', delete_object);
    }
   
});
$(document).ready(function () {
   
    if (route == "product_edit" || route == "product_add") {
        editor_init('editor');
    }
   
    
})
function editor_init(field) {
    CKEDITOR.plugins.addExternal('codesnippet', base + '/static/libs/ckeditor/plugins/codesnippet/', 'plugin.js');
    CKEDITOR.editorConfig = function( config ) {
        config.language = 'es';
        config.uiColor = '#F7B42C';
        config.height = 300;
        config.toolbarCanCollapse = true;
    };
    CKEDITOR.replace(field, {
        skin: 'moono',
        extraPlugins: 'codesnippet,ajax,xml,textmatch,autocomplete,textwatcher,emoji,panelbutton,preview,wordcount,table,tableselection,tabletools,image',
        toolbar: [
            { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText','PasteFromWord', '-', 'Undo', 'Redo'] },
            { name: 'basicstyles', items: ['Bold', 'Italic','Underline', 'BulletedList', 'Strike', 'Easyimage', 'Link', 'Unlink', 'Blockquote'] },
            { name: 'document', items: ['Table', 'Tableselection', 'Tabletools','CodeSnippet', 'EmojiPanel', 'Preview', 'Source'] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
            { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
            
        ]
    });
}

function delete_object(e) {
    e.preventDefault();
    
    var object = this.getAttribute('data-object');
    var action = this.getAttribute('data-action');
    var path = this.getAttribute('data-path');
    var url = base + '/' + path + '/' + object + '/' + action;
  
    swal({
        title: action == "delete" ? '¿Eliminar?' : '¿Restaurar?',
        text: action == "delete" ? 'Recuerda que esta acción enviara este elemento a la papelera' : 'Esta acción restaurará este elemento y estará activo en la base de datos',
        icon: action == "delete" ? 'warning' : 'info',
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            window.location.href = url;
        }
    });
    /*swal.fire({
        title: "¿Estás seguro de eliminar este objeto?",
        text: "Recuerda que esta acción enviará este elemento a la papelera o lo eliminará de forma definitiva",
        icon: 'warning',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar',
        customClass: {
            confirmButton: 'btn btn-dark',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    }).then( result => {
        if (result.value) {
            
            axios.get(`admin/product/${object}/deleted`).then(response => {
                console.log('si');
            });
        }
    });*/
    
   
}