<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Permission;
use App\Traits\Hashidable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property string|null $full-access
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Role $user
 * @method static Builder|Role filtered()
 * @method static Builder|Role newModelQuery()
 * @method static Builder|Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|Role onlyTrashed()
 * @method static Builder|Role query()
 * @method static Builder|Role whereCreatedAt($value)
 * @method static Builder|Role whereDeletedAt($value)
 * @method static Builder|Role whereDescription($value)
 * @method static Builder|Role whereFullAccess($value)
 * @method static Builder|Role whereId($value)
 * @method static Builder|Role whereName($value)
 * @method static Builder|Role whereSlug($value)
 * @method static Builder|Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Role withoutTrashed()
 * @mixin \Eloquent
 */
class Role extends Model
{
    
    use Hashidable, SoftDeletes;

    protected $fillable = [
        'name', 'slug', 'description', 'full-access',
    ];

    public function user(){
        return $this->belongsTo(Role::class);
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class)->withTimesTamps();
    }

    
    public function scopeFiltered(Builder $builder) {
             
        if(request()->has('name')){
            $builder->where('name', 'LIKE', '%' . e(request('name')) . '%');
        }

        return $builder->paginate();
    }

    /*public function scopeForAdmin(Builder $builder) {
        return $builder
            ->withCount('users')
            ->where("user_id", auth()->id())
            ->paginate();
    }
    */
}
