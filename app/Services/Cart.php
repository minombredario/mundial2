<?php
namespace App\Services;
use App\Helpers\Currency;
use App\Models\Product;
use App\Models\Direction;
use App\Models\Order;

use Illuminate\Support\Collection;
//https://www.youtube.com/watch?v=4J939dDUH4M&list=PL55RiY5tL51qUXDyBqx0mKVOhLNFwwxvH&index=9
/**
 * Class Cart
 * @package App\Classes
 */
class Cart {

    /**
     * @var Collection
     */
    protected Collection $cart;
    //protected $cart;

    public $countSeller;
    /**
     * Cart constructor.
     */
    public function __construct() {
        if (session()->has("cart")) {
            $this->cart = session("cart");
        } else {
            $this->cart = new Collection;
        }
    }

    /**
     *
     * Get cart contents
     *
     */
    public function getContent(): Collection {
        return $this->cart;
    }

    /**
     * Save the cart on session
     */
    protected function save(): void {
        session()->put("cart", $this->cart);
        session()->save();
    }

    /**
     *
     * Add product on cart
     *
     * @param Product $product
     */
    public function addProduct(Product $product, $qty): void {
        
        if($this->cart->contains('id', $product->id) ){
           
            $this->cart->map(function ($item, $key) use ($product, $qty) {
                if($item->id == $product->id)
                    $item->qty += intval($qty);
                });
        }else{
            $product->qty = intval($qty);
            $this->cart->push($product);
        }
        
        $this->save();
    }

    /**
     *
     * Remove product from cart
     *
     * @param int $id
     */
    public function removeProduct(int $id): void {
        $this->cart = $this->cart->reject(function (Product $product) use ($id) {
            return $product->id === $id;
        });
        $this->save();
    }

    /**
     *
     * Remove product from cart
     *
     * @param int $id
     */
    public function changeQuantityProduct(Product $product, $qty): void {
        
       if($this->cart->contains('id', $product->id) ){
            $this->cart->map(function ($item, $key) use ($product, $qty) {
                if($item->id == $product->id)
                    $item->qty = intval($qty);
                });
        }
        
        $this->save();
    }

    /**
     *
     * calculates the total cost in the cart
     *
     * @param bool $formatted
     * @return mixed
     */
    public function totalAmount($formatted = true) {
        
        
        $amount = $this->cart->sum(function (Product $product) {
            $discount = 0;
            if($product->discount){
                $discount = $product->discount_type == Product::PERCENT ? $product->price * $product->discount / 100 : $product->discount ;
            }
        
            return ($product->price - $discount) * $product->qty;
        });

        if ($formatted) {
            return Currency::formatCurrency($amount);
        }
        return $amount;
    }

    /**
     *
     * all taxes for cart
     *
     * @param bool $formatted
     * @return float|int|string
     */
    public function taxes($formatted = true) {
       
        $total = $this->totalAmount(false) + $this->shippingCost();
        
        if ($total) {
            //$total = ($total * env('TAXES')) / 100;
             $total = ($total * Config('mundial_souvenir.iva') ) / 100 ;
            if ($formatted) {
                return Currency::formatCurrency($total);
            }
            return $total;
        }
        return 0;
    }

    public function totalAmountWithTaxes($formatted = true){
        $total = $this->totalAmount(false);
        
        if ($total) {
            //$total = ($total * env('TAXES')) / 100;
            $total = ($total * Config('mundial_souvenir.iva')) / 100 + $total + $this->shippingCost();
            if ($formatted) {
                return Currency::formatCurrency($total);
            }
            return $total;
        }
        return 0;
    }

    public function moreSellers(){
        $sellers = $this->cart->countBy(function($item) {
                return $item['user_id'];
        });
        $this->countSellers = count($sellers);
        return count($sellers);
    }

    public function shippingCost(){
        if(session()->has('direction[shipping]')) {
            $direction = Direction::findOrFail(session('direction[shipping]'));
            $cost = strcasecmp($direction->state, 'españa') == 0 ? Config('mundial_souvenir.shipping_cost') : Config('mundial_souvenir.shipping_cost_international');
            return $cost * $this->moreSellers();
        }
    }

    public function totalAmountWithShippingCost($formatted = true){
        $total = $this->totalAmount(false);
       

        if ($total) {
            //$total = ($total * env('TAXES')) / 100;
            $total = $total + ($this->shippingCost() * $this->moreSellers() ) ;
            if ($formatted) {
                return Currency::formatCurrency($total);
            }
            return $total;
        }
        return 0;
    }


    /**
     *
     * Total products in cart
     *
     * @return int
     */
    public function hasProducts(): int {
        return $this->cart->sum(function (Product $product) {
           return $product->qty;
        });
        //return $this->cart->count();
    }

    /**
     *
     * Order number
     *
     * @return int
     */
    
    public function orderNumber() {
        $order = Order::orderBy('id', 'desc')
                    ->take(1)
                    ->first();
        return $order->invoice + 1;
        
    }

    /*
     * Clear cart
     */
    public function clear(): void {
        $this->cart = new Collection;
        $this->save();
    }

    public static function tax() {
        return Config('mundial_souvenir.iva');
    }

    public static function portes() {
        if(session()->has('direction[shipping]')) {
            $direction = Direction::findOrFail(session('direction[shipping]'));
            $cost = strcasecmp($direction->state, 'españa') == 0 ? Config('mundial_souvenir.shipping_cost') : Config('mundial_souvenir.shipping_cost_international');
            return $cost;
        }

        return Config('mundial_souvenir.shipping_cost');
    }


}
