

<div class="row mtop16  d-flex justify-content-center">
    @can('haveaccess', 'dashboard.users.show')  
    <div class="col-md-2 py-1">
        <div class="panel shadow">
            <div class="header text-center">
                <h2 class="title">
                    <i class="fas fa-users"></i> <span class="d-md-none d-xl-inline">{{ __('Clientes.')}} </span> 
                </h2>
            </div>
            <div class="inside">
                <div class="big_count">
                    {{ \App\Models\Stats::usersCustomer() }}
                </div>
            </div>
        </div>
    </div>
    @endcan
    @admin
    <div class="col-md-2 py-1">
        <div class="panel shadow">
            <div class="header text-center">
                <h2 class="title">
                    <i class="fas fa-user-tie "></i> <span class="d-md-none d-xl-inline">{{ __('Distribuidores.')}} </span>
                </h2>
            </div>
            <div class="inside">
                <div class="big_count">
                    {{ \App\Models\Stats::usersSellers() }}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-2 py-1">
        <div class="panel shadow">
            <div class="header text-center">
                <h2 class="title">
                    <i class="fas fa-user-friends"></i> <span class="d-md-none d-xl-inline">{{ __('Proveedores.')}} </span>
                </h2>
            </div>
            <div class="inside">
                <div class="big_count">
                    {{ \App\Models\Stats::usersProviders() }}
                </div>
            </div>
        </div>
    </div>

    @endadmin
    <div class="col-md-2 py-1">
        <div class="panel shadow">
            <div class="header text-center">
                <h2 class="title">
                    <i class="fas fa-boxes"></i> <span class="d-md-none d-xl-inline">{{ __('Total productos') }}.</span>
                </h2>
            </div>
            <div class="inside">
                <div class="big_count">
                    {{ \App\Models\Stats::totalProducts() }}
                </div>
            </div>
        </div>
    </div>
    @can('haveaccess', 'dashboard.sales.show')            
    <div class="col-md-2 py-1">
        <div class="panel shadow">
            <div class="header text-center">
                <h2 class="title">
                    <i class="fas fa-shopping-basket"></i> <span class="d-md-none d-xl-inline">{{ __('Pedidos hoy') }}.</span>
                </h2>
            </div>
            <div class="inside">
                <div class="big_count">
                    {{ \App\Models\Stats::orders('count') }}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-2 py-1">
        <div class="panel shadow">
            <div class="header text-center">
                <h2 class="title">
                    <i class="fas fa-credit-card"></i> <span class="d-md-none d-xl-inline">{{ __('Facturado hoy') }}.</span>
                </h2>
            </div>
            <div class="inside">
                <div class="big_count">
                    {{ \App\Models\Stats::orders('sum') }}
                </div>
            </div>
        </div>
    </div>
    @endcan  
</div>
