<?php
namespace App\Traits\Customer;

use Illuminate\Http\Request;
use App\Models\Direction;
use App\Http\Resources\Direction as DirectionResource;
use Auth, DB;

trait ManageDirections {
    public function json() {
        
        if (request()->ajax()) {
            $user = auth()->user();    
            $directions = Direction::where('user_id',$user->id)
                ->orderBy("created_at", "DESC")
                ->paginate(4);
           
            return DirectionResource::collection($directions);
        }
        abort(401);
	}
	
	public function createDirection(Request $request, Direction $direction){
        
        $user = Auth()->user();
       
        //dd(request()->all());

        try {
            DB::beginTransaction();

            $direction = Direction::create($this->directionInput());
           
            if(request('default') != 'no'){
                Direction::where('user_id', $user->id)->update(['default' => null]);
                $direction->update(['default' => 1]);
            }

            DB::commit();
            session()->flash("message", ["success", __("Dirección creada satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back();
        }        
    }

    public function updateDirection(Request $request){
		
        try {
            if (request()->ajax()) {
                $direction = Direction::findOrFail(request('id'));
                $direction->fill($this->directionInput())->save();
				
                if(request('default') == 1){

					$direction->default = 1;
					$direction->save();
					Direction::where('user_id', $direction->user_id)->where('id','!=', $direction->id)->update(['default' => null]);
					
				}
				
				//return response()->json($direction);
				//session()->flash("message", ["success", __("Dirección actualizada satisfactoriamente")]);
				//return back();
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }

        
        
        return response()->json([
            'message' => __("Dirección actualizada satisfactoriamente"),
            'status' => 'success',
            'items' => Direction::where('user_id', Auth()->user()->id)->paginate()
        ]);
    }

    public function destroyDirection(Request $request){
       
       try {
            if (request()->ajax()) {
               
                $direction = Direction::findOrFail(request('id'));
                $direction->default = null;
                $direction->save();
                $direction->delete();

                 return response()->json(["message" => "success"]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
             return response()->json("message", ["danger", $exception->getMessage()]);
        }

	}

	public function store(Request $request, Direction $direction) {
        
        $user = Auth()->user();
       
        try {
            DB::beginTransaction();

            $direction = Direction::create($this->directionInput());
           
            if(request('default') == 1){

				$direction->default = 1;
				$direction->save();
				Direction::where('user_id', $direction->user_id)->where('id','!=', $direction->id)->update(['default' => null]);
					
			};

            DB::commit();
            return response()->json(["message" => "success"]);
        } catch (\Throwable $exception) {
            return response()->json(["message" => "danger"]);
        } 
    }

    public function shippingDirection(){

        session()->remove('direction[shipping]');
        if (request('id')) {
            session()->put('direction[shipping]', request('id'));
            session()->save();
        }
    
    }

    public function getShippingDirection(){
        if (session()->has('direction[shipping]')) {
            
            return Direction::findOrFail(session('direction[shipping]'));
        }
    }

    protected function directionInput(): array {

        return [
            'status' => 1, 
            'default' => e(request('default')) == '0' || e(request('default')) == null ? null : 1,
            'country' => e(request('country')),
            'name' => e(request('name')) == null ? Auth()->user()->name : e(request('name')), 
            'line_1' => request('line_1'),
            'line_2' => request('line_2'),
            'city' => e(request('city')),
            'state' => e(request('state')),
            'postal_code' => e(request('postal_code')),
            'phone' => e(request('phone')),
            'nif' => e(request('nif')) == null ? Auth()->user()->nif : e(request('nif')),
            'comments' => e(request('comments'))
        ];
    }

}
